<?php die("Access Denied"); ?>#x#a:2:{s:6:"result";s:2876:"<div class="sp-module "><div class="sp-module-content">

<div class="custom"  >
	<div class="row">
<div class="col-md-12">
<h2>Our Experience Makes the Difference</h2>
<p>At Offshore Financial, we have been offering personalized marine financing programs since 1989. Our reputation for being an industry leader in customer service is made possible by our professional staff. Each associate within our organization has been chosen based on their knowledge of boat financing and their dedication to "YOU" our customer. Our associates average over 20 years of experience in just yacht / boat financing. The slogan "Our People and Our Service Make the Difference" are not just words, but is truly what sets us apart from the rest.</p>
<p>We work with one of the largest networks of quality bank lenders in the marine industry to make sure that our customers are offered not only the most competitive rates, but also have the options they deserve in their financial packages.</p>
</div>
</div>
<div class="row text-center">
<div class="col-md-3 wow bounceIn"><img class="img-responsive center-block" src="images/misc/icon-financing.png" />
<h4>Financing available for limited charter boats</h4>
</div>
<div class="col-md-3 wow bounceIn" data-wow-delay="1s"><img class="img-responsive center-block" src="images/misc/icon-download-application.png" />
<h4>Download <a href="index.php?option=com_rsform&view=rsform&formId=2&Itemid=152">credit application</a></h4>
</div>
<div class="col-md-3 wow bounceInwow bounceIn" data-wow-delay="2s"><img class="img-responsive center-block" src="images/misc/icon-state-programs.png" />
<h4>Stated income programs</h4>
</div>
<div class="col-md-3 wow bounceIn" data-wow-delay="3s"><img class="img-responsive center-block" src="images/misc/icon-timeframe.png" />
<h4>20yr term available for loan amounts of $100,000 and up</h4>
</div>
</div>
<div class="row wow bounceInDown">
<div class="col-md-2"><img class="img-responsive center-block" src="images/misc/icon-financing.png" /></div>
<div class="col-md-10" style="padding-top: 20px;">
<p class="text-red"><strong>Financing available for limited charter boats</strong></p>
<p>Offshore Financial offers loans to borrowers that want to use their boats for limited charter. Maximum is 72 per year. Please call office for more details.</p>
</div>
</div>
<div class="row wow bounceInDown">
<div class="col-md-2"><img class="img-responsive center-block" src="images/misc/icon-state-programs.png" /></div>
<div class="col-md-10" style="padding-top: 20px;">
<p class="text-red"><strong>Stated income programs</strong></p>
<p>Offshore Financial offers loans up to $99,000 that do not require copies of your tax returns or pay stubs. Credit score of 720 and a minimum of 15% down is required. Please call this office for further details.</p>
</div>
</div></div>
</div></div>";s:6:"output";a:3:{s:4:"body";s:0:"";s:4:"head";a:0:{}s:13:"mime_encoding";s:9:"text/html";}}