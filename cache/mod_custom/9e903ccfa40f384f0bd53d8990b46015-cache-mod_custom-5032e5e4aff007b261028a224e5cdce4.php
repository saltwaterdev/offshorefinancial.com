<?php die("Access Denied"); ?>#x#a:2:{s:6:"result";s:4916:"<div class="sp-module "><div class="sp-module-content">

<div class="custom"  >
	<div class="row">
<div class="col-md-12">
<h1>About Us</h1>
<h3>About Offshore Financial</h3>
<p>Offshore Financial Corp. is a marine service company specializing in boat loans since 1989. The company was founded by Bernadette Horvath and Matt Bartosh, who have over 60 years of combined experience in the marine financing industry. Together with a team of carefully selected professionals, they have made it their mission to provide you, the boat buyer, with superior personalized service. Starting with a simplified application and continuing thru to closing we make sure that your loan is handled smoothly with no last minute surprises.</p>
<p>Because Offshore Financial works with a variety of national lending institutions, it gives us the ability to offer you the most competitive boat financing programs in the market.</p>
<p>Your complete satisfaction is our number one priority, so whether you’re purchasing a new boat from a dealership or a used boat from a friend, you can be assured that Offshore Financial is working for you and that your boat financing will be handled smoothly and professionally.</p>
<h3>Our Mission</h3>
<ul>
<li>To provide you with financing programs for new &amp; used boats, as well as refinancing existing boat loans.</li>
<li>To offer you the most competitive rates and terms, whether you are purchasing a daysailer or a megayacht.</li>
<li>To tailor a financing program that will meet your individual needs and provide you with other cost saving services.</li>
<li>To provide you with the best in customer service so that your entire boat financing process is a smooth and pleasant one.</li>
</ul>
<h3>Our People Make the Difference</h3>
<p>At Offshore Financial our reputation for being an industry leader in customer service is made possible only by our people. Each associate within our organization has been chosen based on their knowledge, professionalism and dedication to first "you", our customer and second to the industry. Our associates average over 20 years of experience in just marine financing. The slogan "Our People Make The Difference" is not just words, but is what sets us apart from the rest.</p>
<h3>Building Relationships</h3>
<p>Even though the boat you're purchasing today is perfect for your needs, you might want to make a change in the future and we realize that. So at Offshore Financial we don't think of you as just Today's applicant. We work with you to develop a clear understanding of your financial situation the first time, so that if or when you need financing again, one simple call to our office will reactivate your file and you'll be dealing with someone who is totally familiar with your individual situation. At Offshore Financial we want to be your marine finance source today and in the future.</p>
<h3>Person to Person</h3>
<p>While technology is necessary to conduct business efficiently today, we have not forgotten the age old art of communication. The saying, "Bigger is Always Better" is not the case in marine financing; Offshore Financial, provides superior service while maintaining personalized contact before, during, and after the loan. We believe in productive two-way communications which allows us to fully understand your needs. At Offshore Financial, the old-fashioned handshake and quality conversation is a refreshing change from our competitors normal business operations. We look forward to discussing how we can make your dream an affordable reality.</p>
</div>
</div>
<div class="row">
<div class="col-md-12">
<h3>Matthew G. Bartosh</h3>
</div>
</div>
<div class="row">
<div class="col-md-2"><img class="img-responsive center-block" src="images/misc/offshore-mathew.jpg" /></div>
<div class="col-md-10">
<p>Matthew G. Bartosh, 63 of Manasquan passed away unexpectedly Thursday, June 30, 2016 at his home with his devoted wife at his side. Matt was born in Montclair and had resided in Cedar Grove and Brick Township before moving to Manasquan 21 years ago. He was a graduate of Marietta College, in Ohio and had attended Kean College. Matt along with his wife Bernadette were the founders of Offshore Financial Corp. Bay Head, where Matt served as the President. Matt was an avid athlete participating in many different sports, including cycling, skiing, swimming, and baseball, which he played through college and beyond. He was a huge fan of the NY Yankees, but not their broadcast team. He never missed his daughter's softball games; he coached her in field hockey and enthusiastically supported her college Ultimate Frisbee team. Matt was genuinely a loving, caring and incredibly supportive husband. As a family, they spent much of their time together in Colorado. He was a member of the Porsche Club of America and the Jersey Shore Region of the Club.</p>
</div>
</div></div>
</div></div>";s:6:"output";a:3:{s:4:"body";s:0:"";s:4:"head";a:0:{}s:13:"mime_encoding";s:9:"text/html";}}