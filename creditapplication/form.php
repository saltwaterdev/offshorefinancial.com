<h2>Application for Secured Credit</h2>
<form id="application-secured-credit" method="post" action="/">
	<fieldset>
		<input type="radio" name="application-type" value="Individual"> Individual
		<input type="radio" name="application-type" value="Joint"> Joint
		<input type="radio" name="application-type" value="Other"> Other
	</fieldset>
	<div id="boat_information" class="group">
		<h3>Boat Information</h3>
		<fieldset>
			<label for="residence-check">Will this boat be used as your principle residence?</label>
			<input type="radio" name="residence-check" value="Yes"> Yes
			<input type="radio" name="residence-check" value="No"> No
		</fieldset>
		<fieldset>
			<label for="intended-use">Intended Use:</label>
			<input type="radio" name="intended-use" value="Pleasure"> Pleasure
			<input type="radio" name="intended-use" value="Commercial"> Commercial
			<input type="radio" name="intended-use" value="Other"> Other
		</fieldset>
		<div class="row">
			<fieldset class="quint">
				<label for="purchase-price">Purchase Price (without tax)</label>
				<input type="text" name="purchase-price" placeholder="0">
			</fieldset>
			<fieldset class="quint">
				<label for="cash-down">Cash Down Payment</label>
				<input type="text" name="cash-down" placeholder="0">
			</fieldset>
			<fieldset class="quint">
				<label for="trade-in">Trade In</label>
				<input type="text" name="trade-in" placeholder="0">
			</fieldset>
			<fieldset class="quint">
				<label for="total-down">Total Down Payment</label>
				<input type="text" name="total-down" placeholder="0">
			</fieldset>
			<fieldset class="quint">
				<label for="amount-financed">Amount Financed</label>
				<input type="text" name="amount-financed" placeholder="0">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="quint">
				<label for="term-loan">Term of Loan</label>
				<input type="text" name="term-loan">
			</fieldset>
			<fieldset class="eighty">
				<label for="dealer-info">Name & Address of Dealer Broker or Seller</label>
				<textarea name="dealer-info"></textarea>
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="six">
				<label for="new-used">New or Used</label><br>
				<input type="radio" name="new-used" value="New"> New<br>
				<input type="radio" name="new-used" value="Used"> Used
			</fieldset>
			<fieldset class="six">
				<label for="boat-year">Year</label>
				<input type="text" name="boat-year">
			</fieldset>
			<fieldset class="six">
				<label for="boat-manufacturer">Manufacturer</label>
				<input type="text" name="boat-manufacturer">
			</fieldset>
			<fieldset class="six">
				<label for="boat-model">Model</label>
				<input type="text" name="boat-model">
			</fieldset>
			<fieldset class="six">
				<label for="boat-length">Length</label>
				<input type="text" name="boat-length">
			</fieldset>
			<fieldset class="six">
				<label for="hull">Hull</label>
				<input type="text" name="hull">
			</fieldset>
		</div>
		<div class="row">
			<fieldset>
				<h4>Engine Information</h4>
				<fieldset class="thirtyfive">
					<label for="engine-hp">H.P.</label>
					<input type="text" name="engine-hp">
				</fieldset>
				<fieldset class="thirtyfive">
					<label for="engine-manufacturer">Manufacturer</label>
					<input type="text" name="engine-manufacturer">
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="engines" value="Single">Single<br>
					<input type="radio" name="engines" value="Double">Double<br>
					<input type="radio" name="engines" value="Triple">Triple<br>
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="fuel" value="Gas">Gas<br>
					<input type="radio" name="fuel" value="Diesel">Diesel<br>
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="engine-location" value="Inboard">Inboard<br>
					<input type="radio" name="engine-location" value="Outboard">Outboard<br>
					<input type="radio" name="engine-location" value="IB/OB">IB/OB<br>
				</fieldset>
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="trade-in-financer">Trade-In or Previous Boat Financed By</label>
				<input type="text" name="trade-in-financer">
			</fieldset>
			<fieldset class="six">
				<label for="amount-borrowed">Amount Borrowed</label>
				<input type="text" name="amount-borrowed">
			</fieldset>
			<fieldset class="six">
				<label for="monthly-payment">Monthly Payment</label>
				<input type="text" name="monthly-payment">
			</fieldset>
			<fieldset class="six">
				<label for="balance-outstanding">Balance Outstanding</label>
				<input type="text" name="balance-outstanding">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="trade-in-description">Description of Trade In</label>
				<input type="text" name="trade-in-description">
			</fieldset>
			<fieldset class="eight">
				<label for="trade-in-year">Year</label>
				<input type="text" name="trade-in-year">
			</fieldset>
			<fieldset class="eight">
				<label for="trade-in-manufacturer">Manufacturer</label>
				<input type="text" name="trade-in-manufacturer">
			</fieldset>
			<fieldset class="eight">
				<label for="trade-in-model">Model</label>
				<input type="text" name="trade-in-model">
			</fieldset>
			<fieldset class="eight">
				<label for="trade-in-length">Length</label>
				<input type="text" name="trade-in-length">
			</fieldset>
		</div>
		<div class="row">
			<fieldset>
				<h4>Engine Information</h4>
				<fieldset class="thirtyfive">
					<label for="trade-in-engine-hp">H.P.</label>
					<input type="text" name="trade-in-engine-hp">
				</fieldset>
				<fieldset class="thirtyfive">
					<label for="trade-in-engine-manufacturer">Manufacturer</label>
					<input type="text" name="trade-in-engine-manufacturer">
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="trade-in-engines" value="Single">Single<br>
					<input type="radio" name="trade-in-engines" value="Double">Double<br>
					<input type="radio" name="trade-in-engines" value="Triple">Triple<br>
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="trade-in-fuel" value="Gas">Gas<br>
					<input type="radio" name="trade-in-fuel" value="Diesel">Diesel<br>
				</fieldset>
				<fieldset class="tenth">
					<input type="radio" name="trade-in-engine-location" value="Inboard">Inboard<br>
					<input type="radio" name="trade-in-engine-location" value="Outboard">Outboard<br>
					<input type="radio" name="trade-in-engine-location" value="IB/OB">IB/OB<br>
				</fieldset>
			</fieldset>
		</div>
	</div>
	<div id="applicant-data">
		<h3>Applicant Data</h3>
		<div class="row">
			<fieldset class="fourty">
				<label for="applicant-first-name">First Name</label>
				<input type="text" name="applicant-first-name">
			</fieldset>
			<fieldset class="quint">
				<label for="applicant-initial">Initial</label>
				<input type="text" name="applicant-initial">
			</fieldset>
			<fieldset class="fourty">
				<label for="applicant-last-name">Last Name</label>
				<input type="text" name="applicant-last-name">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="quarter">
				<label for="applicant-ssn">Social Security Number</label>
				<input type="text" name="applicant-ssn">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-birthdate">Birth Date</label>
				<input type="text" name="applicant-birthdate">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-placeofbirth">Place of Birth</label>
				<input type="text" name="applicant-placeofbirth">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-citizen-check">US Citizen</label><br>
				<input type="radio" name="applicant-citizen-check" value="Yes">Yes<br>
				<input type="radio" name="applicant-citizen-check" value="No">No<br>
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-homestreet">Home Address: Street, City</label>
				<input type="text" name="applicant-homestreet">
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-ownrent">Own or Rent</label><br>
				<input type="radio" name="applicant-ownrent" value="Own">Own<br>
				<input type="radio" name="applicant-ownrent" value="Rent">Rent<br>
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-monthly-payment">Monthly Payment</label>
				<input type="text" name="applicant-monthly-payment">
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-years">Years</label>
				<input type="text" name="applicant-years">
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-phone">Phone</label>
				<input type="text" name="applicant-phone">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-homestate">Home Address: State, Zip</label>
				<input type="text" name="applicant-homestate">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-email">E-mail Address</label>
				<input type="text" name="applicant-email">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-cell">Cell Phone</label>
				<input type="text" name="applicant-cell">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-prevadd">Previous Address (If at present address less than 2 years)</label>
				<input type="text" name="applicant-prevadd">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-relative">Nearest Relative</label>
				<input type="text" name="applicant-relative">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-relative-phone">Phone</label>
				<input type="text" name="applicant-relative-phone">
			</fieldset>
		</div>
	</div>
	<div id="applicant-employment">
		<h3>Applicant Employment Data</h3>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-employment-employer">Present Employer Name, Address</label>
				<input type="text" name="applicant-employment-employer">
			</fieldset>
			<fieldset class="six">
				<label for="applicant-employment-business">Employer's Business</label>
				<input type="text" name="applicant-employment-business">
			</fieldset>
			<fieldset class="six">
				<label for="applicant-employment-phone">Phone</label>
				<input type="text" name="applicant-employment-phone">
			</fieldset>
			<fieldset class="six">
				<label for="applicant-employment-length">Length of Employment</label>
				<input type="text" name="applicant-employment-length">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-employment-position">Position Held</label>
				<input type="text" name="applicant-employment-position">
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-employment-salary">Salary</label>
				<input type="text" name="applicant-employment-salary">
			</fieldset>
			<fieldset class="eight">
				<label for="applicant-employment-salary-per">per:</label>
				<input type="text" name="applicant-employment-salary-per">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-employment-supervisor">Supervisor</label>
				<input type="text" name="applicant-employment-supervisor">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="applicant-employment-prev-employer">Previous Employer Name, Address</label>
				<input type="text" name="applicant-employment-employer">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-employment-prev-phone">Phone</label>
				<input type="text" name="applicant-employment-prev-phone">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-employment-prev-length">Length of Employment</label>
				<input type="text" name="applicant-employment-prev-length">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="quarter">
				<label for="applicant-employment-other-income">Other Income Sources (Income from alimony, child support or maintenance payments need not be revealed)</label>
			</fieldset>
			<fieldset class="half">
				<label for="applicant-employment-other-describe">Describe:</label>
				<input type="text" name="applicant-employment-other-describe">
			</fieldset>
			<fieldset class="quarter">
				<label for="applicant-employment-other-amount">Annual Amount</label>
				<input type="text" name="applicant-employment-other-amount">
			</fieldset>
		</div>
	</div>
	<div id="co-applicant-data">
		<h3>Co-Applicant Data</h3>
		<div class="row">
			<fieldset class="fourty">
				<label for="co-applicant-first-name">First Name</label>
				<input type="text" name="co-applicant-first-name">
			</fieldset>
			<fieldset class="quint">
				<label for="co-applicant-initial">Initial</label>
				<input type="text" name="co-applicant-initial">
			</fieldset>
			<fieldset class="fourty">
				<label for="co-applicant-last-name">Last Name</label>
				<input type="text" name="co-applicant-last-name">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="quarter">
				<label for="co-applicant-ssn">Social Security Number</label>
				<input type="text" name="co-applicant-ssn">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-birthdate">Birth Date</label>
				<input type="text" name="co-applicant-birthdate">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-placeofbirth">Place of Birth</label>
				<input type="text" name="co-applicant-placeofbirth">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-citizen-check">US Citizen</label><br>
				<input type="radio" name="co-applicant-citizen-check" value="Yes">Yes<br>
				<input type="radio" name="co-applicant-citizen-check" value="No">No<br>
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-homestreet">Home Address: Street, City</label>
				<input type="text" name="co-applicant-homestreet">
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-ownrent">Own or Rent</label><br>
				<input type="radio" name="co-applicant-ownrent" value="Own">Own<br>
				<input type="radio" name="co-applicant-ownrent" value="Rent">Rent<br>
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-monthly-payment">Monthly Payment</label>
				<input type="text" name="co-applicant-monthly-payment">
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-years">Years</label>
				<input type="text" name="co-applicant-years">
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-phone">Phone</label>
				<input type="text" name="co-applicant-phone">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-homestate">Home Address: State, Zip</label>
				<input type="text" name="co-applicant-homestate">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-email">E-mail Address</label>
				<input type="text" name="co-applicant-email">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-cell">Cell Phone</label>
				<input type="text" name="co-applicant-cell">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-prevadd">Previous Address (If at present address less than 2 years)</label>
				<input type="text" name="co-applicant-prevadd">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-relative">Nearest Relative</label>
				<input type="text" name="co-applicant-relative">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-relative-phone">Phone</label>
				<input type="text" name="co-applicant-relative-phone">
			</fieldset>
		</div>
	</div>
	<div id="co-applicant-employment">
		<h3>Co-Applicant Employment Data (if any)</h3>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-employment-employer">Present Employer Name, Address</label>
				<input type="text" name="co-applicant-employment-employer">
			</fieldset>
			<fieldset class="six">
				<label for="co-applicant-employment-business">Employer's Business</label>
				<input type="text" name="co-applicant-employment-business">
			</fieldset>
			<fieldset class="six">
				<label for="co-applicant-employment-phone">Phone</label>
				<input type="text" name="co-applicant-employment-phone">
			</fieldset>
			<fieldset class="six">
				<label for="co-applicant-employment-length">Length of Employment</label>
				<input type="text" name="co-applicant-employment-length">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-employment-position">Position Held</label>
				<input type="text" name="co-applicant-employment-position">
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-employment-salary">Salary</label>
				<input type="text" name="co-applicant-employment-salary">
			</fieldset>
			<fieldset class="eight">
				<label for="co-applicant-employment-salary-per">per:</label>
				<input type="text" name="co-applicant-employment-salary-per">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-employment-supervisor">Supervisor</label>
				<input type="text" name="co-applicant-employment-supervisor">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="half">
				<label for="co-applicant-employment-prev-employer">Previous Employer Name, Address</label>
				<input type="text" name="co-applicant-employment-employer">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-employment-prev-phone">Phone</label>
				<input type="text" name="co-applicant-employment-prev-phone">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-employment-prev-length">Length of Employment</label>
				<input type="text" name="co-applicant-employment-prev-length">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="quarter">
				<label for="co-applicant-employment-other-income">Other Income Sources (Income from alimony, child support or maintenance payments need not be revealed)</label>
			</fieldset>
			<fieldset class="half">
				<label for="co-applicant-employment-other-describe">Describe:</label>
				<input type="text" name="co-applicant-employment-other-describe">
			</fieldset>
			<fieldset class="quarter">
				<label for="co-applicant-employment-other-amount">Annual Amount</label>
				<input type="text" name="co-applicant-employment-other-amount">
			</fieldset>
		</div>
	</div>
	
	<div id="personal-financial-statement">
		<h3>Personal Financial Statement</h3>
		<table class="master">
			<tbody style="display: block;width:95%;margin:1.5em auto;">
			<tr>
				<td width="50%">
					<table>
						<th>
							Assets
						</th>
						<tr>
							<td colspan="3"><b>Cash on Hand and In Banks</b></td>
						</tr>
						<tr>
							<td width="40%">Bank/Name & Address</td>
							<td width="40%">Account No.</td>
							<td width="20%">Amount</td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="coh-bank1"></td>
							<td width="40%"><input type="text" name="coh-acc1"></td>
							<td width="20%"><input type="text" name="coh-amt1"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="coh-bank2"></td>
							<td width="40%"><input type="text" name="coh-acc2"></td>
							<td width="20%"><input type="text" name="coh-amt2"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="coh-bank3"></td>
							<td width="40%"><input type="text" name="coh-acc3"></td>
							<td width="20%"><input type="text" name="coh-amt3"></td>
						</tr>
						<tr>
							<td width="80%">Deposit on Boat Being Purchased</td>
							<td width="20%"><input type="text" name="coh-deposit"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="coh-total"></td>
						</tr>
					</table>
				</td>
				<td width="50%">
<!-- 					SIGNATURE INFO HERE -->
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td><b>Stocks and Securities</b></td>
						</tr>
						<tr>
							<td width="40%">Broker Company & Address</td>
							<td width="40%">Account No. / Account Executive</td>
							<td width="20%">Amount</td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="stocks-broker1"></td>
							<td width="40%"><input type="text" name="stocks-acc1"></td>
							<td width="20%"><input type="text" name="stocks-amt1"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="stocks-broker2"></td>
							<td width="40%"><input type="text" name="stocks-acc2"></td>
							<td width="20%"><input type="text" name="stocks-amt2"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="stocks-broker3"></td>
							<td width="40%"><input type="text" name="stocks-acc3"></td>
							<td width="20%"><input type="text" name="stocks-amt3"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="stocks-broker4"></td>
							<td width="40%"><input type="text" name="stocks-acc4"></td>
							<td width="20%"><input type="text" name="stocks-amt4"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="stocks-total"></td>
						</tr>
					</table>
				</td>
				<td width="50%" style="vertical-align: bottom;">
					<table>
						<th>
							LIABILITIES
						</th>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td><b>Retirement Accounts</b></td>
						</tr>
						<tr>
							<td width="20%"></td>
							<td width="60%">Where Held?</td>
							<td width="20%">Amount</td>
						</tr>
						<tr>
							<td width="20%">IRA(S)</td>
							<td width="60%"><input type="text" name="iras-where"></td>
							<td width="20%"><input type="text" name="iras-amt"></td>
						</tr>
						<tr>
							<td width="20%">401K</td>
							<td width="60%"><input type="text" name="401k-where"></td>
							<td width="20%"><input type="text" name="401k-amt"></td>
						</tr>
						<tr>
							<td width="20%">Keough</td>
							<td width="60%"><input type="text" name="keough-where"></td>
							<td width="20%"><input type="text" name="keough-amt"></td>
						</tr>
						<tr>
							<td width="20%">Other</td>
							<td width="60%"><input type="text" name="other-where"></td>
							<td width="20%"><input type="text" name="other-amt"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="retirement-total"></td>
						</tr>
					</table>
					<table>
						<tr>
							<td><b>Life Insurance</b></td>
						</tr>
						<tr><td width="40%">Amount of Coverage</td><td width="40%">Cash Surrender Value:</td><td width="20%"><input type="text" name="cash-surrender-value"></td>
					</table>
				</td>
				<td width="50%">
					<table>
						<tr>
							<td><b>Credit Cards</b></td>
						</tr>
						<tr>
							<td width="40%">Type</td>
							<td width="20%">Account No.</td>
							<td width="20%">Monthly Payments</td>
							<td width="20%">Balance Outstanding</td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type1"></td>
							<td width="20%"><input type="text" name="credit-acc1"></td>
							<td width="20%"><input type="text" name="credit-payment1"></td>
							<td width="20%"><input type="text" name="credit-balance1"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type2"></td>
							<td width="20%"><input type="text" name="credit-acc2"></td>
							<td width="20%"><input type="text" name="credit-payment2"></td>
							<td width="20%"><input type="text" name="credit-balance2"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type3"></td>
							<td width="20%"><input type="text" name="credit-acc3"></td>
							<td width="20%"><input type="text" name="credit-payment3"></td>
							<td width="20%"><input type="text" name="credit-balance3"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type4"></td>
							<td width="20%"><input type="text" name="credit-acc4"></td>
							<td width="20%"><input type="text" name="credit-payment4"></td>
							<td width="20%"><input type="text" name="credit-balance4"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type5"></td>
							<td width="20%"><input type="text" name="credit-acc5"></td>
							<td width="20%"><input type="text" name="credit-payment5"></td>
							<td width="20%"><input type="text" name="credit-balance5"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="credit-type6"></td>
							<td width="20%"><input type="text" name="credit-acc6"></td>
							<td width="20%"><input type="text" name="credit-payment6"></td>
							<td width="20%"><input type="text" name="credit-balance6"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL BALANCES:</b></td>
							<td width="20%"><input type="text" name="credit-total"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td width="50%"><b>Real Estate</b> - Title in Name of:</td><td width="50%"> <input type="radio" name="real-estate-title" value="Applicant"> Applicant<br><input type="radio" name="real-estate-title" value="Co-Applicant"> Co-Applicant<br><input type="radio" name="real-estate-title" value="Jointly Owned"> Jointly Owned<br></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td width="20%">Address</td>
							<td width="20%">Type</td>
							<td width="20%">Price Paid</td>
							<td width="20%">Year Purchased</td>
							<td width="20%">Market Value</td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add1"></td>
							<td width="20%"><input type="text" name="property-type1"></td>
							<td width="20%"><input type="text" name="property-price1"></td>
							<td width="20%"><input type="text" name="property-year1"></td>
							<td width="20%"><input type="text" name="property-value1"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add2"></td>
							<td width="20%"><input type="text" name="property-type2"></td>
							<td width="20%"><input type="text" name="property-price2"></td>
							<td width="20%"><input type="text" name="property-year2"></td>
							<td width="20%"><input type="text" name="property-value2"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add3"></td>
							<td width="20%"><input type="text" name="property-type3"></td>
							<td width="20%"><input type="text" name="property-price3"></td>
							<td width="20%"><input type="text" name="property-year3"></td>
							<td width="20%"><input type="text" name="property-value3"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add4"></td>
							<td width="20%"><input type="text" name="property-type4"></td>
							<td width="20%"><input type="text" name="property-price4"></td>
							<td width="20%"><input type="text" name="property-year4"></td>
							<td width="20%"><input type="text" name="property-value4"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add5"></td>
							<td width="20%"><input type="text" name="property-type5"></td>
							<td width="20%"><input type="text" name="property-price5"></td>
							<td width="20%"><input type="text" name="property-year5"></td>
							<td width="20%"><input type="text" name="property-value5"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-add6"></td>
							<td width="20%"><input type="text" name="property-type6"></td>
							<td width="20%"><input type="text" name="property-price6"></td>
							<td width="20%"><input type="text" name="property-year6"></td>
							<td width="20%"><input type="text" name="property-value6"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="property-total"></td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td width="20%">Mortgage Holder's Name & Address</td>
							<td width="20%">Account No.</td>
							<td width="20%">Monthly Rental Income</td>
							<td width="20%">Monthly Payment</td>
							<td width="20%">Balance Outstanding</td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder1"></td>
							<td width="20%"><input type="text" name="property-acc1"></td>
							<td width="20%"><input type="text" name="property-rental1"></td>
							<td width="20%"><input type="text" name="property-payment1"></td>
							<td width="20%"><input type="text" name="property-outstanding1"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder2"></td>
							<td width="20%"><input type="text" name="property-acc2"></td>
							<td width="20%"><input type="text" name="property-rental2"></td>
							<td width="20%"><input type="text" name="property-payment2"></td>
							<td width="20%"><input type="text" name="property-outstanding2"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder3"></td>
							<td width="20%"><input type="text" name="property-acc3"></td>
							<td width="20%"><input type="text" name="property-rental3"></td>
							<td width="20%"><input type="text" name="property-payment3"></td>
							<td width="20%"><input type="text" name="property-outstanding3"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder4"></td>
							<td width="20%"><input type="text" name="property-acc4"></td>
							<td width="20%"><input type="text" name="property-rental4"></td>
							<td width="20%"><input type="text" name="property-payment4"></td>
							<td width="20%"><input type="text" name="property-outstanding4"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder5"></td>
							<td width="20%"><input type="text" name="property-acc5"></td>
							<td width="20%"><input type="text" name="property-rental5"></td>
							<td width="20%"><input type="text" name="property-payment5"></td>
							<td width="20%"><input type="text" name="property-outstanding5"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="property-holder6"></td>
							<td width="20%"><input type="text" name="property-acc6"></td>
							<td width="20%"><input type="text" name="property-rental6"></td>
							<td width="20%"><input type="text" name="property-payment6"></td>
							<td width="20%"><input type="text" name="property-outstanding6"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL BALANCES:</b></td>
							<td width="20%"><input type="text" name="property-total"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td><b>Automobiles / RV's / Boats</b></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td width="80%">Type / Year & Model</td>
							<td width="20%">Current Value</td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="auto-type1"></td>
							<td width="20%"><input type="text" name="auto-value1"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="auto-type2"></td>
							<td width="20%"><input type="text" name="auto-value2"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="auto-type3"></td>
							<td width="20%"><input type="text" name="auto-value3"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="auto-type4"></td>
							<td width="20%"><input type="text" name="auto-value4"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="auto-type5"></td>
							<td width="20%"><input type="text" name="auto-value5"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="auto-total"></td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table>
						<tr>
							<td></td>
						</tr>
						<tr>
							<td width="40%">Lender Name & Address</td>
							<td width="20%">Account No.</td>
							<td width="20%">Monthly Payment</td>
							<td width="20%">Balance Outstanding</td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="auto-lender1"></td>
							<td width="20%"><input type="text" name="auto-acc1"></td>
							<td width="20%"><input type="text" name="auto-payment1"></td>
							<td width="20%"><input type="text" name="auto-outstanding1"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="auto-lender2"></td>
							<td width="20%"><input type="text" name="auto-acc2"></td>
							<td width="20%"><input type="text" name="auto-payment2"></td>
							<td width="20%"><input type="text" name="auto-outstanding2"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="auto-lender3"></td>
							<td width="20%"><input type="text" name="auto-acc3"></td>
							<td width="20%"><input type="text" name="auto-payment3"></td>
							<td width="20%"><input type="text" name="auto-outstanding3"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="auto-lender4"></td>
							<td width="20%"><input type="text" name="auto-acc4"></td>
							<td width="20%"><input type="text" name="auto-payment4"></td>
							<td width="20%"><input type="text" name="auto-outstanding4"></td>
						</tr>
						<tr>
							<td width="40%"><input type="text" name="auto-lender5"></td>
							<td width="20%"><input type="text" name="auto-acc5"></td>
							<td width="20%"><input type="text" name="auto-payment5"></td>
							<td width="20%"><input type="text" name="auto-outstanding5"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL BALANCES:</b></td>
							<td width="20%"><input type="text" name="auto-total"></td>
						</tr>
					</table>
				</td>
			</tr>
			<tr>
				<td width="50%">
					<table>
						<tr>
							<td><b>Other Assets</b></td>
						</tr>
						<tr>
							<td width="80%"></td>
							<td width="20%">Current Value</td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item1"></td>
							<td width="20%"><input type="text" name="other-assets-value1"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item2"></td>
							<td width="20%"><input type="text" name="other-assets-value2"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item3"></td>
							<td width="20%"><input type="text" name="other-assets-value3"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item4"></td>
							<td width="20%"><input type="text" name="other-assets-value4"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item5"></td>
							<td width="20%"><input type="text" name="other-assets-value5"></td>
						</tr>
						<tr>
							<td width="80%"><input type="text" name="other-assets-item6"></td>
							<td width="20%"><input type="text" name="other-assets-value6"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL:</b></td>
							<td width="20%"><input type="text" name="other-assets-total"></td>
						</tr>
					</table>
				</td>
				<td width="50%">
					<table>
						<tr>
							<td><b>Other Liabilities</b></td>
						</tr>
						<tr>
							<td width="20%">Type</td>
							<td width="20%">Bank of Lender</td>
							<td width="20%">Account No.</td>
							<td width="20%">Monthly Payment</td>
							<td width="20%">Balance Outstanding</td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type1"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank1"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco1"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym1"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala1"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type2"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank2"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco2"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym2"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala2"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type3"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank3"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco3"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym3"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala3"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type4"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank4"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco4"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym4"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala4"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type5"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank5"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco5"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym5"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala5"></td>
						</tr>
						<tr>
							<td width="20%"><input type="text" name="other-liabilities-type6"></td>
							<td width="20%"><input type="text" name="other-liabilities-bank6"></td>
							<td width="20%"><input type="text" name="other-liabilities-acco6"></td>
							<td width="20%"><input type="text" name="other-liabilities-paym6"></td>
							<td width="20%"><input type="text" name="other-liabilities-bala6"></td>
						</tr>
						<tr>
							<td width="80%"><b>TOTAL BALANCES:</b></td>
							<td width="20%"><input type="text" name="other-liabilities-total"></td>
						</tr>
					</table>
				</td>
			</tr>
			</tbody>
		</table>
		<div class="row">
			<fieldset class="third">
				<label for="total-assets">Total Assets</label>
				<input type="text" name="total-assets">
			</fieldset>
			<fieldset class="third">
				<label for="total-liabilities">Total Liabilities</label>
				<input type="text" name="total-liabilities">
			</fieldset>
			<fieldset class="third">
				<label for="net-worth">Net Worth (Total Assets - Total Liabilities)</label>
				<input type="text" name="net-worth">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="third">
				<label for="bankrupt">Were you ever bankrupt?</label>
				<input type="checkbox" name="bankrupt" value="Yes"> Yes
				<input type="checkbox" name="bankrupt" value="No"> No
			</fieldset>
			<fieldset class="third">
				<label for="bankrupt-year">If yes, year</label>
				<input type="text" name="bankrupt-year">
			</fieldset>
			<fieldset class="third">
				<label for="bankrupt-where">If yes, where</label>
				<input type="text" name="bankrupt-where">
			</fieldset>
		</div>
		<div class="row">
			<fieldset class="third">
				<label for="legal">Are there any judgements, tax liens, or legal proceedings against you?</label>
				<input type="checkbox" name="legal" value="Yes"> Yes
				<input type="checkbox" name="legal" value="No"> No
			</fieldset>
			<fieldset class="otherthird">
				<label for="legal-explain">If yes, explain</label>
				<input type="text" name="legal-explain">
			</fieldset>
		</div>
		<fieldset>
			<label for="sales-rep">Are you currently working with a Sales Representative? If so, please enter their name.</label>
			<input type="text" name="sales-rep">
		</fieldset>
		<fieldset>
			<p>NOTE: Any financial institution or financial company which Applicant or Co-Applicant (or Seller or broker on behalf of Applicant) may apply for financing on the Boat described above is hereby authorized to investigate the credit history of Applicant or Co-Applicant.</p>
			<p>IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT - To help the government fight the funding of terrorism and money laundering activities, federal law requires all financial institutions to obtain, verify, and record information that identifies each person who opens an account. What this means for you: When you open an account, we will ask for your name, address, date of birth, and other information that will allow us to identify you. We may also ask to see your driver's license or other identifying documents.</p>
			<p>I/We make this statement of all my/our assets and liabilities and other material information for the purpose of obtaining credit with you. I/We certify that the information provided in the Personal Financial Statement and Offshore Financial a Division of BankNewport Application for Applied Credit is true and correct and may be relied upon by you. I/We agree to promptly notify you of any material change concerning the information provided herein.</p>
			<div class="applicant-signature">
				<label for="applicant-signature">Signature of Applicant</label>
				<input type="text" name="applicant-signature">
				<label for="applicant-date">Date</label>
				<input type="text" name="applicant-date">
			</div>
			<div class="co-applicant-signature">
				<label for="co-applicant-signature">Signature of Co-Applicant</label>
				<input type="text" name="co-applicant-signature">
				<label for="co-applicant-date">Date</label>
				<input type="text" name="co-applicant-date">
			</div>
		</fieldset>
		<fieldset>
			<input type="submit" value="Submit">
		</fieldset>
	</div>
	
</form>