<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr / Yannis Maragos
*/

defined('_JEXEC') or die;

use Joomla\Registry\Registry;

JLoader::register('FinderIndexerAdapter', JPATH_ADMINISTRATOR . '/components/com_finder/helpers/indexer/adapter.php');

/**
 * Smart Search adapter for com_faqbookpro.
 *
 * @since  2.5
 */
class PlgFinderFAQBookPro extends FinderIndexerAdapter
{
	protected $context = 'FAQBookPro';

	protected $extension = 'com_faqbookpro';

	protected $layout = 'question';

	protected $type_title = 'FAQ Book Question';

	protected $table = '#__minitek_faqbook_questions';

	protected $autoloadLanguage = true;

	public function onFinderAfterDelete($context, $table)
	{
		if ($context == 'com_faqbookpro.question')
		{
			$id = $table->id;
		}
		elseif ($context == 'com_finder.index')
		{
			$id = $table->link_id;
		}
		else
		{
			return true;
		}

		// Remove item from the index.
		return $this->remove($id);
	}

	public function onFinderAfterSave($context, $row, $isNew)
	{
		// We only want to handle questions here.
		if ($context == 'com_faqbookpro.question')
		{
			if (!$isNew && $this->old_access != $row->access)
			{
				// Process the change.
				$this->itemAccessChange($row);
			}

			// Reindex the item.
			$this->reindex($row->id);
		}

		return true;
	}

	public function onFinderBeforeSave($context, $row, $isNew)
	{
		// We only want to handle questions here.
		if ($context == 'com_faqbookpro.question')
		{
			// Query the database for the old access level if the item isn't new.
			if (!$isNew)
			{
				$this->checkItemAccess($row);
			}
		}

		return true;
	}

	public function onFinderChangeState($context, $pks, $value)
	{
		// We only want to handle questions here.
		if ($context == 'com_faqbookpro.question')
		{
			$this->itemStateChange($pks, $value);
		}
		
		// Handle topics change state
		// ...
		
		// Handle sections change state
		// ...
	}

	protected function index(FinderIndexerResult $item, $format = 'html')
	{
		$item->setLanguage();
		
		if ($item->state != '1' || $item->topic_state != '1' || $item->section_state != '1') 
		{
			$this->remove($item->id);
			return;
		}
		
		// Check if the extension is enabled.
		if (JComponentHelper::isEnabled($this->extension) == false)
		{
			return;
		}

		// Initialise the item parameters.
		$registry = new Registry;
		$registry->loadString($item->params);
		$item->params = JComponentHelper::getParams('com_faqbookpro', true);
		$item->params->merge($registry);

		$registry = new Registry;
		$registry->loadString($item->metadata);
		$item->metadata = $registry;

		// Trigger the onContentPrepare event.
		$item->summary = FinderIndexerHelper::prepareContent($item->summary, $item->params);
		$item->body = FinderIndexerHelper::prepareContent($item->body, $item->params);

		// Build the necessary route and path information.
		$item->url = $this->getUrl($item->id, $this->extension, $this->layout);
		$item->route = FaqBookProHelperRoute::getQuestionRoute($item->id, $item->topicid, $item->language);
		$item->path = FinderIndexerHelper::getContentPath($item->route);
		
		// Get the menu title if it exists.
		$title = $this->getItemMenuTitle($item->url);

		// Adjust the title if necessary.
		if (!empty($title) && $this->params->get('use_menu_title', true))
		{
			$item->title = $title;
		}

		// Add the meta author.
		$item->metaauthor = $item->metadata->get('author');

		// Add the metadata processing instructions.
		$item->addInstruction(FinderIndexer::META_CONTEXT, 'metakey');
		$item->addInstruction(FinderIndexer::META_CONTEXT, 'metadesc');
		$item->addInstruction(FinderIndexer::META_CONTEXT, 'metaauthor');
		$item->addInstruction(FinderIndexer::META_CONTEXT, 'author');
		$item->addInstruction(FinderIndexer::META_CONTEXT, 'created_by_alias');

		// Translate the state. Questions should only be published if the topic is published.
		$item->state = $this->translateState($item->state, $item->topic_state);

		// Add the type taxonomy data.
		$item->addTaxonomy('Type', 'FAQ Book Question');

		// Add the author taxonomy data.
		if (!empty($item->author) || !empty($item->created_by_alias))
		{
			$item->addTaxonomy('Author', !empty($item->created_by_alias) ? $item->created_by_alias : $item->author);
		}

		// Add the topic taxonomy data.
		$item->addTaxonomy('FAQ Book Topic', $item->topic, $item->topic_state, $item->topic_access);

		// Add the language taxonomy data.
		$item->addTaxonomy('Language', $item->language);

		// Get content extras.
		FinderIndexerHelper::getContentExtras($item);

		// Index the item.
		$this->indexer->index($item);
	}

	protected function setup()
	{
		// Load dependent classes.
        include_once JPATH_SITE.'/components/com_faqbookpro/helpers/route.php';

		return true;
	}

	protected function getListQuery($query = null)
	{
		$db = JFactory::getDbo();

		// Check if we can use the supplied SQL query.
		$query = $query instanceof JDatabaseQuery ? $query : $db->getQuery(true)
			->select('a.id, a.title, a.alias, a.introtext AS summary, a.introtext AS body')
			->select('a.state, a.topicid, a.created AS start_date, a.created_by')
			->select('a.created_by_alias, a.modified, a.modified_by, a.attribs AS params')
			->select('a.metakey, a.metadesc, a.metadata, a.language, a.access, a.ordering')
			->select('a.publish_up AS publish_start_date, a.publish_down AS publish_end_date')
			->select('c.title AS topic, c.published AS topic_state, c.access AS topic_access')
			->select('s.state AS section_state, s.access AS section_access');

		// Handle the alias CASE WHEN portion of the query
		$case_when_item_alias = ' CASE WHEN ';
		$case_when_item_alias .= $query->charLength('a.alias', '!=', '0');
		$case_when_item_alias .= ' THEN ';
		$a_id = $query->castAsChar('a.id');
		$case_when_item_alias .= $query->concatenate(array($a_id, 'a.alias'), ':');
		$case_when_item_alias .= ' ELSE ';
		$case_when_item_alias .= $a_id . ' END as slug';
		$query->select($case_when_item_alias);

		$case_when_category_alias = ' CASE WHEN ';
		$case_when_category_alias .= $query->charLength('c.alias', '!=', '0');
		$case_when_category_alias .= ' THEN ';
		$c_id = $query->castAsChar('c.id');
		$case_when_category_alias .= $query->concatenate(array($c_id, 'c.alias'), ':');
		$case_when_category_alias .= ' ELSE ';
		$case_when_category_alias .= $c_id . ' END as topicslug';
		$query->select($case_when_category_alias)

			->select('u.name AS author')
			->from('#__minitek_faqbook_questions AS a')
			->join('LEFT', '#__minitek_faqbook_topics AS c ON c.id = a.topicid')
			->join('LEFT', '#__minitek_faqbook_sections AS s ON s.id = c.section_id')
			->join('LEFT', '#__users AS u ON u.id = a.created_by');

		return $query;
	}
	
	protected function itemStateChange($pks, $value)
	{
		/*
		 * The item's published state is tied to the topic
		 * published state so we need to look up all published states
		 * before we change anything.
		 */
		foreach ($pks as $pk)
		{
			$query = clone $this->getStateQuery();
			$query->where('a.id = ' . (int) $pk);

			// Get the published states.
			$this->db->setQuery($query);
			$item = $this->db->loadObject();

			// Translate the state.
			$temp = $this->translateState($value, $item->topic_state);

			// Update the item.
			$this->change($pk, 'state', $temp);

			// Reindex the item
			$this->reindex($pk);
		}
	}
	
	protected function getStateQuery()
	{
		$query = $this->db->getQuery(true);

		// Item ID
		$query->select('a.id');

		// Item and category published state
		$query->select('a.' . $this->state_field . ' AS state, c.published AS topic_state');

		// Item and category access levels
		$query->select('a.access, c.access AS topic_access')
			->from($this->table . ' AS a')
			->join('LEFT', '#__minitek_faqbook_topics AS c ON c.id = a.topicid');

		return $query;
	}
}
