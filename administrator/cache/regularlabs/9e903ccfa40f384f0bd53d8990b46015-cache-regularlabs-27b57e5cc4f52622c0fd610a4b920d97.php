<?php die("Access Denied"); ?>#x#s:1374:"<?xml version="1.0" encoding="utf-8"?>
<extensions>
	<extension>
		<name>Advanced Module Manager</name>
		<id>advancedmodulemanager</id>
		<alias>advancedmodulemanager</alias>
		<extname>advancedmodules</extname>
		<version>7.5.0</version>
		<has_pro>1</has_pro>
		<pro>0</pro>
		<downloadurl>http://download.regularlabs.com/?ext=advancedmodulemanager</downloadurl>
		<downloadurl_pro></downloadurl_pro>
		<changelog><![CDATA[
<div style="font-family:monospace;font-size:1.2em;">19-Jan-2018 : v7.5.0<br /> ^ Changes minimum requirement from Joomla 3.6.0 to Joomla 3.7.0<br /> ^ Updates Mobile Detect Library to 2.8.30<br /> ^ Updates translations: de-DE, nb-NO<br /> ^ [PRO] Updates browser list<br /> # Fixes issue with untranslated language strings on custom module tabs<br /> # Fixes some javascript issues in admin side on Joomla 3.7.0<br /> # [PRO] Fixes issue with browser assignment not working on some Internet Explorer 11 browsers<br /> # [PRO] Fixes issue with incorrect language tags on some HikaShop pagetypes<br /><br />04-Dec-2017 : v7.4.1<br /> ^ Updates translations: ru-RU<br /> # Fixes issue with multi-select fields not showing on some setups<br /> # [PRO] Fixes issue with HikaShop page type assignments still not working correctly<br /></div><div style="text-align: right;"><i>...click for more...</i></div>
		]]></changelog>
	</extension>
</extensions>";