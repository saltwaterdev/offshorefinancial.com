<?php
/**
* @title			Minitek FAQ Book Pro
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProViewEmailTemplate extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null)
	{
		$this->form		= $this->get('Form');
		$this->item		= $this->get('Item');
		$this->state	= $this->get('State');
		$this->canDo	= FAQBookProHelperUtilities::getActions('com_faqbookpro');

		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
	
		// Get Navbar & Sidebar
		$utilities = new FAQBookProHelperUtilities();
		$this->navbar = $utilities->getNavbarHTML();
		$this->sidebar = $utilities->getSideMenuHTML();

		// Get toolbar
		$this->addToolbar();
		
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user		= JFactory::getUser();
		$userId		= $user->get('id');
		$isNew		= ($this->item->id == 0);
		$checkedOut	= !($this->item->checked_out == 0 || $this->item->checked_out == $userId);
		$canDo		= $this->canDo;
		
		JToolbarHelper::title(JText::_('COM_FAQBOOKPRO_EMAIL_TEMPLATE_'.($checkedOut ? 'VIEW_EMAIL_TEMPLATE' : ($isNew ? 'ADD_EMAIL_TEMPLATE' : 'EDIT_EMAIL_TEMPLATE'))), '');

		// Built the actions for new and existing records.

		// For new records, check the create permission.
		if ($isNew && $canDo->get('core.create'))
		{
			JToolbarHelper::apply('emailtemplate.apply');
			JToolbarHelper::save('emailtemplate.save');
			JToolbarHelper::save2new('emailtemplate.save2new');
			JToolbarHelper::cancel('emailtemplate.cancel');
		}
		else
		{
			// Can't save the record if it's checked out.
			if (!$checkedOut)
			{
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId))
				{
					JToolbarHelper::apply('emailtemplate.apply');
					JToolbarHelper::save('emailtemplate.save');

					// We can save this record, but check the create permission to see if we can return to make a new one.
					if ($canDo->get('core.create'))
					{
						JToolbarHelper::save2new('emailtemplate.save2new');
					}
				}
			}

			// If checked out, we can still save
			if ($canDo->get('core.create'))
			{
				JToolbarHelper::save2copy('emailtemplate.save2copy');
			}

			JToolbarHelper::cancel('emailtemplate.cancel', 'JTOOLBAR_CLOSE');
		}

	}
}
