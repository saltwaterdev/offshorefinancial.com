<?php
/**
* @title			Minitek Content Utilities
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');

$app = JFactory::getApplication();
$input = $app->input;
?>

<script type="text/javascript">
	Joomla.submitbutton = function(task)
	{
		if (task == 'emailtemplate.cancel' || document.formvalidator.isValid(document.id('item-form')))
		{
			Joomla.submitform(task, document.getElementById('item-form'));
		}
	}
</script>

<div id="mn-cpanel"><!-- Main Container -->

	<?php echo $this->navbar; ?>
	
	<div id="mn-main-container" class="main-container container-fluid">
		
		<a id="menu-toggler" class="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>

		<div id="mn-sidebar" class="sidebar">
		
			<?php echo $this->sidebar; ?>
					
		</div>
		
		<div class="main-content">
			
			<div class="page-header clearfix no-padding"> </div>
			
			<div class="page-content">
				
				<form action="<?php echo JRoute::_('index.php?option=com_faqbookpro&layout=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="item-form" class="form-validate">
				
					<?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>
				
					<div class="row-fluid">
						<!-- Begin Content -->
						<div class="span12 form-horizontal">
							<?php //echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>
				
								<?php //echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('COM_FAQBOOKPRO_EMAIL_TEMPLATE_DETAILS', true)); ?>
									
									<fieldset class="adminform">
										
										<div class="row-fluid">
											
											<div class="span6">
											
												<div class="control-group form-inline">
													<div class="control-label">
														<?php echo $this->form->getLabel('template_key'); ?> 
													</div>
														<div class="controls">
													<?php echo $this->form->getInput('template_key'); ?> 
													</div>
												</div>
												
												<div class="control-group form-inline">
													<div class="control-label">
														<?php echo $this->form->getLabel('language'); ?> 
													</div>
													<div class="controls">
														<?php echo $this->form->getInput('language'); ?> 
													</div>
												</div>
												
												<div class="control-group form-inline">
													<div class="control-label">
														<?php echo $this->form->getLabel('state'); ?> 
													</div>
													<div class="controls">
														<?php echo $this->form->getInput('state'); ?> 
													</div>
												</div>
																									
												<div class="control-group">
													<div class="control-label">
														<?php echo $this->form->getLabel('id'); ?>
													</div>
													<div class="controls">
														<?php echo $this->form->getInput('id'); ?>
													</div>
												</div>
													
											</div>
											
										</div>
										
										<div class="row-fluid">
											
											<div class="span6">
											
											    <div class="control-group form-inline">
													<div class="control-label">
														<?php echo $this->form->getLabel('title'); ?> 
													</div>
													<div class="controls">
														<?php echo $this->form->getInput('title'); ?> 
													</div>
												</div>
												
												<?php echo $this->form->getLabel('description'); ?> 
												<?php echo $this->form->getInput('description'); ?>
												
											</div>
										
											<div class="span6">
											
											    <div class="control-group form-inline">
													<div class="control-label">
														<?php echo $this->form->getLabel('subject'); ?> 
													</div>
													<div class="controls">
														<?php echo $this->form->getInput('subject'); ?> 
													</div>
												</div>
												
												<?php echo $this->form->getLabel('content'); ?> 
												<?php echo $this->form->getInput('content'); ?>
												
											</div>
											
										</div>
										
										<div class="row-fluid">
											
											<br />
											<div class="alert alert-info">
												<h3>Email Body</h3>
												<br />
												<p>You can only use <strong>inline styling</strong> inside email body. <strong>CSS classes</strong> are not allowed.</p> 
												<p>Available placeholders:</p>
													<p><pre><strong>[NOTIFICATION_DATE]</strong></pre> Date of event</p>
													<p><pre><strong>[RECIPIENT_NAME]</strong></pre> Name of email recipient</p>
													<p><pre><strong>[QUESTION_ID]</strong></pre> Question ID</p>
													<p><pre><strong>[QUESTION_TITLE]</strong></pre> Question title</p>
													<p><pre><strong>[QUESTION_URL]</strong></pre> Question URL</p>
													<p><pre><strong>[AUTHOR_NAME]</strong></pre> Question author / Answer author</p>
													<p><pre><strong>[TOPIC_TITLE]</strong></pre> Topic title</p>
													<p><pre><strong>[TOPIC_URL]</strong></pre> Topic URL</p>
													<p><pre><strong>[SITENAME]</strong></pre> Site name</p>
												<p>Please consult the documentation for more details.</p>
											</div>
											
										</div>
										
									</fieldset>
										
								<?php //echo JHtml::_('bootstrap.endTab'); ?>
												
							<?php //echo JHtml::_('bootstrap.endTabSet'); ?>
				
							<input type="hidden" name="task" value="" />
							<input type="hidden" name="return" value="<?php echo $input->getCmd('return');?>" />
							<?php echo JHtml::_('form.token'); ?>
						</div>
												
					</div>
				</form>
				
			</div><!-- End page-content -->
			
		</div><!-- End main-content -->

	</div><!-- End mcu-main-container -->
	
</div><!-- End Main Container -->

