<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JHtml::addIncludePath(JPATH_COMPONENT.'/helpers/html');

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$columns	= 8;
?>

<script type="text/javascript">
	Joomla.orderTable = function()
	{
		table = document.getElementById("sortTable");
		direction = document.getElementById("directionTable");
		order = table.options[table.selectedIndex].value;
		if (order != '<?php echo $listOrder; ?>')
		{
			dirn = 'asc';
		}
		else
		{
			dirn = direction.options[direction.selectedIndex].value;
		}
		Joomla.tableOrdering(order, dirn, '');
	}
</script>

<div id="mn-cpanel"><!-- Main Container -->
	
	<?php echo $this->navbar; ?>
	
	<div id="mn-main-container" class="main-container container-fluid">
		
		<a id="menu-toggler" class="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>

		<div id="mn-sidebar" class="sidebar">
		
			<?php echo $this->sidebar; ?>
		
		</div>
		
		<div class="main-content">
		
			<div class="page-header clearfix"> </div>
			
			<div class="page-content">
			
				<form action="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=votes'); ?>" method="post" name="adminForm" id="adminForm">
				
					<div id="j-main-container">	
				
						<?php
						// Search tools bar
						echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
						?>
						
						<div class="clearfix"> </div>
						
						<table class="table table-striped" id="articleList">
							<thead>
								<tr>
									<th width="1%" class="hidden-phone">
										<?php echo JHtml::_('grid.checkall'); ?>
									</th>
									<th>
										<?php echo JHtml::_('searchtools.sort', 'COM_FAQBOOKPRO_HEADING_QUESTION_TITLE', 'question_title', $listDirn, $listOrder); ?>
									</th>
									<th width="10%" class="nowrap center hidden-phone">
										<?php echo JHtml::_('searchtools.sort',  'COM_FAQBOOKPRO_HEADING_VOTER', 'author_name', $listDirn, $listOrder); ?>
									</th>
									<th width="10%" class="nowrap center hidden-phone">
										<?php echo JHtml::_('searchtools.sort',  'COM_FAQBOOKPRO_HEADING_VOTER_IP', 'a.user_ip', $listDirn, $listOrder); ?>
									</th>
									<th width="10%" class="nowrap center hidden-phone">
										<?php echo JHtml::_('searchtools.sort', 'JDATE', 'a.creation_date', $listDirn, $listOrder); ?>
									</th>
									<th width="10%" class="center">
										<?php echo JText::_('COM_FAQBOOKPRO_HEADING_VOTE_TYPE'); ?>
									</th>
									<th style="min-width:140px" class="nowrap center hidden-phone">
										<?php echo JHtml::_('searchtools.sort', 'COM_FAQBOOKPRO_HEADING_REASON', 'a.reason', $listDirn, $listOrder); ?>
									</th>
									<th width="1%" class="nowrap center hidden-phone">
										<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
									</th>
								</tr>
							</thead>
							<tbody>
							<?php foreach ($this->items as $i => $item) :
								$item->max_ordering = 0; //??
								$canCreate  = $user->authorise('core.create',     'com_faqbookpro.topic.'.$item->topicid);
								$canEdit    = $user->authorise('core.edit',       'com_faqbookpro.question.'.$item->question_id);
								$canEditOwn = $user->authorise('core.edit.own',   'com_faqbookpro.question.'.$item->question_id) && $item->user_id == $userId;
								$canChange  = $user->authorise('core.edit.state', 'com_faqbookpro.question.'.$item->question_id); //&& $canCheckin;
								?>
								<tr class="row<?php echo $i % 2; ?>">
									
									<td class="center hidden-phone">
										<?php echo JHtml::_('grid.id', $i, $item->id); ?>
									</td>
									
									<td class="nowrap has-context">
										<div class="pull-left">
											<?php if ($canEdit || $canEditOwn) : ?>
												<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&task=question.edit&id=' . $item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>">
													<?php echo $this->escape($item->question_title); ?></a>
											<?php else : ?>
												<span title="<?php echo JText::sprintf('JFIELD_ALIAS_LABEL', $this->escape($item->question_title)); ?>"><?php echo $this->escape($item->question_title); ?></span>
											<?php endif; ?>
											<div class="small">
												<?php echo JText::_('COM_FAQBOOKPRO_TOPIC').": "; ?>
												<a class="hasTooltip" href="<?php echo JRoute::_('index.php?option=com_faqbookpro&task=topic.edit&id=' . $item->topic_id); ?>">
													<?php echo $this->escape($item->topic_title); ?>
												</a>
											</div>
										</div>
									</td>
									
									<td class="small center hidden-phone">
										<?php if ($item->author_name) { ?>
											<a href="<?php echo JRoute::_('index.php?option=com_users&task=user.edit&id='.(int) $item->user_id); ?>" title="<?php echo JText::_('JAUTHOR'); ?>">
											<?php echo $this->escape($item->author_name); ?></a>
										<?php } else { ?>
											<small><?php echo JText::_('COM_FAQBOOKPRO_GUEST'); ?></small>
										<?php } ?>
									</td>
									
									<td class="small center hidden-phone">
										<?php if ($item->user_ip) { ?>
											<?php echo '<small>'.$item->user_ip.'</small>'; ?>
										<?php } else { ?>
											<?php echo '-'; ?>
										<?php } ?>
									</td>
									
									<td class="nowrap small center hidden-phone">
										<?php echo JHtml::_('date', $item->creation_date, JText::_('DATE_FORMAT_LC4')); ?>
									</td>
									
									<td class="center">
										<?php 
										if ($item->vote_up) { 
											echo '<span class="badge badge-success"><i class="fa fa-arrow-up"></i></span>';
										} else {
											echo '<span class="badge badge-important"><i class="fa fa-arrow-down"></i></span>';	
										}
										?>
									</td>
									
									<td class="center hidden-phone">
										<?php if ($item->vote_down) { ?>
											<span class="badge badge-info">
											<?php if ((int)$item->reason == '1') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_1'); ?>
											<?php } else if ((int)$item->reason == '2') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_2'); ?>
											<?php } else if ((int)$item->reason == '3') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_3'); ?>
											<?php } else if ((int)$item->reason == '4') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_4'); ?>
											<?php } else if ((int)$item->reason == '5') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_5'); ?>
											<?php } else if ((int)$item->reason == '6') { ?>
												<?php echo JText::_('COM_FAQBOOKPRO_REASON_6'); ?>
											<?php } ?>
											</span>
										<?php } else { ?>
											<?php echo '-'; ?>
										<?php } ?>
									</td>
									
									<td class="center hidden-phone">
										<?php echo (int) $item->id; ?>
									</td>
									
								</tr>
								<?php endforeach; ?>
							</tbody>
							
						</table>
						
						<?php echo $this->pagination->getListFooter(); ?>
				
						<input type="hidden" name="task" value="" />
						<input type="hidden" name="boxchecked" value="0" />
						<?php echo JHtml::_('form.token'); ?>
						
					</div>
					
				</form>

			</div>
		</div>
	</div>
</div>