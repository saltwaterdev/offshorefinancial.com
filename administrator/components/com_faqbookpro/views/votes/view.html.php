<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProViewVotes extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$this->items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->authors		= $this->get('Authors');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		// Get Navbar & Sidebar
		$utilities = new FAQBookProHelperUtilities();
		$this->navbar = $utilities->getNavbarHTML();
		$this->sidebar = $utilities->getSideMenuHTML();
		
		// Get Toolbar
		$this->addToolbar();
		
		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$user  = JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_FAQBOOKPRO_VOTES'), 'vote.png');

		if ($user->authorise('core.delete', 'com_minitekcontentutilities'))
		{
			JToolbarHelper::custom('votes.remove', 'delete.png', 'delete_f2.png', 'COM_FAQBOOKPRO_DELETE', true);
			JToolbarHelper::custom('votes.purge', 'delete.png', 'delete_f2.png', 'COM_FAQBOOKPRO_PURGE_ALL', false);
		}

		JHtmlSidebar::setAction('index.php?option=com_faqbookpro&view=votes');

	}

	protected function getSortFields()
	{
		return array(
			'question_title' => JText::_('COM_FAQBOOKPRO_QUESTIONS'),
			'a.user_id' => JText::_('JAUTHOR'),
			'a.creation_date' => JText::_('JDATE'),
			'a.id' => JText::_('JGRID_HEADING_ID')
		);
	}
	
}
