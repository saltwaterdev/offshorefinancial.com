<?php
/**
* @title			Minitek FAQ Book Pro
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProViewEmailTemplates extends JViewLegacy
{
	protected $items;
	protected $pagination;
	protected $state;

	public function display($tpl = null)
	{
		$items		= $this->get('Items');
		$this->pagination	= $this->get('Pagination');
		$this->state		= $this->get('State');
		$this->filterForm    = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		$utilities = new FAQBookProHelperUtilities();
		
		// Extra item fields
		foreach ($items as $key=>$item)
		{						
			$this->items[] = $item;
		}
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}

		// Get Navbar & Sidebar
		$this->navbar = $utilities->getNavbarHTML();
		$this->sidebar = $utilities->getSideMenuHTML();
		
		// Get Toolbar
		$this->addToolbar();

		parent::display($tpl);
	}

	protected function addToolbar()
	{
		$canDo = FAQBookProHelperUtilities::getActions('com_faqbookpro');
		$user  = JFactory::getUser();

		// Get the toolbar object instance
		$bar = JToolBar::getInstance('toolbar');

		JToolbarHelper::title(JText::_('COM_FAQBOOKPRO_EMAIL_TEMPLATES'), '');
		
		if ($canDo->get('core.create'))
		{
			JToolbarHelper::addNew('emailtemplate.add');
		}

		if (($canDo->get('core.edit')) || ($canDo->get('core.edit.own')))
		{
			JToolbarHelper::editList('emailtemplate.edit');
		}
		
		if ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::publish('emailtemplates.publish', 'JTOOLBAR_PUBLISH', true);
			JToolbarHelper::unpublish('emailtemplates.unpublish', 'JTOOLBAR_UNPUBLISH', true);
			JToolbarHelper::archiveList('emailtemplates.archive');
			JToolbarHelper::checkin('emailtemplates.checkin');
		}

		if ($this->state->get('filter.published') == -2 && $canDo->get('core.delete'))
		{
			JToolbarHelper::deleteList('', 'emailtemplates.delete', 'JTOOLBAR_EMPTY_TRASH');
		}
		elseif ($canDo->get('core.edit.state'))
		{
			JToolbarHelper::trash('emailtemplates.trash');
		}
		
		JHtmlSidebar::setAction('index.php?option=com_faqbookpro&view=emailtemplates');

	}
}
