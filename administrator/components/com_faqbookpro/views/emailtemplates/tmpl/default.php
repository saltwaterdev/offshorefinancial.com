<?php
/**
* @title			Minitek FAQ Book Pro
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.multiselect');
JHtml::_('formbehavior.chosen', 'select');

$app		= JFactory::getApplication();
$user		= JFactory::getUser();
$userId		= $user->get('id');
$listOrder	= $this->escape($this->state->get('list.ordering'));
$listDirn	= $this->escape($this->state->get('list.direction'));
$archived	= $this->state->get('filter.published') == 2 ? true : false;
$trashed	= $this->state->get('filter.published') == -2 ? true : false;
?>

<div id="mn-cpanel"><!-- Main Container -->

	<?php echo $this->navbar; ?>
	
	<div id="mn-main-container" class="main-container container-fluid">
		
		<a id="menu-toggler" class="menu-toggler" href="#">
			<span class="menu-text"></span>
		</a>

		<div id="mn-sidebar" class="sidebar">
		
			<?php echo $this->sidebar; ?>
		
		</div>
		
		<div class="main-content">
			
			<div class="page-header clearfix no-padding"> </div>
			
			<div class="page-content">
			
				<div class="alert alert-error">
					<span>For multi-language sites, <strong>the last published email template of a particular language</strong> will be used as an email template for that language.</span>
				</div>

				<form action="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=emailtemplates'); ?>" method="post" name="adminForm" id="adminForm">
				
					<div id="j-main-container">
				
						<?php
						// Search tools bar
						echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
						?>
						
						<div class="clearfix"> </div>
				
						<table class="table table-striped" id="articleList">
							<thead>
								<tr>
								
									<th width="1%" class="">
										<?php echo '#'; ?>
									</th>
									
									<th width="1%" class="">
										<?php echo JHtml::_('grid.checkall'); ?>
									</th>
									
									<th width="1%" class="nowrap center">
										<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
									</th>
																		
									<th>
										<?php echo JHtml::_('searchtools.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder); ?>
									</th>
									
									<th>
										<?php echo JHtml::_('searchtools.sort', 'COM_FAQBOOKPRO_HEADING_SUBJECT', 'a.subject', $listDirn, $listOrder); ?>
									</th>
																		
									<th width="20%" class="nowrap center">
										<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_LANGUAGE', 'language', $listDirn, $listOrder); ?>
									</th>
																		
									<th width="1%" class="nowrap center">
										<?php echo JHtml::_('searchtools.sort', 'JGRID_HEADING_ID', 'a.id', $listDirn, $listOrder); ?>
									</th>
									
								</tr>
							</thead>
							<tbody>
							<?php 
							if (count($this->items)) {
							foreach ($this->items as $i => $item) :
								$item->max_ordering = 0; //??
								$ordering   = ($listOrder == 'a.ordering');
								$canCreate  = $user->authorise('core.create');
								$canEdit    = $user->authorise('core.edit',       'com_faqbookpro.emailtemplate.'.$item->id);
								$canCheckin = $user->authorise('core.manage',     'com_checkin') || $item->checked_out == $userId || $item->checked_out == 0;
								$canEditOwn = $user->authorise('core.edit.own',   'com_faqbookpro.emailtemplate.'.$item->id);
								$canChange  = $user->authorise('core.edit.state', 'com_faqbookpro.emailtemplate.'.$item->id) && $canCheckin;
								?>
								<tr class="row<?php echo $i % 2; ?>">
									
									<td class="center">
										<?php echo ( $i + 1 ); ?>
									</td>
									
									<td class="center">
										<?php echo JHtml::_('grid.id', $i, $item->id); ?>
									</td>
									
									<td class="center">
										<div class="btn-group">
											<?php echo JHtml::_('jgrid.published', $item->state, $i, 'emailtemplates.', $canChange, 'cb', '', ''); ?>
										</div>
									</td>
																		
									<td class="nowrap has-context">
										<div class="pull-left">
											<?php if ($item->checked_out) : ?>
												<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'emailtemplates.', $canCheckin); ?>
											<?php endif; ?>
											<?php if ($item->language == '*'):?>
												<?php $language = JText::alt('JALL', 'language'); ?>
											<?php else:?>
												<?php $language = $item->language_title ? $this->escape($item->language_title) : JText::_('JUNDEFINED'); ?>
											<?php endif;?>
											<?php if ($canEdit || $canEditOwn) : ?>
												<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&task=emailtemplate.edit&id=' . $item->id); ?>" title="<?php echo JText::_('JACTION_EDIT'); ?>">
													<?php echo $this->escape($item->title); ?></a>
											<?php else : ?>
												<span><?php echo $this->escape($item->title); ?></span>
											<?php endif; ?>
										</div>
									</td>
									
									<td class="nowrap has-context">
										<?php echo $this->escape($item->subject); ?>
									</td>
																		
									<td class="small center">
										<?php if ($item->language == '*'):?>
											<?php echo JText::alt('JALL', 'language'); ?>
										<?php else:?>
											<?php echo $item->language_title ? $this->escape($item->language_title) : JText::_('JUNDEFINED'); ?>
										<?php endif;?>
									</td>
																		
									<td class="center">
										<?php echo (int) $item->id; ?>
									</td>
								</tr>
							<?php endforeach; 
							} ?>
							</tbody>
						</table>
						<?php echo $this->pagination->getListFooter(); ?>
				
						<input type="hidden" name="task" value="" />
						<input type="hidden" name="boxchecked" value="0" />
						<?php echo JHtml::_('form.token'); ?>
					</div>
				</form>
				
			</div><!-- End page-content -->
			
		</div><!-- End main-content -->

	</div><!-- End mcu-main-container -->
	
</div><!-- End Main Container -->
