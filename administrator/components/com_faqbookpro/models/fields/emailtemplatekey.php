<?php
/**
* @title			Minitek FAQ Book Pro
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldEmailTemplateKey extends JFormFieldList
{
	public $type = 'EmailTemplateKey';

	public function getOptions()
	{
		
		$options = Array( 
			Array(
				'value' => '',
				'text' => JText::_('COM_FAQBOOKPRO_SELECT_EMAIL_TEMPLATE_KEY')
			),
			Array(
				'value' => 'new-question',
				'text' => JText::_('COM_FAQBOOKPRO_EMAIL_TEMPLATE_KEY_NEW_QUESTION')
			),
			Array(
				'value' => 'new-answer',
				'text' => JText::_('COM_FAQBOOKPRO_EMAIL_TEMPLATE_KEY_NEW_ANSWER')
			)
		); 
		
		return $options;
	}
	
}
