<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JFormHelper::loadFieldClass('list');

class JFormFieldVoteType extends JFormFieldList
{
	public $type = 'VoteType';

	public function getOptions()
	{
		
		$options = Array( 
			Array(
				'value' => '',
				'text' => JText::_('COM_FAQBOOKPRO_SELECT_BY_VOTE_TYPE')
			),
			Array(
				'value' => 'vote_up',
				'text' => JText::_('COM_FAQBOOKPRO_VOTE_UP')
			),
			Array(
				'value' => 'vote_down',
				'text' => JText::_('COM_FAQBOOKPRO_VOTE_DOWN')
			)
		); 
		
		return $options;
	}
	
}
