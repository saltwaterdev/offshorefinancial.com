<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class JFormFieldTopicManagerList extends JFormField
{
	public function getInput()
	{
		require_once JPATH_ROOT.'/administrator/components/com_faqbookpro/helpers/utilities.php';

		$html  = '<div style="float:left;width:300px">';

		$all[] = JHTML::_('select.option', 'all', JText::_('COM_FAQBOOKPRO_NOTIFY_ALL_MODERATORS'), 'id', 'name');
		$global[] = JHTML::_('select.option', 'global', JText::_('COM_FAQBOOKPRO_NOTIFY_GLOBAL_MODERATORS'), 'id', 'name');
		$managers = FAQBookProHelperUtilities::getManagers();
		$options  = array_merge($all, $managers);
		$options  = array_merge($global, $options);

		$html .= JHTML::_('select.genericlist', $options, $this->name.'[]', 'class="inputbox" multiple="multiple" size="5"', 'id', 'name', $this->value, $this->id);
		$html .= '</div>';

		return $html;
	}
}