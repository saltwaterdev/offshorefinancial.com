<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProModelVotes extends JModelList
{
	public function __construct($config = array())
	{
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'question_id', 'a.question_id',
				'title', 'c.title', 'question_title',
				'user_id', 'a.user_id',
				'user_ip', 'a.user_ip',
				'vote_up', 'a.vote_up',
				'vote_down', 'a.vote_down',
				'reason', 'a.reason',
				'creation_date', 'a.creation_date',
				'name', 'ua.name', 'author_name',
				'topicid', 'c.topicid',
				'title', 'cc.title', 'topic_title'
			);

		}

		parent::__construct($config);
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		$vote_type = $this->getUserStateFromRequest($this->context . '.filter.vote_type', 'filter_vote_type', '');
		$this->setState('filter.vote_type', $vote_type);

		$topicId = $this->getUserStateFromRequest($this->context . '.filter.topic_id', 'filter_topic_id');
		$this->setState('filter.topic_id', $topicId);

		// List state information.
		parent::populateState('a.id', 'desc');
	}

	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.vote_type');
		$id .= ':' . $this->getState('filter.topic_id');

		return parent::getStoreId($id);
	}

	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id,
				a.question_id,
				c.title,
				cc.title,
				c.topicid,
				a.user_id,
				ua.name,
				a.user_ip,
				a.vote_up,
				a.vote_down,
				a.reason,
				a.creation_date'
			)
		);
		$query->from('#__minitek_faqbook_votes AS a');
		
		// Join over the questions.
		$query->select('c.title AS question_title, c.topicid as topic_id')
			->join('LEFT', '#__minitek_faqbook_questions AS c ON c.id = a.question_id');

		// Join over the users for the author.
		$query->select('ua.name AS author_name')
			->join('LEFT', '#__users AS ua ON ua.id = a.user_id');
			
		// Join over the topics.
		$query->select('cc.title AS topic_title')
			->join('LEFT', '#__minitek_faqbook_topics AS cc ON cc.id = c.topicid');

		// Filter by a single or group of topics.
		$baselevel = 1;
		$topicId = $this->getState('filter.topic_id');

		if (is_numeric($topicId))
		{
			$topic_tbl = JTable::getInstance('Topic', 'FAQBookProTable');
			$topic_tbl->load($topicId);
			$rgt = $topic_tbl->rgt;
			$lft = $topic_tbl->lft;
			$baselevel = (int) $topic_tbl->level;
			$query->where('cc.lft >= ' . (int) $lft)
				->where('cc.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($topicId))
		{
			JArrayHelper::toInteger($topicId);
			$topicId = implode(',', $topicId);
			$query->where('c.topicid IN (' . $topicId . ')');
		}

		// Filter by vote type
		$vote_type = $this->getState('filter.vote_type');
		if ($vote_type == 'vote_up')
		{
			$query->where('a.vote_up = 1');
		}
		if ($vote_type == 'vote_down')
		{
			$query->where('a.vote_down = 1');
		}

		// Filter by search in title.
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
				$search = $db->quote('%' . $db->escape($search, true) . '%');
				$query->where('(c.title LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'c.title');
		$orderDirn = $this->state->get('list.direction', 'asc');
		if ($orderCol == 'question_title')
		{
			$orderCol = 'c.title ' . $orderDirn . ', a.id';
		}
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		return $query;
	}

	public function getAuthors()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);

		// Construct the query
		$query->select('u.id AS value, u.name AS text')
			->from('#__users AS u')
			->join('INNER', '#__minitek_faqbook_votes AS a ON a.user_id = u.id')
			->group('u.id, u.name')
			->order('u.name');

		// Setup the query
		$db->setQuery($query);

		// Return the result
		return $db->loadObjectList();
	}

	public function getItems()
	{
		$items = parent::getItems();
		$app = JFactory::getApplication();
		if ($app->isSite())
		{
			$user = JFactory::getUser();
			$groups = $user->getAuthorisedViewLevels();

			for ($x = 0, $count = count($items); $x < $count; $x++)
			{
				//Check the access level. Remove articles the user shouldn't see
				if (!in_array($items[$x]->access, $groups))
				{
					unset($items[$x]);
				}
			}
		}
		return $items;
	}
	
	public function remove($ids)
	{
		$db = JFactory::getDbo();
		
		foreach ($ids as $voteId)
		{
			$query = $db->getQuery(true);
			$conditions = array(
				$db->quoteName('id') . ' = ' . $db->quote($voteId)
			);
			$query->delete($db->quoteName('#__minitek_faqbook_votes'));
			$query->where($conditions);
			$db->setQuery($query);
			$result = $db->execute();
		}
		
		return true;
	}
	
	public function purge()
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		
		$conditions = array(
			$db->quoteName('id') . ' > 0'
		);
		$query->delete($db->quoteName('#__minitek_faqbook_votes'));
		$query->where($conditions);
		 
		$db->setQuery($query);
		$result = $db->execute();
		
		return true;
	}
}
