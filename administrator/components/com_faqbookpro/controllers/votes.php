<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProControllerVotes extends JControllerAdmin
{
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	public function getModel($name = 'Vote', $prefix = 'FAQBookProModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	protected function postDeleteHook(JModelLegacy $model, $ids = null)
	{
	}
	
	public function remove()
	{
		$ids = $this->input->get('cid', array(), 'array');
		$model = $this->getModel('Votes');
		
		$removeVotes = $model->remove($ids); 
	
		// Redirect after loop end
		$app = JFactory::getApplication();
		
		if ($removeVotes) 
		{
			$message = JText::plural('COM_FAQBOOKPRO_N_ITEMS_DELETED', count($ids));
		} 
		else 
		{
			$message = JText::_('COM_FAQBOOKPRO_DELETE_VOTES_ERROR');
		}
		
		$link = JRoute::_('index.php?option=com_faqbookpro&view=votes', false);
		$app->redirect($link, $message);
	}
	
	public function purge()
	{
		$model = $this->getModel('Votes');
		
		$purgeVotes = $model->purge(); 
		
		// Redirect after loop end
		$app =& JFactory::getApplication();
		
		if ($purgeVotes) 
		{
			$message = JText::_('COM_FAQBOOKPRO_VOTES_SUCCESSFULLY_PURGED');
		} 
		else 
		{
			$message = JText::_('COM_FAQBOOKPRO_DELETE_VOTES_ERROR');
		}
		$link = JRoute::_('index.php?option=com_faqbookpro&view=votes', false);
		$app->redirect($link, $message);
	}

}
