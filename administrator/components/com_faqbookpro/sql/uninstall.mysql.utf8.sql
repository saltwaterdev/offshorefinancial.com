DROP TABLE IF EXISTS `#__minitek_faqbook_questions`;
DROP TABLE IF EXISTS `#__minitek_faqbook_sections`;
DROP TABLE IF EXISTS `#__minitek_faqbook_topics`;
DROP TABLE IF EXISTS `#__minitek_faqbook_votes`;
DROP TABLE IF EXISTS `#__minitek_faqbook_email_templates`;