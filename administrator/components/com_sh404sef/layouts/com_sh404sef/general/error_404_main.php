<?php
/**
 * sh404SEF - SEO extension for Joomla!
 *
 * @author       Yannick Gaultier
 * @copyright    (c) Yannick Gaultier - Weeblr llc - 2018
 * @package      sh404SEF
 * @license      http://www.gnu.org/copyleft/gpl.html GNU/GPL
 * @version      4.13.1.3756
 * @date        2017-12-22
 *
 */

/**
 * $displayData['text'] string the main content of the page
 * $displayData['language_tag'] string the full language page is identified by Joomla
 */

defined('_JEXEC') or die;

echo $displayData['text'];
