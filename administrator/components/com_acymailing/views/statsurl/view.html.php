<?php
/**
 * @package	AcyMailing for Joomla!
 * @version	5.6.0
 * @author	acyba.com
 * @copyright	(C) 2009-2016 ACYBA S.A.R.L. All rights reserved.
 * @license	GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
 */
defined('_JEXEC') or die('Restricted access');
?><?php


class StatsurlViewStatsurl extends acymailingView{
	var $searchFields = array('b.subject', 'a.mailid', 'a.urlid', 'c.name', 'c.url', 'a.click');
	var $selectFields = array('b.subject', 'a.mailid', 'a.urlid', 'c.name', 'c.url', 'COUNT(a.click) as uniqueclick', 'SUM(a.click) as totalclick');
	var $detailSearchFields = array('b.subject', 'a.mailid', 'a.urlid', 'a.subid', 'c.name', 'c.url', 'd.name', 'd.email');
	var $detailSelectFields = array('d.*', 'a.*', 'b.subject', 'c.name as urlname', 'c.url');

	function display($tpl = null){
		$function = $this->getLayout();
		if(method_exists($this, $function)) $this->$function();

		parent::display($tpl);
	}

	function listing(){
		$app = JFactory::getApplication();

		JHTML::_('behavior.modal', 'a.modal');

		$pageInfo = new stdClass();
		$pageInfo->filter = new stdClass();
		$pageInfo->filter->order = new stdClass();
		$pageInfo->limit = new stdClass();
		$pageInfo->elements = new stdClass();

		$paramBase = ACYMAILING_COMPONENT.'.'.$this->getName().$this->getLayout();
		$pageInfo->filter->order->value = $app->getUserStateFromRequest($paramBase.".filter_order", 'filter_order', '', 'cmd');
		$pageInfo->filter->order->dir = $app->getUserStateFromRequest($paramBase.".filter_order_Dir", 'filter_order_Dir', 'desc', 'word');
		if(strtolower($pageInfo->filter->order->dir) !== 'desc') $pageInfo->filter->order->dir = 'asc';
		$pageInfo->search = $app->getUserStateFromRequest($paramBase.".search", 'search', '', 'string');
		$pageInfo->search = JString::strtolower(trim($pageInfo->search));
		$selectedMail = $app->getUserStateFromRequest($paramBase."filter_mail", 'filter_mail', 0, 'int');
		$selectedUrl = $app->getUserStateFromRequest($paramBase."filter_url", 'filter_url', 0, 'int');

		$pageInfo->limit->value = $app->getUserStateFromRequest($paramBase.'.list_limit', 'limit', $app->getCfg('list_limit'), 'int');
		$pageInfo->limit->start = $app->getUserStateFromRequest($paramBase.'.limitstart', 'limitstart', 0, 'int');

		$database = JFactory::getDBO();

		$filters = array();
		if(!empty($pageInfo->search)){
			$searchVal = '\'%'.acymailing_getEscaped($pageInfo->search, true).'%\'';
			$filters[] = implode(" LIKE $searchVal OR ", $this->searchFields)." LIKE $searchVal";
		}

		if(!empty($selectedMail)) $filters[] = 'a.mailid = '.$selectedMail;
		if(!empty($selectedUrl)) $filters[] = 'a.urlid = '.$selectedUrl;

		$query = 'SELECT SQL_CALC_FOUND_ROWS '.implode(' , ', $this->selectFields);
		$query .= ' FROM '.acymailing_table('urlclick').' as a';
		$query .= ' JOIN '.acymailing_table('mail').' as b on a.mailid = b.mailid';
		$query .= ' JOIN '.acymailing_table('url').' as c on a.urlid = c.urlid';
		if(!empty($filters)) $query .= ' WHERE ('.implode(') AND (', $filters).')';
		$query .= ' GROUP BY a.mailid,a.urlid';
		if(!empty($pageInfo->filter->order->value)){
			$query .= ' ORDER BY '.$pageInfo->filter->order->value.' '.$pageInfo->filter->order->dir;
		}

		$database->setQuery($query, $pageInfo->limit->start, $pageInfo->limit->value);
		$rows = $database->loadObjectList();

		$database->setQuery('SELECT FOUND_ROWS()');
		$pageInfo->elements->total = $database->loadResult();

		$pageInfo->elements->page = count($rows);

		jimport('joomla.html.pagination');
		$pagination = new JPagination($pageInfo->elements->total, $pageInfo->limit->start, $pageInfo->limit->value);

		$filtersType = new stdClass();
		$mailType = acymailing_get('type.urlmail');
		$urlType = acymailing_get('type.url');
		$filtersType->mail = $mailType->display('filter_mail', $selectedMail);
		$filtersType->url = $urlType->display('filter_url', $selectedUrl);

		$acyToolbar = acymailing::get('helper.toolbar');
		if(acymailing_level(2)) $acyToolbar->link(acymailing_completeLink('statsurl&task=detaillisting&filter_mail='.$selectedMail.'&filter_url='.$selectedUrl), JText::_('VIEW_DETAILS'), 'export');
		$acyToolbar->custom('exportglobal', JText::_('ACY_EXPORT'), 'export', false, '');
		$acyToolbar->link(acymailing_completeLink('stats'), JText::_('GLOBAL_STATISTICS'), 'cancel');
		$acyToolbar->divider();
		$acyToolbar->help('statsurl-listing');
		$acyToolbar->setTitle(JText::_('CLICK_STATISTICS'), 'statsurl');
		$acyToolbar->display();


		$this->assignRef('filters', $filtersType);
		$this->assignRef('rows', $rows);
		$this->assignRef('pageInfo', $pageInfo);
		$this->assignRef('pagination', $pagination);
	}

	function form(){
		$acyToolbar = acymailing::get('helper.toolbar');
		$acyToolbar->save();
		$acyToolbar->setTitle(JText::_('URL'));
		$acyToolbar->topfixed = false;
		$acyToolbar->display();

		$urlid = acymailing_getCID('urlid');
		$urlClass = acymailing_get('class.url');
		$this->assign('url', $urlClass->get($urlid));
	}

	function detaillisting(){
		require(dirname(__FILE__).DS.'view.detaillisting.php');
	}

	function globalOverview(){
		$app = JFactory::getApplication();
		$mailid = JRequest::getInt('filter_mail');

		if($app->isAdmin() && JRequest::getCmd('tmpl', '') != 'component'){
			$acyToolbar = acymailing::get('helper.toolbar');
			$acyToolbar->link(acymailing_completeLink('statsurl'), JText::_('ACY_CANCEL'), 'cancel');
			$acyToolbar->help('statsurl-overview');
			$acyToolbar->setTitle(JText::_('CLICK_STATISTICS'), 'statsurl&task=globalOverview&filter_mail='.$mailid);
			$acyToolbar->display();
		}

		$mailerHelper = acymailing_get('helper.mailer');
		$mailerHelper->loadedToSend = false;
		$mail = $mailerHelper->load($mailid);

		if(empty($mail)) return;

		$user = JFactory::getUser();
		$userClass = acymailing_get('class.subscriber');
		$receiver = $userClass->get($user->email);
		$mail->sendHTML = true;
		$mailerHelper->dispatcher->trigger('acymailing_replaceusertags', array(&$mail, &$receiver, false));
		if(empty($mail->html)) $mail->body = $mailerHelper->textVersion($mail->altbody, false);

		$query = 'SELECT SQL_CALC_FOUND_ROWS uc.urlid, u.name, u.url, COUNT(uc.click) AS uniqueclick, SUM(uc.click) AS totalclick';
		$query .= ' FROM '.acymailing_table('urlclick').' AS uc';
		$query .= ' JOIN '.acymailing_table('url').' AS u ON uc.urlid = u.urlid';
		$query .= ' WHERE uc.mailid = '.intval($mailid);
		$query .= ' GROUP BY uc.mailid, uc.urlid';
		$query .= ' ORDER BY totalclick DESC';
		$db = JFactory::getDBO();
		$db->setQuery($query);
		$stats = $db->loadObjectList();

		$db->setQuery('SELECT COUNT(click) AS uniqueclick, SUM(click) AS totalclick FROM '.acymailing_table('urlclick').' WHERE mailid = '.intval($mailid));
		$total = $db->loadObject();

		if(!empty($stats)){
			preg_match_all('~<a [^>]*href="([^"]*)"~Uis', $mail->body, $allLinks);
			foreach($allLinks[1] as $oneLink){
				$mail->body = str_replace('href="'.$oneLink.'"', 'href="'.$this->getUrlTrackedForm($oneLink).'"', $mail->body);
			}

			$maxClick = $stats[0]->totalclick;
			$minClick = $stats[count($stats)-1]->totalclick;
			foreach ($stats as $oneLink) {
				$percentage = intval($oneLink->totalclick / $total->totalclick*100);
				$red = $maxClick == $minClick ? 255 : intval((($oneLink->totalclick-$minClick)/($maxClick-$minClick))*255);
				$blue = intval(255-$red);

				$content = acymailing_tooltip(JText::sprintf('ACY_CLICKS_DETAILS', $oneLink->totalclick, $total->totalclick), $percentage.'%', '', $percentage.'<span style="font-size: 7px;">%</span>');
				$bubble = '<div class="overviewbubble" style="background-color:rgba(' . $red . ',0,' . $blue . ',0.5);">'.$content.'</div>';

				if(strpos($mail->body, 'option=com_acymailing&ctrl=url&urlid='.$oneLink->urlid) === false) {
					$mail->body = preg_replace('#(<a [^>]*href="'.preg_quote($oneLink->url).'"[^>]*>)#Uis', '$1'.$bubble, $mail->body);
				}else{
					$mail->body = preg_replace('#(<a [^>]*href="[^"]*option=com_acymailing&ctrl=url&urlid='.$oneLink->urlid.'[^"]*"[^>]*>)#Uis', '$1'.$bubble, $mail->body);
				}
			}
		}

		$this->assignRef('app', $app);
		$this->assignRef('mail', $mail);
		$this->assignRef('stats', $stats);
	}

	function getUrlTrackedForm($currentURL){
		$currentURL = str_replace('&amp;', '&', $currentURL);

		$removeParams = array();
		$removeParams[''] = 'subid|key|acm|utm_source|utm_medium|utm_campaign|token|user\[.{1,10}\]|user_var[0-9]*';
		$removeParams['passw'] = 'passw|user';
		$removeParams['activation'] = 'activation|id|infos';

		foreach($removeParams as $needed => $val){
			if(!empty($needed) && strpos($currentURL,$needed) === false) continue;
			$currentURL = preg_replace('#(\?|&|\/)('.$val.')[=:-][^&\/]*#i','',$currentURL);
		}

		if(!strpos($currentURL,'?') && strpos($currentURL,'&')){
			$firstpos = strpos($currentURL,'&');
			$currentURL = substr($currentURL,0,$firstpos).'?'.substr($currentURL,$firstpos+1);
		}

		return $currentURL;
	}
}
