<?php
/**
* @title			Minitek Wall
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
 
class com_minitekwallInstallerScript
{
	function preflight($type, $parent) 
	{}
	
	function install($parent) 
	{}

	function update($parent) 
	{
		//////////////////////////////////////////////////////////////////
		// Update database tables
		//////////////////////////////////////////////////////////////////
		
		$db = JFactory::getDbo();
										
		// 1. Add rss_source column in __minitek_wall_widgets_source
		$columns = $db->getTableColumns('#__minitek_wall_widgets_source');
		if (!isset($columns['rss_source']))
		{
			$query = $db->getQuery(true);
			$query = " ALTER TABLE `#__minitek_wall_widgets_source` ADD `rss_source` text NOT NULL ";
					
			$db->setQuery($query);
				
			try
			{
				$db->execute();
			}
			catch (Exception $e)
			{
				die('Error mw101');
			}
		}						
	}
	
	function uninstall($parent) 
	{}

	function postflight($type, $parent) 
	{}	
}