<?php
/**
* @title			Minitek Wall
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.application.component.controller');
 
class MinitekWallControllerMasonry extends JControllerLegacy
{
  	function __construct() 
	{		
		parent::__construct();	
	}
		
	public function getContent()
	{
		// Get input
		$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$jinput = $app->input;
		
		// Get variables
		$widget_id = $jinput->get('widget_id', '', 'INT');
		$page = $jinput->get('page', '2', 'INT');
		$grid = $jinput->get('grid', 'masonry', 'STRING');
		
		// Get params
		$model = $this->getModel('Masonry', 'MinitekWallModel');
		$utilities = $model->utilities;
		$masonry_params = $utilities->getMasonryParams($widget_id);
		$layout = $masonry_params['mas_layout'];
		$layout = substr($layout, strpos($layout, ":") + 1);    	
	
		// Set variables
		JRequest::setVar( 'view', 'masonry' );
		JRequest::setVar( 'widget_id', $widget_id );
		JRequest::setVar( 'page', $page );
		
		// Set layout		
		$jinput->set('layout', $layout.'_'.$grid, 'STRING');	

		// Display
		parent::display();	
		
		// Exit
		$app->close();
	}		
}
