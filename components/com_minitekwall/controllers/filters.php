<?php
/**
* @title			Minitek Wall
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
 
jimport('joomla.application.component.controller');
 
class MinitekWallControllerFilters extends JControllerLegacy
{
  	function __construct() 
	{		
		parent::__construct();	
	}
	
	public function getContent()
	{
		// Get input
		$app = JFactory::getApplication();
		$jinput = $app->input;
		
		// Get variables
		$widget_id = $jinput->get('widget_id', '', 'INT');
		$page = $jinput->get('page', '2', 'INT');
	
		// Set variables
		JRequest::setVar( 'view', 'filters' );
		JRequest::setVar( 'widget_id', $widget_id );
		JRequest::setVar( 'page', $page );
		
		// Set layout		
		JRequest::setVar('layout', 'default');
		
		// Display
		parent::display();	
		jexit();
	}		
}
