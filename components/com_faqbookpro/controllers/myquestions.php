<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Add model
JModelLegacy::addIncludePath(JPATH_SITE.'/components/com_faqbookpro/models');

class FAQBookProControllerMyQuestions extends JControllerAdmin
{			
	public function __construct($config = array())
	{
		parent::__construct($config);
	}
		
	public function getModel($name = 'MyQuestion', $prefix = 'FAQBookProModel', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}

	protected function postDeleteHook(JModelLegacy $model, $ids = null)
	{	
		// Delete votes of questions
		$db = JFactory::getDbo();
		
		foreach ($ids as $id)
		{
			$query = $db->getQuery(true);
			
			$conditions = array(
				$db->quoteName('question_id') . ' = ' . $db->quote($id)
			);
			$query->delete($db->quoteName('#__minitek_faqbook_votes'));
			$query->where($conditions);
			 
			$db->setQuery($query);
			$result = $db->execute();	
		}	
	}
	
}
