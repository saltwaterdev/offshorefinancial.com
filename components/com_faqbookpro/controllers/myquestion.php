<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class FAQBookProControllerMyQuestion extends JControllerForm
{
	protected $view_item = 'myquestion';
	protected $view_list = 'section';
	protected $urlVar = 'a.id';
	
	public function __construct($config = array())
	{
		parent::__construct($config);
	}

	protected function allowAdd($data = array())
	{
		$user       = JFactory::getUser();
		$topicId 	= JArrayHelper::getValue($data, 'topicid', $this->input->getInt('topicid'), 'int');
		$allow      = null;

		if ($topicId)
		{
			// If the topic has been passed in the data or URL check it.
			$allow = $user->authorise('core.create', 'com_faqbookpro.topic.' . $topicId);
		}

		if ($allow === null)
		{
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd();
		}
		else
		{
			return $allow;
		}
	}

	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$user = JFactory::getUser();
		$userId = $user->get('id');
		$asset    = 'com_faqbookpro.question.' . $recordId;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset))
		{
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', $asset))
		{
			// Now test the owner is the user.
			$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;

			if (empty($ownerId) && $recordId)
			{
				// Need to do a lookup from the model.
				$record = $this->getModel()->getItem($recordId);

				if (empty($record))
				{
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId)
			{
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}
	
	public function add()
	{
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$context = "$this->option.edit.$this->context";
		$sectionId = $app->input->get('section', '', 'INT');
		
		// Access check.
		if (!$this->allowAdd())
		{
			// Set the internal error and also the redirect error.
			$this->setError(JText::_('COM_FAQBOOKPRO_ERROR_CREATE_QUESTION_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));

			return false;
		}

		// Clear the record edit information from the session.
		$app->setUserState($context . '.data', null);

		// Redirect to question form.
		$this->setRedirect(JRoute::_('index.php?option=com_faqbookpro&view=myquestion&layout=edit&section='.$sectionId, false));

		return true;
	}
	
	public function cancel($key = null)
	{
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app = JFactory::getApplication();
		$model = $this->getModel();
		$table = $model->getTable();
		$user = JFactory::getUser();
		$checkin = property_exists($table, 'checked_out');
		$context = "$this->option.edit.$this->context";

		if (empty($key))
		{
			$key = $table->getKeyName();
		}

		$recordId = $app->input->getInt($key);
		$sectionId = $app->input->get('section', '', 'INT');

		// Attempt to check-in the current record.
		if ($recordId)
		{
			if ($checkin)
			{
				if ($model->checkin($recordId) === false)
				{
					// Check-in failed, go back to the record and display a notice.
					$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()));
					$this->setMessage($this->getError(), 'error');

					$this->setRedirect(JRoute::_(FaqBookProHelperRoute::editQuestionRoute($recordId, $sectionId), false));

					return false;
				}
			}
			
			// Clean the session data and redirect.
			$this->releaseEditId($context, $recordId);
			$app->setUserState($context . '.data', null);
		
			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));
			
			return false;
		}

		// Clean the session data and redirect.
		$this->releaseEditId($context, $recordId);
		$app->setUserState($context . '.data', null);
		
		if (!$user->id)
		{
			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));
		}
		else
		{
			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getMyQuestionsRoute($sectionId), false));	
		}

		return true;
	}
	
	public function edit($key = null, $urlVar = null)
	{
		$app   = JFactory::getApplication();
		$model = $this->getModel();
		$table = $model->getTable();
		$user = JFactory::getUser();
		$cid   = $this->input->post->get('cid', array(), 'array');
		$context = "$this->option.edit.$this->context";
		$sectionId = $app->input->get('section', '', 'INT');

		// Determine the name of the primary key for the data.
		if (empty($key))
		{
			$key = $table->getKeyName();
		}

		// To avoid data collisions the urlVar may be different from the primary key.
		if (empty($urlVar))
		{
			$urlVar = $key;
		}

		// Get the previous record id (if any) and the current record id.
		$recordId = (int) (count($cid) ? $cid[0] : $this->input->getInt($urlVar));
		$checkin = property_exists($table, 'checked_out');

		// Access check.
		if (!$this->allowEdit(array($key => $recordId), $key))
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_EDIT_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');
						
			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));
			
			return false;
		}

		// Attempt to check-out the new record for editing and redirect.
		if ($checkin && !$model->checkout($recordId))
		{
			// Check-out failed
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKOUT_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));

			return false;
		}
		else
		{
			// Check-out succeeded, push the new record id into the session.
			$this->holdEditId($context, $recordId);
			$app->setUserState($context . '.data', null);

			$this->setRedirect(JRoute::_('index.php?option=com_faqbookpro&view=myquestion&layout=edit&id='.$recordId.'&section='.$sectionId, false));

			return true;
		}
	}
	
	public function save($key = null, $urlVar = null)
	{
		// Check for request forgeries.
		JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

		$app   = JFactory::getApplication();
		$lang  = JFactory::getLanguage();
		$model = $this->getModel();
		$table = $model->getTable();
		$user = JFactory::getUser();
		$data  = $this->input->post->get('jform', array(), 'array');
		$checkin = property_exists($table, 'checked_out');
		$context = "$this->option.edit.$this->context";
		$task = $this->getTask();
		$sectionId = $app->input->get('section', '', 'INT');
		$params  = JComponentHelper::getParams('com_faqbookpro');
		
		// Determine the name of the primary key for the data.
		if (empty($key))
		{
			$key = $table->getKeyName();
		}

		// To avoid data collisions the urlVar may be different from the primary key.
		if (empty($urlVar))
		{
			$urlVar = $key;
		}

		$recordId = $this->input->getInt($urlVar);

		// Populate the row id from the session.
		$data[$key] = $recordId;

		// Access check.
		if (!$this->allowSave($data, $key))
		{
			$this->setError(JText::_('JLIB_APPLICATION_ERROR_SAVE_NOT_PERMITTED'));
			$this->setMessage($this->getError(), 'error');
		
			if (!$recordId)
			{
				$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));
			}
			else
			{
				$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));
				
				return false;
			}
		}
	
		// Validate the posted data.
		// Sometimes the form needs some posted data, such as for plugins and modules.
		$form = $model->getForm($data, false);

		if (!$form)
		{
			$app->enqueueMessage($model->getError(), 'error');

			return false;
		}

		// Test whether the data is valid.
		$validData = $model->validate($form, $data);

		// Check for validation errors.
		if ($validData === false)
		{
			// Get the validation messages.
			$errors = $model->getErrors();

			// Push up to three validation messages out to the user.
			for ($i = 0, $n = count($errors); $i < $n && $i < 3; $i++)
			{
				if ($errors[$i] instanceof Exception)
				{
					$app->enqueueMessage($errors[$i]->getMessage(), 'warning');
				}
				else
				{
					$app->enqueueMessage($errors[$i], 'warning');
				}
			}

			// Save the data in the session.
			$app->setUserState($context . '.data', $data);

			// Redirect back to the edit screen.
			$this->setRedirect(JRoute::_('index.php?option=com_faqbookpro&view=myquestion&layout=edit&id='.$recordId.'&section='.$sectionId, false));

			return false;
		}

		// Attempt to save the data.
		$row_id = $model->save($validData);
		if (!$row_id)
		{
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Redirect back to the edit screen.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_SAVE_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');

			$this->setRedirect(JRoute::_('index.php?option=com_faqbookpro&view=myquestion&layout=edit&id='.$recordId.'&section='.$sectionId, false));

			return false;
		}

		// Save succeeded, so check-in the record.
		if ($checkin && $model->checkin($validData[$key]) === false)
		{
			// Save the data in the session.
			$app->setUserState($context . '.data', $validData);

			// Check-in failed, so go back to the record and display a notice.
			$this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_CHECKIN_FAILED', $model->getError()));
			$this->setMessage($this->getError(), 'error');
			
			// Get recordId
			if (!$recordId)
			{
				 $recordId = $row_id;
			}
							
			if ($params->get('auto_publish', 0))
			{
				if (is_int($recordId))
				{
					$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));
				}
				else
				{
					$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));	
				}
			}
			else
			{
				$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));	
			}

			return false;
		}
		
		if ($params->get('auto_publish', 0))
		{
			$this->setMessage(
				JText::_('COM_FAQBOOKPRO_QUESTION_SUCCESSFULLY_SUBMITED')
			);
		}
		else
		{
			$this->setMessage(
				JText::_('COM_FAQBOOKPRO_QUESTION_SUCCESSFULLY_SUBMITED_REVIEW_PENDING')
			);
		}

		// Redirect the user and adjust session state based on the chosen task.
		switch ($task)
		{
			default:
				// Clear the record id and data from the session.
				$this->releaseEditId($context, $recordId);
				$app->setUserState($context . '.data', null);
							
				// Get recordId
				if (!$recordId)
				{
					 $recordId = $row_id;
				}

				if ($params->get('auto_publish', 0))
				{
					if (is_int($recordId))
					{
						$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getQuestionRoute($recordId), false));
					}
					else
					{
						$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));	
					}
				}
				else
				{
					$this->setRedirect(JRoute::_(FaqBookProHelperRoute::getSectionRoute($sectionId), false));	
				}
				
				break;
		}

		// Invoke the postSave method to allow for the child class to access the model.
		$this->postSaveHook($model, $validData);

		return true;
	}
			
	/**
	 * Method to get a model object, loading it if required.
	 *
	 * @param   string  $name    The model name. Optional.
	 * @param   string  $prefix  The class prefix. Optional.
	 * @param   array   $config  Configuration array for model. Optional.
	 *
	 * @return  object  The model.
	 *
	 * @since   1.5
	 */
	public function getModel($name = 'myquestion', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}
	
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
	}
}