<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
  
class FaqBookProControllerQuestion extends JControllerForm 
{
	protected $view_item = 'form';
	protected $view_list = 'section';
	protected $urlVar = 'a.id';
	
 	function __construct() 
	{			
		parent::__construct();	
		
		$this->registerTask('faqThumbsUp', 'ajaxFaqThumbUp');
		$this->registerTask('faqThumbsDown', 'ajaxFaqThumbDown');	
		$this->registerTask('faqVoteReason', 'ajaxFaqVoteReason');	
		$this->registerTask('addHit', 'ajaxAddHit');	
	}
		
	function ajaxFaqThumbUp()
	{
		$this->ajaxFaqRating(1);
	}
		
	function ajaxFaqThumbDown()
	{
		$this->ajaxFaqRating(0);
	}
		
	function ajaxFaqVoteReason()
	{
		$this->ajaxFaqReason();
	}
		
	private function ajaxFaqRating($type)
	{
		$user = JFactory::getUser();
		JSession::checkToken();			
		$id = JRequest::getVar('id',0,'','INT');
			
		if ($id)
		{
			$model = $this->getModel('question');
			$data = $model->FaqVoting($id, $type);
			if ($data)
			{
				echo $data;
			} else {
				$error = $model->getError();		
			 }
		}
			  			
  		jexit();
  	}
		
	private function ajaxFaqReason()
	{
		$user = JFactory::getUser();
		JSession::checkToken();			
		$rid = JRequest::getVar('rid',0,'','INT');
		$fid = JRequest::getVar('fid',0,'','INT');
			
		if ($rid && $fid)
		{
			$model = $this->getModel('question');
			$data = $model->FaqVotingReason($rid, $fid);
			if ($data)
			{
				echo $data;
			} else {
				$error = $model->getError();		
			}
		}
			  			
  		jexit();
  	}
	
	function ajaxAddHit()
	{
		JSession::checkToken();			
		$id = JRequest::getVar('id',0,'','INT');
			
		if ($id)
		{
			$model = $this->getModel('question');
			$data = $model->addHit($id);
			if ($data)
			{
				echo $data;
			} else {
				$error = $model->getError();		
			}
		}
			  			
  		jexit();
  	}
	
	public function add()
	{
		if (!parent::add())
		{
			// Redirect to the return page.
			$this->setRedirect($this->getReturnPage());
		}
	}
	
	protected function allowAdd($data = array())
	{
		$user       = JFactory::getUser();
		$topicId 	= JArrayHelper::getValue($data, 'topicid', $this->input->getInt('topicid'), 'int');
		$allow      = null;

		if ($topicId)
		{
			// If the topic has been passed in the data or URL check it.
			$allow = $user->authorise('core.create', 'com_faqbookpro.topic.' . $topicId);
		}

		if ($allow === null)
		{
			// In the absense of better information, revert to the component permissions.
			return parent::allowAdd();
		}
		else
		{
			return $allow;
		}
	}
	
	protected function allowEdit($data = array(), $key = 'id')
	{
		$recordId = (int) isset($data[$key]) ? $data[$key] : 0;
		$user     = JFactory::getUser();
		$userId   = $user->get('id');
		$asset    = 'com_faqbookpro.question.' . $recordId;

		// Check general edit permission first.
		if ($user->authorise('core.edit', $asset))
		{
			return true;
		}

		// Fallback on edit.own.
		// First test if the permission is available.
		if ($user->authorise('core.edit.own', $asset))
		{
			// Now test the owner is the user.
			$ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;

			if (empty($ownerId) && $recordId)
			{
				// Need to do a lookup from the model.
				$record = $this->getModel()->getItem($recordId);

				if (empty($record))
				{
					return false;
				}

				$ownerId = $record->created_by;
			}

			// If the owner matches 'me' then do the test.
			if ($ownerId == $userId)
			{
				return true;
			}
		}

		// Since there is no asset tracking, revert to the component permissions.
		return parent::allowEdit($data, $key);
	}
	
	public function cancel($key = 'a_id')
	{
		parent::cancel($key);

		// Redirect to the return page.
		$this->setRedirect($this->getReturnPage());
	}
	
	public function edit($key = null, $urlVar = 'a_id')
	{
		$result = parent::edit($key, $urlVar);

		if (!$result)
		{
			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}
	
	public function getModel($name = 'form', $prefix = '', $config = array('ignore_request' => true))
	{
		$model = parent::getModel($name, $prefix, $config);

		return $model;
	}
	
	protected function getRedirectToItemAppend($recordId = null, $urlVar = 'a_id')
	{
		// Need to override the parent method completely.
		$tmpl   = $this->input->get('tmpl');

		$append = '';

		// Setup redirect info.
		if ($tmpl)
		{
			$append .= '&tmpl=' . $tmpl;
		}

		// TODO This is a bandaid, not a long term solution.
		/**
		 * if ($layout)
		 * {
		 *	$append .= '&layout=' . $layout;
		 * }
		 */

		$append .= '&layout=edit';

		if ($recordId)
		{
			$append .= '&' . $urlVar . '=' . $recordId;
		}

		$itemId = $this->input->getInt('Itemid');
		$return = $this->getReturnPage();
		$topicId  = $this->input->getInt('topicid', null, 'get');

		if ($itemId)
		{
			$append .= '&Itemid=' . $itemId;
		}

		if ($topicId)
		{
			$append .= '&topicid=' . $topicId;
		}

		if ($return)
		{
			$append .= '&return=' . base64_encode($return);
		}

		return $append;
	}
	
	protected function getReturnPage()
	{
		$return = $this->input->get('return', null, 'base64');

		if (empty($return) || !JUri::isInternal(base64_decode($return)))
		{
			return JUri::base();
		}
		else
		{
			return base64_decode($return);
		}
	}
	
	protected function postSaveHook(JModelLegacy $model, $validData = array())
	{
		return;
	}
	
	public function save($key = null, $urlVar = 'a_id')
	{
		$result = parent::save($key, $urlVar);

		// If ok, redirect to the return page.
		if ($result)
		{
			$this->setRedirect($this->getReturnPage());
		}

		return $result;
	}
	
}