<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

// Permission to edit
$user = JFactory::getUser();
$userId = $user->get('id');
$question = $displayData['question'];
$sectionId = $displayData['sectionId'];
$params = $displayData['params'];
$utilities = $displayData['utilities'];
$canDo = $utilities->getActions('com_faqbookpro', 'question', $question->id);
?>

<div id="a_w_<?php echo $question->id; ?>">

	<div class="faq_faqAnswerWrapper_inner">
	
		<?php if ($params->question_title) { ?>
			<h2>
				<?php echo $question->title; ?>
				<?php if ($question->featured) { ?>
					<span class="topic_faqFeatured"><?php echo JText::_('COM_FAQBOOKPRO_FEATURED_QUESTION'); ?></span>
				<?php } ?>
			</h2>
		<?php } ?>
		
		<?php if ($params->question_image && $question->image) { ?>
			<div class="faq_image">
				<?php $img = $utilities->resizeImage($params->question_image_width, $params->question_image_height, $question->image, $question->title); ?>
				<img src="<?php echo $img; ?>" alt="<?php echo $question->imageAlt; ?>"/>
			</div>
		<?php } ?>
		
		<?php if ($params->question_description) { ?>
	  		<?php echo $question->finaltext; ?>
		<?php } ?>
	  
		<?php if ($params->question_date || $params->question_author) { ?>
			<div class="faq_extra">
				<?php if ($params->question_date) { ?>
					<span class="faq_date">
						<?php echo JText::_('COM_FAQBOOKPRO_ON'); ?>
						<?php echo JHTML::_('date', $question->created, $params->question_date_format); ?>
					</span>
				<?php } ?>
				<?php if ($params->question_author) { ?>
					<span class="faq_author">
						<span><?php echo JText::_('COM_FAQBOOKPRO_BY'); ?></span>
						<?php if ($displayData['params']->get('questions_author_name', 'name') === 'name') { ?>
							<span><?php echo JFactory::getUser($question->created_by)->name; ?></span>
						<?php } else if ($displayData['params']->get('questions_author_name', 'name') === 'username') { ?>
							<span><?php echo JFactory::getUser($question->created_by)->username; ?></span>
						<?php } ?>
					</span>
				<?php } ?>
			</div>
		<?php } ?>
	  
	  	<?php if ($params->question_voting || ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $question->created_by == $userId))) { ?>
	  		
			<div class="faq_tools clearfix">
			
				<?php if ($params->question_voting) { ?>
					<div class="faq_voting"> 
						<span class="faq_votingQuestion"><?php echo JText::_('COM_FAQBOOKPRO_WAS_THIS_HELPFUL'); ?></span> 	
						<div id="vote_box" class="vote_box"> 
							<div class="thumb-box">
								<a href="#" id="thumbs_up_<?php echo $question->id; ?>" class="thumbs_up" onclick="return false;" rel="nofollow">
									<i></i>
									<span><?php echo $question->question_up_votes; ?></span>
								</a>
							</div>
							<div class="thumb-box">
								<a href="#" id="thumbs_down_<?php echo $question->id; ?>" class="thumbs_down" onclick="return false;" rel="nofollow">
									<i></i>
									<span><?php echo $question->question_down_votes; ?></span>
								</a>
							</div>
						</div>							
					</div>
				<?php } ?>
				
				<?php if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $question->created_by == $userId)) { ?>
					
					<div class="faq_links"> 
					
						<?php if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $question->created_by == $userId)) { ?>
							<a id="faqEditlink_<?php echo $question->id; ?>" href="<?php echo JRoute::_(FaqBookProHelperRoute::editQuestionRoute($question->id, $sectionId)); ?>" rel="nofollow">
								<i class="fa fa-edit"></i>&nbsp;<?php echo JText::_('COM_FAQBOOKPRO_EDIT_QUESTION_LINK'); ?>
							</a> 
						<?php } ?>	
												
					</div>
					
				<?php } ?>
				
	  		</div>
			

	  	<?php } ?>
	  
	</div>

</div>