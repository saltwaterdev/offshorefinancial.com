<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');
?>

<div id="fbpExtended" class="fbpSectionExtended">
  
  	<?php if ($this->sections_topnav) { ?>
  	<div class="fbpTopNavigation_core_outer">
		
		<div class="fbpTopNavigation_core">
		
			<div class="fbpTopNavigation_wrap">
			
				<ul class="fbpTopNavigation_root">
					<li class="NavTopUL_home">
						<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionsRoute($this->home_itemid)); ?>" class="NavTopUL_link">
							<i class="fa fa-home NavTopUL_homeIcon"></i>&nbsp;&nbsp;<?php echo $this->home_title; ?>
						</a>
					</li>
				</ul>
				
			</div>
			
		</div>
		
		<div class="clearfix"> </div>
		
	</div>
	<?php } ?>
	
	<div id="fbpcontent" class="fbpContent_core noleftnav">
		<div class="fbpContent_root">
			<?php echo $this->loadTemplate('content'); ?>
		</div>
	</div>
	
</div>

<div class="clearfix"> </div>