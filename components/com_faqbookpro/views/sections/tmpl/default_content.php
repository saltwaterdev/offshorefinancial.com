<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$user = JFactory::getUser();
?>

<div class="fbpContent_sections">
	
	<?php if ($this->params->get('show_page_title')): ?>
		<h2 class="fbpContent_sections_title">
			<?php echo $this->escape($this->params->get('page_title')); ?>
		</h2>
	<?php endif; ?>
	
	<?php if ($this->sections_page_description && $this->params->get('menu-meta_description')) { ?>
		<div class="fbpContent_sections_desc">
			<?php echo $this->params->get('menu-meta_description'); ?>
		</div>
	<?php } ?>
	
	<?php if ($this->sections) { ?>
		
		<div class="fbp_columns clearfix">
		
			<?php $i = 0;
			foreach ($this->sections as $key=>$section) { ?>
			
				<div class="fbp_column" style="width:<?php echo number_format(100/$this->params->get('sections_cols', 3), 2); ?>%;">
				
					<div class="fbp_column_inner">
					
						<?php if ($this->sections_title) { ?>
							<h3 class="fbp_column_header">
								<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionRoute($section->id)); ?>">
									<?php echo $section->title; ?>
								</a>
							</h3>
						<?php } ?>
						
						<?php if ($this->sections_description) { ?>
							<div class="fbp_column_desc">
								<?php echo $section->description; ?>
							</div>
						<?php } ?>
						
						<?php if ($this->sections_topics && count($section->topics)) { ?>
							<div class="fbp_column_topics"> 
								<?php foreach ($section->topics as $topic) { ?>
									<div class="fbp_column_topic">
										<a href="<?php echo JRoute::_(FAQBookProHelperRoute::getTopicRoute($topic->id)); ?>">
											<?php if ($topic->icon_class) { ?>
												<i class="fa fa-<?php echo $topic->icon_class; ?>"></i>
											<?php } ?>
											<?php echo $topic->title; ?>
										</a>
									</div>
								<?php } ?>
							</div>
						<?php } ?>
						
						<?php if ($this->params->get('user_questions', true) && $this->params->get('sections_question_button', false) && $user->authorise('core.create', 'com_faqbookpro')) { ?>
							<a href="<?php echo JRoute::_(FaqBookProHelperRoute::newQuestionRoute($section->id)); ?>" class="fbpSections_askQuestion btn btn-default"><i class="fa fa-edit"></i>&nbsp;&nbsp;<?php echo JText::_('COM_FAQBOOKPRO_ASK_A_QUESTION'); ?></a>
						<?php } ?>
					
					</div>
				
				</div>
				
				<?php $i++;				
				if (($i)%$this->params->get('sections_cols', 3)==0) { ?>
					<div class="clearfix"> </div>
				<?php }
			
			} ?>
			
		</div>
	<?php } ?>
	
</div>
