<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2017 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

jimport('joomla.application.component.view');
 
class FaqBookProViewSections extends JViewLegacy
{
  	function display($tpl = null) 
  	{
		$document = JFactory::getDocument();
	  	$app = JFactory::getApplication();	
		$this->model = $this->getModel();
		$sectionModel = JModelLegacy::getInstance('Section', 'FaqBookProModel'); 
		$activeMenu = $app->getMenu()->getActive();	
		$this->home_title = $activeMenu->title;
		$this->home_itemid = $activeMenu->id;
	
		// Get Params & Attribs
		$utilities = $this->model->utilities;
		$this->assignRef('utilities', $utilities);
		$params = $utilities->getParams('com_faqbookpro');
		$this->assignRef('params', $params);
		
		// Get Sections				
		$specific_sections = $params->get('fbp_sections', '');
		$this->sections = $this->model->getSections($specific_sections);
		
		// Sections params
		$this->sections_topnav = $params->get('sections_topnav', true);
		$this->sections_page_title = $params->get('sections_page_title', false);	
		$this->sections_page_description = $params->get('sections_page_description', false);	
		$this->sections_cols = $params->get('sections_cols', 3);	
		$this->sections_title = $params->get('sections_title', 1);	
		$this->sections_description = $params->get('sections_description', 1);	
		$this->sections_topics = $params->get('sections_topics', false);	
		
		// Extra Section data
		foreach ($this->sections as $key => $section)
		{
			$section->topics = $sectionModel->getSectionTopics($section->id);
			
			foreach ($section->topics as $topic)
			{
				$topicParams = json_decode($topic->params, false);
				$topic->icon_class = false;
				if (isset($topicParams->topic_icon_class) && $topicParams->topic_icon_class)
				{
					$topic->icon_class = $topicParams->topic_icon_class;
				}
			}
		}
		
		// Set metadata
		$document->setTitle($params->get('page_title'));
		
		if ($params->get('menu-meta_description'))
		{
			$document->setDescription($params->get('menu-meta_description'));
		}
		
		if ($params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $params->get('menu-meta_keywords'));
		}
		
		if ($params->get('robots'))
		{
			$document->setMetadata('robots', $params->get('robots'));
		}
		
		if (!is_object($params->get('metadata')))
		{
			$metadata = new Registry($params->get('metadata'));
		}
		
		$mdata = $metadata->toArray();

		foreach ($mdata as $k => $v)
		{
			if ($v)
			{
				$document->setMetadata($k, $v);
			}
		}
		
		// Menu page display options
		if ($params->get('page_heading'))
		{
		  	$params->set('page_title', $params->get('page_heading'));
		}
		$params->set('show_page_title', $params->get('show_page_heading'));
																									
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}
			  
		// Display the view
		parent::display($tpl);
					
  	}
}