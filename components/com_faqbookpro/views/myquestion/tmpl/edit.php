<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

JHtml::_('bootstrap.tooltip');
JHtml::_('behavior.tabstate');
JHtml::_('behavior.keepalive');
JHtml::_('behavior.calendar');
JHtml::_('behavior.formvalidator');
JHtml::_('formbehavior.chosen', 'select');

$user = JFactory::getUser();
$userId    = $user->get('id');

if (!is_null($this->item->id)) 
{
	JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'myquestion.cancel' || document.formvalidator.isValid(document.getElementById('adminForm')))
			{
				" . $this->form->getField('articletext')->save() . "
				Joomla.submitform(task);
			}
		}
	");
} 
else
{
	JFactory::getDocument()->addScriptDeclaration("
		Joomla.submitbutton = function(task)
		{
			if (task == 'myquestion.cancel' || document.formvalidator.isValid(document.getElementById('adminForm')))
			{
				Joomla.submitform(task);
			}
		}
	");
}
?>

<div id="fbpExtended" class="fbpFormExtended">
  	
	<div class="fbpTopNavigation_core_outer">

		<div class="fbpTopNavigation_core">
		
			<div class="fbpTopNavigation_wrap">
			
				<ul class="fbpTopNavigation_root">
					
					<?php if ($this->params->get('home_link', '0')) { ?>
						<li class="NavTopUL_home">
							<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionsRoute($this->home_itemid)); ?>" class="NavTopUL_link">
								<i class="fa fa-home NavTopUL_homeIcon"></i>&nbsp;&nbsp;<?php echo $this->home_title; ?>
							</a>
						</li>
					<?php } ?>
					
					<li class="NavTopUL_item">
						<span class="NavTopUL_link">
							<?php if ($this->params->get('home_link', '0')) { ?>
								<i class="fa fa-caret-right"></i>&nbsp;
							<?php } ?>
							<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionRoute($this->sectionId)); ?>" class="NavTopUL_link">
								<?php if (!$this->params->get('home_link', '0')) { ?>
									<i class="fa fa-home NavTopUL_homeIcon"></i>&nbsp;
								<?php } ?>
								<?php echo $this->sectionTitle; ?>
							</a>
						</span>
					</li>
					
					<li class="NavTopUL_item">
						<span class="NavTopUL_link">
							<i class="fa fa-caret-right"></i>&nbsp;
							<?php if (!is_null($this->item->id)) { ?>
								<?php echo JText::_('COM_FAQBOOKPRO_FORM_EDIT_QUESTION'); ?>
							<?php } else { ?>
								<?php echo JText::_('COM_FAQBOOKPRO_FORM_NEW_QUESTION'); ?>
							<?php } ?>
						</span>
					</li>
					
				</ul>
			
			</div>
			
			<?php echo $this->getToolbar(); ?>
					
		</div>
		
		<div class="clearfix"> </div>
	
	</div>
	
	<div id="fbpcontent" class="fbpContent_core noleftnav">
	
		<div class="fbpContent_root">
		
			<div class="fbpContent_myquestions"> 
				
				<?php if (!$user->id && $this->params->get('question_guest_message', 1)) { ?>
				<div class="alert alert-warning">
					<?php echo JText::_('COM_FAQBOOKPRO_ANONYMOUS_QUESTION_MESSAGE'); ?>
				</div>
				<?php } ?>
					
				<form action="<?php echo JRoute::_('index.php?option=com_faqbookpro&task=edit&id='.(int) $this->item->id); ?>" method="post" name="adminForm" id="adminForm" class="form-validate form-vertical">
		
					<fieldset>
					
						<ul class="nav nav-tabs">
						
							<li class="active">
								<a href="#editor" data-toggle="tab"><?php echo JText::_('COM_FAQBOOKPRO_QUESTION_CONTENT') ?></a>
							</li>
							
							<?php if ($this->params->get('select_images', '2') == 1 || ($this->params->get('select_images', '2') == 2 && !is_null($this->item->id))) : ?>
								<li>
									<a href="#images" data-toggle="tab"><?php echo JText::_('COM_FAQBOOKPRO_QUESTION_IMAGES') ?></a>
								</li>
							<?php endif; ?>
																			
							<?php if ($this->params->get('question_metadata') == 1 || ($this->params->get('question_metadata') == 2 && !is_null($this->item->id))) : ?>
								<li>
									<a href="#metadata" data-toggle="tab"><?php echo JText::_('COM_FAQBOOKPRO_QUESTION_METADATA') ?></a>
								</li>
							<?php endif; ?>
						</ul>
			
						<div class="tab-content">
						
							<div class="tab-pane active" id="editor">
							
								<?php echo $this->form->renderField('title'); ?>
			
								<?php if (!is_null($this->item->id)) : ?>
									<?php echo $this->form->renderField('alias'); ?>
								<?php endif; ?>
								
								<?php if ($this->params->get('select_topic')) :?>
									<?php echo $this->form->renderField('topicid'); ?>
								<?php endif; ?>
								
								<?php //if ($this->canDo->get('core.edit.state')) : ?>
									<?php if ($this->params->get('auto_publish')) : ?>
										<input type="hidden" name="jform[state]" value="1" />
									<?php endif; ?>
									<?php if (!is_null($this->item->id)) : ?>
										<?php echo $this->form->renderField('featured'); ?>
									<?php endif; ?>
								<?php //endif; ?>
								
								<?php if ($this->params->get('select_access') == 1 || ($this->params->get('select_access') == 2 && !is_null($this->item->id))) : ?>
									<?php echo $this->form->renderField('access'); ?>
								<?php endif; ?>
								
								<?php if ($this->params->get('select_language') == 1 || ($this->params->get('select_language') == 2 && !is_null($this->item->id))) { ?>
									<?php echo $this->form->renderField('language'); ?>
								<?php } else { ?>
									<input type="hidden" name="jform[language]" value="*" />
								<?php } ?>
													
								<?php if (!is_null($this->item->id)) : ?>
									<?php echo $this->form->renderField('articletext'); ?>
								<?php endif; ?>
								
								<?php if ($this->params->get('request_guest_email') && !$userId) : ?>
									<?php echo $this->form->renderField('created_by_email'); ?>
								<?php endif; ?>	
								
								<?php if ($this->params->get('question_captcha')) :?>
									<?php echo $this->form->renderField('captcha'); ?>
								<?php endif; ?>
								
								<?php echo $this->form->renderField('id'); ?>
								
							</div>
							
							<?php if ($this->params->get('select_images', '2') == 1 || ($this->params->get('select_images', '2') == 2 && !is_null($this->item->id))) : ?>
							<div class="tab-pane" id="images">
								<?php echo $this->form->renderField('image_intro', 'images'); ?>
								<?php echo $this->form->renderField('image_intro_alt', 'images'); ?>
								<?php echo $this->form->renderField('image_fulltext', 'images'); ?>
								<?php echo $this->form->renderField('image_fulltext_alt', 'images'); ?>
							</div>
							<?php endif; ?>
							
							<?php if ($this->params->get('question_metadata') == 1 || ($this->params->get('question_metadata') == 2 && !is_null($this->item->id))) : ?>
							<div class="tab-pane" id="metadata">
								<?php echo $this->form->renderField('metadesc'); ?>
								<?php echo $this->form->renderField('metakey'); ?>
							</div>
							<?php endif; ?>
							
							<input type="hidden" name="task" value="" />
							<input type="hidden" name="return" value="" />
							<input type="hidden" name="section" value="<?php echo $this->sectionId; ?>" />
							
							<?php if (!$this->params->get('select_topic')) : ?>
								<input type="hidden" name="jform[topicid]" value="<?php echo $this->params->get('static_topic'); ?>" />
							<?php endif; ?>
							
							<?php if (!$this->params->get('auto_publish') && is_null($this->item->id)) : ?>
								<input type="hidden" name="jform[state]" value="0" />
							<?php endif; ?>
							
						</div>
						
						<?php echo JHtml::_('form.token'); ?>
						
					</fieldset>
										
				</form>
			
			</div>
			
		</div>
		
	</div>
		
</div>

<div class="clearfix"> </div>