<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

class FAQBookProViewMyQuestion extends JViewLegacy
{
	protected $form;
	protected $item;
	protected $state;

	public function display($tpl = null)
	{
		$app = JFactory::getApplication();	
		$this->model = $this->getModel();
		$this->utilities = $this->model->utilities;
		$menu = $app->getMenu();
		$activeMenu = $menu->getActive();
		
		$this->form  = $this->get('Form');
		$this->item  = $this->get('Item');
		$this->state = $this->get('State');
		$this->canDo = $this->utilities->getActions('com_faqbookpro', 'question', $this->item->id);
		$user = JFactory::getUser();
		$userId = $user->get('id');
		
		// Create a shortcut to the parameters.
		$params = &$this->state->params;
		$this->params = $params;
		
		// Get section
		$this->sectionId = $app->input->get('section', '', 'INT');
		if (!$this->sectionId)
		{
			// Get first section
			$db = JFactory::getDBO();
			$query = 'SELECT id, title FROM '. $db->quoteName( '#__minitek_faqbook_sections' );
			$query .= ' WHERE ' . $db->quoteName( 'state' ) . ' = '. $db->quote('1').' ';
			$db->setQuery($query);
			$sectionId = $db->loadObject()->id;
			
			$app->input->set('section', $sectionId);
			$this->sectionId = $app->input->get('section', '', 'INT');
		}
		
		// Get section title/attribs
		$sectionModel = JModelLegacy::getInstance('Section', 'FAQBookProModel'); 
		$this->sectionTitle = $sectionModel->getSection($this->sectionId)->title;
		$sectionAttribs = json_decode($sectionModel->getSection($this->sectionId)->attribs, false);
		$this->home_itemid = isset($sectionAttribs->topnav_root) ? $sectionAttribs->topnav_root : $activeMenu->id;;
		$home_menuitem = $menu->getItem($this->home_itemid);
		$this->home_title = $home_menuitem->title;
		
		// Check for existing question.
		if (!empty($this->item->id))
		{
			$questionAuthor = $this->model->getQuestion($this->item->id)->created_by;
	
			if ($this->params->get('user_questions', '1') 
			&& ($user->authorise('core.edit', 'com_faqbookpro.question.' . (int) $this->item->id) || ($user->authorise('core.edit.own', 'com_faqbookpro.question.' . (int) $this->item->id) && $questionAuthor == $userId)))
			{
			}
			else
			{
				JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
	
				return false;
			}
		}
		// New question, so check against the global permissions.
		else
		{
			if (!$user->authorise('core.create', 'com_faqbookpro'))
			{
				JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
	
				return false;
			}
		}
		
		// Propose current language as default when creating new question
		if (JLanguageMultilang::isEnabled() && empty($this->item->id))
		{
			$lang = JFactory::getLanguage()->getTag();
			$this->form->setFieldAttribute('language', 'default', $lang);
		}
		
		// Bind images
		if (!empty($this->item) && isset($this->item->id))
		{
			$tmp = new stdClass;
			$tmp->images = $this->item->images;
			$this->form->bind($tmp);
		}
		
		// Check for errors.
		if (count($errors = $this->get('Errors')))
		{
			JError::raiseError(500, implode("\n", $errors));
			return false;
		}
		
		// Get Toolbar
		$this->getToolbar();
		
		parent::display($tpl);
	}

	protected function getToolbar()
	{
		jimport('cms.html.toolbar');
		$bar = new JToolBar( 'toolbar' );
		
		JFactory::getApplication()->input->set('hidemainmenu', true);
		$user       = JFactory::getUser();
		$userId     = $user->get('id');
		$isNew      = ($this->item->id == 0);
		$checkedOut = !($this->item->checked_out == 0 || $this->item->checked_out == $userId);

		// Built the actions for new and existing records.
		$canDo = $this->canDo;

		// For new records, check the create permission.
		if ($isNew && (count($this->utilities->getAuthorisedTopics('core.create')) > 0))
		{
			//$bar->appendButton( 'Standard', 'apply', 'COM_FAQBOOKPRO_MYQUESTION_APPLY', 'myquestion.apply', false );
			$bar->appendButton( 'Standard', 'apply', 'COM_FAQBOOKPRO_MYQUESTION_SAVE', 'myquestion.save', false );
			$bar->appendButton( 'Standard', 'cancel', 'COM_FAQBOOKPRO_MYQUESTION_CANCEL', 'myquestion.cancel', false );
		}
		else
		{
			// Can't save the record if it's checked out.
			if (!$checkedOut)
			{
				// Since it's an existing record, check the edit permission, or fall back to edit own if the owner.
				if ($canDo->get('core.edit') || ($canDo->get('core.edit.own') && $this->item->created_by == $userId))
				{
					//$bar->appendButton( 'Standard', 'apply', 'COM_FAQBOOKPRO_MYQUESTION_APPLY', 'myquestion.apply', false );
					$bar->appendButton( 'Standard', 'apply', 'COM_FAQBOOKPRO_MYQUESTION_SAVE', 'myquestion.save', false );
				}
			}

			$bar->appendButton( 'Standard', 'cancel', 'COM_FAQBOOKPRO_MYQUESTION_CANCEL', 'myquestion.cancel', false );
		}
		
		// Generate the html and return
		return $bar->render();
	}
}