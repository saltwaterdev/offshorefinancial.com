<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.application.component.view');

class FAQBookProViewMyQuestions extends JViewLegacy
{
	function display($tpl = null) 
	{	
		$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$this->model = $this->getModel();
		$menu = $app->getMenu();
		$activeMenu = $menu->getActive();

		$this->utilities = $this->model->utilities;
		$params = $this->utilities->getParams('com_faqbookpro');
		$this->assignRef('params', $params);
		$this->user = JFactory::getUser();
	
		if (!$this->user->authorise('core.create', 'com_faqbookpro') || !$params->get('user_questions', '1'))
		{
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));

			return false;
		}

		// Get section
		$this->sectionId = $app->input->get('section', '', 'INT');
		if (!$this->sectionId)
		{
			// Get first section
			$db = JFactory::getDBO();
			$query = 'SELECT id, title FROM '. $db->quoteName( '#__minitek_faqbook_sections' );
			$query .= ' WHERE ' . $db->quoteName( 'state' ) . ' = '. $db->quote('1').' ';
			$db->setQuery($query);
			$sectionId = $db->loadObject()->id;
			
			$app->input->set('section', $sectionId);
			$this->sectionId = $app->input->get('section', '', 'INT');
		}
		
		// Get section title/attribs
		$sectionModel = JModelLegacy::getInstance('Section', 'FAQBookProModel'); 
		$this->sectionTitle = $sectionModel->getSection($this->sectionId)->title;
		$sectionAttribs = json_decode($sectionModel->getSection($this->sectionId)->attribs, false);
		$this->home_itemid = isset($sectionAttribs->topnav_root) ? $sectionAttribs->topnav_root : $activeMenu->id;;
		$home_menuitem = $menu->getItem($this->home_itemid);
		$this->home_title = $home_menuitem->title;
		
		$leftnav = 0; // For javascript purposes
		$topicId = 0;
		
		// Get Items
		$this->items = $this->get('Items');
		$this->pagination = $this->get('Pagination');
		$this->state = $this->get('State');
		$this->filterForm = $this->get('FilterForm');
		$this->activeFilters = $this->get('ActiveFilters');
		
		// Check for errors.
		if (count($errors = $this->get('Errors'))) {
			JError::raiseError(500, implode('<br />', $errors));
			return false;
		}

		if (!$activeMenu)
		{
			return JError::raiseError(404, JText::_('COM_FAQBOOKPRO_PAGE_NOT_FOUND'));	
		}
						
		// Display the view
		parent::display($tpl);
		
	}
	
	protected function getToolbar()
	{
		jimport('cms.html.toolbar');
		$canDo = $this->utilities->getActions('com_faqbookpro', 'topic', $this->state->get('filter.topic_id'));
	
		$bar = new JToolBar( 'toolbar' );
		
		if ($canDo->get('core.delete') && $this->user->id) 
		{
			$bar->appendButton( 'Standard', 'trash', 'COM_FAQBOOKPRO_DELETE_QUESTION', 'myquestions.delete', true );
		}
		
		if ($canDo->get('core.edit.state') && $this->user->id) 
		{
			$bar->appendButton( 'Standard', 'unpublish', 'COM_FAQBOOKPRO_UNPUBLISH_QUESTION', 'myquestions.unpublish', true );
			$bar->appendButton( 'Standard', 'publish', 'COM_FAQBOOKPRO_PUBLISH_QUESTION', 'myquestions.publish', true );
		}
		
		if ((($canDo->get('core.edit')) || ($canDo->get('core.edit.own'))) && $this->user->id) 
		{
			$bar->appendButton( 'Standard', 'edit', 'COM_FAQBOOKPRO_EDIT_QUESTION', 'myquestion.edit', true );
		}
		
		if ($canDo->get('core.create') || (count($this->utilities->getAuthorisedTopics('core.create'))) > 0 ) 
		{
			$bar->appendButton( 'Standard', 'new', 'COM_FAQBOOKPRO_NEW_QUESTION', 'myquestion.add', false );
		}
		
		//generate the html and return
		return $bar->render();
	}
	
}