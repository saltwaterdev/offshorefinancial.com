<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

JHtml::_('bootstrap.tooltip');
?>

<div id="fbpExtended" class="fbpFormExtended">

	<div class="fbpTopNavigation_core_outer">

		<div class="fbpTopNavigation_core">
		
			<div class="fbpTopNavigation_wrap">
			
				<ul class="fbpTopNavigation_root">
					
					<?php if ($this->params->get('home_link', '0')) { ?>
						<li class="NavTopUL_home">
							<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionsRoute($this->home_itemid)); ?>" class="NavTopUL_link">
								<i class="fa fa-home NavTopUL_homeIcon"></i>&nbsp;&nbsp;<?php echo $this->home_title; ?>
							</a>
						</li>
					<?php } ?>
					
					<li class="NavTopUL_item">
						<span class="NavTopUL_link">
							<?php if ($this->params->get('home_link', '0')) { ?>
								<i class="fa fa-caret-right"></i>&nbsp;
							<?php } ?>
							<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getSectionRoute($this->sectionId)); ?>" class="NavTopUL_link">
								<?php if (!$this->params->get('home_link', '0')) { ?>
									<i class="fa fa-home NavTopUL_homeIcon"></i>&nbsp;
								<?php } ?>
								<?php echo $this->sectionTitle; ?>
							</a>
						</span>
					</li>
					
					<li class="NavTopUL_item">
						<span class="NavTopUL_link">
							<i class="fa fa-caret-right"></i>&nbsp;
							<?php echo JText::_('COM_FAQBOOKPRO_MY_QUESTIONS'); ?>
						</span>
					</li>
				
				</ul>
			
			</div>
			
			<?php echo $this->getToolbar(); ?>
					
		</div>
		
		<div class="clearfix"> </div>
	
	</div>
		
	<div id="fbpcontent" class="fbpContent_core noleftnav">
		
		<div class="fbpContent_root">
					
			<div class="fbpContent_myquestions"> 
				
				<?php // Main content 			
				$listOrder	= $this->escape($this->state->get('list.ordering'));
				$listDirn	= $this->escape($this->state->get('list.direction'));
				?>
									
				<form action="<?php echo JRoute::_('index.php?option=com_faqbookpro&view=myquestions&section='.$this->sectionId); ?>" method="post" name="adminForm" id="adminForm">
					
					<div id="j-main-container">
						
						<?php if ($this->user->id) { ?>
						
							<?php
							// Search tools bar
							echo JLayoutHelper::render('joomla.searchtools.default', array('view' => $this));
							?>
											
							<table class="table table-responsive" id="articleList">
								<thead>
									<tr>
																	
										<th width="1%" class="">
											<?php echo JHtml::_('grid.checkall'); ?>
										</th>
										
										<th width="5%" class="nowrap center">
											<?php echo JHtml::_('searchtools.sort', 'JSTATUS', 'a.state', $listDirn, $listOrder); ?>
										</th>
										
										<th>
											<?php echo JHtml::_('searchtools.sort', 'JGLOBAL_TITLE', 'a.title', $listDirn, $listOrder); ?>
										</th>
										
										<th width="20%" class="nowrap hidden-phone">
											<?php echo JHtml::_('searchtools.sort', 'COM_FAQBOOKPRO_HEADING_TOPIC', 'topic_title', $listDirn, $listOrder); ?>
										</th>
										
										<th width="12%" class="nowrap center hidden-phone">
											<?php echo JHtml::_('searchtools.sort',  'COM_FAQBOOKPRO_HEADING_VOTES_UP', 'votes_up', $listDirn, $listOrder); ?>
										</th>
										
										<th width="12%" class="nowrap center hidden-phone">
											<?php echo JHtml::_('searchtools.sort',  'COM_FAQBOOKPRO_HEADING_VOTES_DOWN', 'votes_down', $listDirn, $listOrder); ?>
										</th>
																											
										<th width="20%" class="nowrap center">
											<?php echo JHtml::_('searchtools.sort', 'JDATE', 'a.created', $listDirn, $listOrder); ?>
										</th>
																																		
									</tr>
								</thead>
								<tbody>
								<?php 
								if (isset($this->items)) {
								foreach ($this->items as $i => $item) :
									$ordering   = ($listOrder == 'a.created');
									$canCheckin = $this->user->authorise('core.manage',     'com_checkin') || $item->checked_out == $this->user->id || $item->checked_out == 0;
									$canChange  = $this->user->authorise('core.edit.state', 'com_faqbookpro.question.' . $item->id) && $canCheckin;
									$canEdit    = $this->user->authorise('core.edit',       'com_faqbookpro.question.' . $item->id);
									$canEditOwn = $this->user->authorise('core.edit.own',   'com_faqbookpro.question.' . $item->id) && $item->created_by == $this->user->id;
									?>
									<tr class="row<?php echo $i % 2; ?>">
																		
										<td class="center">
											<?php echo JHtml::_('grid.id', $i, $item->id); ?>
										</td>
										
										<td class="center">
											<div class="btn-group">
												<?php echo JHtml::_('jgrid.published', $item->state, $i, 'myquestions.', $canChange, 'cb', $item->publish_up, $item->publish_down); ?>
											</div>
										</td>
										
										<td class="nowrap has-context">
											<div class="pull-left">
												<?php if ($item->checked_out) : ?>
													<?php echo JHtml::_('jgrid.checkedout', $i, $item->editor, $item->checked_out_time, 'myquestions.', $canCheckin); ?>
												<?php endif; ?>
												<?php if ($canEdit || $canEditOwn) : ?>
													<a href="<?php echo JRoute::_('index.php?option=com_faqbookpro&task=myquestion.edit&id=' . $item->id); ?>">
														<?php echo $this->escape($item->title); ?>
													</a>
												<?php else : ?>
													<span><?php echo $this->escape($item->title); ?></span>
												<?php endif; ?>	
												<?php if ($item->state == 1) { ?>
													<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getQuestionRoute($item->id, $item->topicid)); ?>" title="<?php echo JText::_('COM_FAQBOOKPRO_GO_TO_QUESTION'); ?>" target="_blank">
														&nbsp;<small><i class="fa fa-external-link"></i></small>
													</a>	
												<?php } ?>	
											</div>
										</td>
										
										<td>
											<a href="<?php echo JRoute::_(FaqBookProHelperRoute::getTopicRoute($item->topicid)); ?>" target="_blank">
												<?php echo $this->escape($item->topic_title); ?>
												&nbsp;<small><i class="fa fa-external-link"></i></small>
											</a>
										</td>
										
										<td class="small center hidden-phone">
											<?php echo (int) $item->votes_up; ?>
										</td>
										
										<td class="small center hidden-phone">
											<?php echo (int) $item->votes_down; ?>
										</td>
																											
										<td class="nowrap small center">
											<?php echo JHtml::_('date', $item->created, 'Y-m-d H:i:s'); ?>
										</td>
																										
									</tr>
								<?php endforeach; 
								} ?>
								</tbody>
							</table>
							<?php echo $this->pagination->getListFooter(); ?>
						
						<?php } else { ?>
				
							<div class="alert alert-danger">
								<?php echo JText::_('COM_FAQBOOKPRO_PLEASE_LOGIN_TO_MANAGE_YOUR_QUESTIONS'); ?>
							</div>
						
						<?php } ?>
				
						<input type="hidden" name="task" value="" />
						<input type="hidden" name="boxchecked" value="0" />
						<input type="hidden" name="section" value="<?php echo $this->sectionId; ?>" />
						
						<?php echo JHtml::_('form.token'); ?>
					</div>
					
				</form>
										
			</div>
			
		</div>
		
	</div>
		
</div>

<div class="clearfix"> </div>