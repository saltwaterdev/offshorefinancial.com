<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

$data = array();
$data['params'] = $this->params;
$data['question'] = $this->question;
$data['sectionId'] = $this->sectionId;
$data['utilities'] = $this->utilities;

$layout = new JLayoutFile('fbp_question');
echo $layout->render($data);