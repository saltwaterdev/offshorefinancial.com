<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2016 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

use Joomla\Registry\Registry;

jimport('joomla.application.component.view');
 
class FaqBookProViewQuestion extends JViewLegacy
{
  	function display($tpl = null) 
  	{
		JPluginHelper::importPlugin('content');
	  	$document = JFactory::getDocument();
		$app = JFactory::getApplication();
		$user = JFactory::getUser();
		$questionId = $app->input->get('id', false, 'INT');
		$this->model = $this->getModel();
		$activeMenu = $app->getMenu()->getActive();	
		
		if (!$activeMenu)
		{
			return JError::raiseError(404, JText::_('COM_FAQBOOKPRO_PAGE_NOT_FOUND'));	
		}
		
		if (empty($questionId) || !$this->model->getQuestion($questionId)) 
		{ 
			JError::raiseError(404, JText::_('COM_FAQBOOKPRO_ERROR_QUESTION_NOT_FOUND'));
		}
		
		// Get access levels
		$authorised = $this->model->authorizeQuestion($questionId);
		if (!$authorised)
		{
			JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
	
			return false;
		}	
		
		$question = $this->model->getQuestion($questionId);
		$this->assignRef('question', $question);
		$topicId = $question->topicid;
		$topicModel = JModelLegacy::getInstance('Topic', 'FAQBookProModel'); 
		$this->sectionId = $this->model->getQuestionSection($topicId);
		$sectionModel = JModelLegacy::getInstance('Section', 'FAQBookProModel'); 
		$this->section = $sectionModel->getSection($this->sectionId);
		
		// Get Params & Attribs
		$sectionAttribs = json_decode($this->section->attribs, false);
		$utilities = $this->model->utilities;
		$this->assignRef('utilities', $utilities);
		$this->params = $utilities->getParams('com_faqbookpro');
		$questionParams = json_decode($question->attribs, false);
		
		// Check if we have an active topic
		$this->active_topic = 0;
		if (isset($sectionAttribs->show_active_topic) && $sectionAttribs->show_active_topic 
		&& isset($sectionAttribs->topicid) && $sectionAttribs->topicid)
		{
			$this->active_topic = $sectionAttribs->topicid;
		}
	
		// Images
		$question->images = json_decode($question->images, true);
		$question->image = $question->images['image_fulltext'];
		$question->imageAlt = '';
		if (array_key_exists('image_fulltext_alt', $question->images)) 
		{
			$question->imageAlt = $question->images['image_fulltext_alt'];
		}
		
		// Votes
		$question->question_up_votes = $topicModel->getQuestionVotes($question->id, $type = 'vote_up');
		$question->question_down_votes = $topicModel->getQuestionVotes($question->id, $type = 'vote_down');
		
		// Text
		$question->finaltext = $question->introtext.' '.$question->fulltext;
		$question->finaltext = JHtml::_('content.prepare', $question->finaltext);
		
		// Add hit
		$this->model->addHit($question->id);
		
		$navigation = $this->model->navigation;
		
		// Get Top Navigation
		if (isset($sectionAttribs->topnav))
		{
			$topnav = $sectionAttribs->topnav;
		}
		else
		{
			$topnav = true;	
		}
		$topnavigation = false;
		
		if ($topnav)
		{
			$topnavigation = $navigation->getTopNav($this->sectionId);
		}
		$this->assignRef('topnavigation', $topnavigation);
		
		// Get Left Navigation
		$leftnav = $sectionAttribs->leftnav;
		$leftnavigation = $navigation->getLeftNav($this->sectionId);
		
		if ($leftnav)
		{
		  	$content_class = 'fbpContent_core';
		} 
		else 
		{
		  	$content_class = 'fbpContent_core noleftnav';
		}
		
		$this->assignRef('content_class', $content_class);
		$this->assignRef('leftnavigation', $leftnavigation);
		
		// Load Endpoint Topics / All Topics
		$this->loadAllTopics = 1;
		if (isset($sectionAttribs->load_all_topics))
		{
			$this->loadAllTopics = $sectionAttribs->load_all_topics;
		}
		
		// Question title
		if (!isset($questionParams) || $questionParams->question_title == '')
		{
			$this->params->question_title = $this->params->get('question_title', '1');
		}
		else
		{
			$this->params->question_title = $questionParams->question_title;
		}
		
		// Question description
		if (!isset($questionParams) || $questionParams->question_description == '')
		{
			$this->params->question_description = $this->params->get('question_description', '1');
		}
		else
		{
			$this->params->question_description = $questionParams->question_description;
		}
		
		// Question image

		if (!isset($questionParams) || $questionParams->question_image == '')
		{
			$this->params->question_image = $this->params->get('question_image', '1');
		}
		else
		{
			$this->params->question_image = $questionParams->question_image;
		}
		
		// Question image width
		if (!isset($questionParams) || !isset($questionParams->question_image_width) || empty($questionParams->question_image_width))
		{
			$this->params->question_image_width = $this->params->get('question_image_width', '300');
		}
		else
		{
			$this->params->question_image_width = $questionParams->question_image_width;
		}
		
		// Question image height
		if (!isset($questionParams) || !isset($questionParams->question_image_height) || empty($questionParams->question_image_height))
		{
			$this->params->question_image_height = $this->params->get('question_image_height', '200');
		}
		else
		{
			$this->params->question_image_height = $questionParams->question_image_height;
		}
		
		// Question date
		if (!isset($questionParams) || empty($questionParams->question_date))
		{
			$this->params->question_date = $this->params->get('question_date', '1');
		}
		else
		{
			$this->params->question_date = $questionParams->question_date;
		}
	
		// Question date format
		if (!isset($questionParams) || !isset($questionParams->question_date_format) || empty($questionParams->question_date_format))
		{
			$this->params->question_date_format = $this->params->get('question_date_format', 'l F d');
		}
		else
		{
			$this->params->question_date_format = $questionParams->question_date_format;
		}
		
		// Question author
		if (!isset($questionParams) || empty($questionParams->question_author))
		{
			$this->params->question_author = $this->params->get('question_author', '1');
		}
		else
		{
			$this->params->question_author = $questionParams->question_author;
		}
		
		// Question voting
		if (!isset($questionParams) || empty($questionParams->question_voting))
		{
			$this->params->question_voting = $this->params->get('question_voting', '1');
		}
		else
		{
			$this->params->question_voting = $questionParams->question_voting;
		}
		
		// Question edit link
		if (!isset($questionParams) || empty($questionParams->question_editlink))
		{
			$this->params->question_editlink = $this->params->get('question_editlink', '1');
		}
		else
		{
			$this->params->question_editlink = $questionParams->question_editlink;
		}
						
		// Get javascript variables //////////////////////////// To faqbookpro.php or controller.php
		$page_view = $app->input->get('view', false);
		$page_title = $document->getTitle();
		
  		$document->addScriptDeclaration(
		'window.fbpvars = {
    	  	token: "'.JSession::getFormToken().'",
			site_path: "'.JURI::root().'",
			page_view: "'.$page_view.'",
			page_title: "'.$page_title.'",
			sectionId: "'.$this->sectionId.'",
			topicId: "'.$topicId.'",
			activeTopic: "'.$this->active_topic.'",
			leftnav: "'.$leftnav.'",
			loadAllTopics: "'.$this->loadAllTopics.'",
			
			thank_you_up: "'.JText::_('COM_FAQBOOKPRO_THANK_YOU_UP').'",
			thank_you_down: "'.JText::_('COM_FAQBOOKPRO_THANK_YOU_DOWN').'",
			already_voted: "'.JText::_('COM_FAQBOOKPRO_ALREADY_VOTED').'",
			why_not: "'.JText::_('COM_FAQBOOKPRO_WHY_NOT').'",
			incorrect_info: "'.JText::_('COM_FAQBOOKPRO_INCORRECT_INFO').'",
			dont_like: "'.JText::_('COM_FAQBOOKPRO_DO_NOT_LIKE').'",
			confusing: "'.JText::_('COM_FAQBOOKPRO_CONFUSING').'",
			not_answer: "'.JText::_('COM_FAQBOOKPRO_NOT_ANSWER').'",
			too_much: "'.JText::_('COM_FAQBOOKPRO_TOO_MUCH').'",
			other: "'.JText::_('COM_FAQBOOKPRO_OTHER').'",
			error_voting: "'.JText::_('COM_FAQBOOKPRO_ERROR_VOTING').'"
    	};'
		);
		
		// Set metadata
		$document->setTitle($question->title);
		
		if ($question->metadesc)
		{
			$document->setDescription($question->metadesc);
		}
		elseif ($this->params->get('menu-meta_description'))
		{
			$document->setDescription($this->params->get('menu-meta_description'));
		}
		
		if ($question->metakey)
		{
			$document->setMetadata('keywords', $question->metakey);
		}
		elseif ($this->params->get('menu-meta_keywords'))
		{
			$document->setMetadata('keywords', $this->params->get('menu-meta_keywords'));
		}
		
		if (!is_object($question->metadata))
		{
			$question->metadata = new Registry($question->metadata);
		}
		
		$mdata = $question->metadata->toArray();

		foreach ($mdata as $k => $v)
		{
			if ($v)
			{
				$document->setMetadata($k, $v);
			}
		}
			
		// Check for errors.
		if (count($errors = $this->get('Errors'))) 
		{
		  	JError::raiseError(500, implode('<br />', $errors));
		  	return false;
		}
		
    	// Display the view
    	parent::display($tpl);
  }
	
}