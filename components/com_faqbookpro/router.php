<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die;

/* Builds the URL 
 * Transforms an array of URL parameters 
 * into an array of segments that will form the SEF URL
 */
function FaqBookProBuildRoute(&$query)
{
	// Initialize
	$segments = array();
	
	// If there is only the option and Itemid, let Joomla! decide on the naming scheme
	if (isset($query['option']) && isset($query['Itemid']) &&
		!isset($query['view']) && !isset($query['id'])
	)
	{
		return $segments;
	}
    
	// Get the menu
	$menu = JFactory::getApplication()->getMenu();
	
	// Detect the active menu item
	if (empty($query['Itemid']))
	{
		$menuItem = $menu->getActive();
		$menuItemGiven = false;
	}
	else
	{
		$menuItem = $menu->getItem($query['Itemid']);
		$menuItemGiven = true;
	}

	// Check again
	if ($menuItemGiven && isset($menuItem) && $menuItem->component != 'com_faqbookpro')
	{
		$menuItemGiven = false;
		unset($query['Itemid']);
	}
	
	if (isset($query['view']))
	{
		$view = $query['view'];
	}
	else
	{
		// We need to have a view in the query or it is an invalid URL
		return $segments;
	}
	
	// Are we dealing with a page that is directly attached to a menu item? If yes, just return segments.
	if (($menuItem instanceof stdClass) 
		&& $menuItem->query['view'] == $query['view'] 
		&& isset($query['id'])
		&& $menuItem->query['id'] == (int) $query['id'])
	{
		unset($query['view']);

		if (isset($query['id']))
		{
			unset($query['id']);
		}

		return $segments;
	}
	
	// Page is not directly attached to menu item.
	
	if ($view == 'sections')
	{
		if (!$menuItemGiven)
		{
			$segments[] = $view;
		}
		
		unset($query['view']);
	}
	
	if ($view == 'myquestions')
	{
		if ($menuItem->query['view'] != 'myquestions')
		{
			$segments[] = 'my-questions';
		}
		
		unset($query['view']);
	}
	
	if ($view == 'myquestion' && isset($query['layout']) && $query['layout'] == 'edit' && !isset($query['id']))
	{
		$segments[] = 'ask-a-question';
		
		unset($query['view']);
		unset($query['layout']);
	}
	
	if ($view == 'myquestion' && isset($query['layout']) && $query['layout'] == 'edit' && isset($query['id']) && $query['id'])
	{
		$segments[] = 'edit-question';
		$segments[] = $query['id'];
			
		unset($query['view']);
		unset($query['layout']);
		unset($query['id']);
	}
		
	if ($view == 'section')
	{	
		if (isset($query['view']))
		{
			unset($query['view']);
		}
		
		if (isset($query['id']))
		{
			// Get the section alias
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('alias')
				->from('#__minitek_faqbook_sections')
				->where('id=' . (int) $query['id']);
			$db->setQuery($dbQuery);
			$alias = $db->loadResult();
			
			$segments[] = $alias;
	
			unset($query['id']);
		};
	}
	
	if ($view == 'topic')
	{		
		if (isset($query['view']))
		{
			unset($query['view']);
		}
		
		if (isset($query['id']))
		{
			// If Itemid is for sections page, we don't have a menu item for section or topic, therefore we need the section segment
			if ($menuItem->query['view'] == 'sections')
			{
				// Get the section alias
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('b.alias as alias')
					->from('#__minitek_faqbook_topics AS a')
					->join('INNER', $db->quoteName('#__minitek_faqbook_sections', 'b') . ' ON (' . $db->quoteName('a.section_id') . ' = ' . $db->quoteName('b.id') . ')')
					->where($db->quoteName('a.id').'=' . (int) $query['id']);
				$db->setQuery($dbQuery);
				$alias = $db->loadResult();
				
				$segments[] = $alias;
			}
			
			// Get the topic path
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('path')
				->from('#__minitek_faqbook_topics')
				->where('id=' . (int) $query['id']);
			$db->setQuery($dbQuery);
			$path = $db->loadResult();
			
			// If Itemid is for parent topic, we must remove the parent topic path from the topic path
			if ($menuItem->query['view'] == 'topic')
			{
				// Get the parent topic path
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('path')
					->from('#__minitek_faqbook_topics')
					->where('id=' . (int) $menuItem->query['id']);
				$db->setQuery($dbQuery);
				$parentPath = $db->loadResult();
				
				$path = str_replace($parentPath.'/', '', $path.'/');
				$path = rtrim($path, '/');
			}
						
			$segments[] = $path;
			
			unset($query['id']);
		};
	}
	
	if ($view == 'question')
	{
		if (isset($query['view']))
		{
			unset($query['view']);
		}
		
		if (isset($query['id']))
		{
			// If Itemid is for sections page, we don't have a menu item for section or topic, therefore we need the section segment
			if ($menuItem->query['view'] == 'sections')
			{
				// Get the section alias
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('c.alias as alias')
					->from('#__minitek_faqbook_questions AS a')
					->join('INNER', $db->quoteName('#__minitek_faqbook_topics', 'b') . ' ON (' . $db->quoteName('a.topicid') . ' = ' . $db->quoteName('b.id') . ')')
					->join('INNER', $db->quoteName('#__minitek_faqbook_sections', 'c') . ' ON (' . $db->quoteName('b.section_id') . ' = ' . $db->quoteName('c.id') . ')')
					->where($db->quoteName('a.id').'=' . (int) $query['id']);
				$db->setQuery($dbQuery);
				$alias = $db->loadResult();
				
				$segments[] = $alias;
			}
			
			// Get the topic path
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('b.path as path')
				->from('#__minitek_faqbook_questions AS a')
				->join('INNER', $db->quoteName('#__minitek_faqbook_topics', 'b') . ' ON (' . $db->quoteName('a.topicid') . ' = ' . $db->quoteName('b.id') . ')')
				->where($db->quoteName('a.id').'=' . (int) $query['id']);
			$db->setQuery($dbQuery);
			$path = $db->loadResult();
			
			// If Itemid is for parent topic, we must remove the parent topic path from the topic path
			if ($menuItem->query['view'] == 'topic')
			{
				// Get the parent topic path
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('path')
					->from('#__minitek_faqbook_topics')
					->where('id=' . (int) $menuItem->query['id']);
				$db->setQuery($dbQuery);
				$parentPath = $db->loadResult();
				
				$path = str_replace($parentPath.'/', '', $path.'/');
				$path = rtrim($path, '/');
			}
						
			$segments[] = $path;
			
			// Get the question alias
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('alias')
				->from('#__minitek_faqbook_questions')
				->where($db->quoteName('id').'=' . (int) $query['id']);
			$db->setQuery($dbQuery);
			$questionAlias = $db->loadResult();
			
			$segments[] = $questionAlias;
			
			unset($query['id']);
		};
	}

	return $segments;
}

/* Parses the URL
 * Transforms an array of segments
 * back into an array of URL parameters
 */
function FaqBookProParseRoute($segments)
{
	$vars = array();
	$menus = JMenu::getInstance('site');
	$menu = $menus->getActive();
	
	// Count route segments
	$count = count($segments);
	
	// We check to see if it is a My-Questions or a Ask-Question / Edit-Question page	
	$lastSegment = str_replace(':', '-', $segments[$count - 1]);
	if ($lastSegment == 'my-questions')
	{
		$vars['view'] = 'myquestions';
		return $vars;
	}
		
	if ($count == 1)
	{
		$lastSegment = str_replace(':', '-', $segments[$count - 1]);
		if ($lastSegment == 'ask-a-question')
		{
			$vars['view'] = 'myquestion';
			$vars['layout'] = 'edit';
			return $vars;
		}
	}
	
	if ($count > 1)
	{
		$penultimateSegment = str_replace(':', '-', $segments[$count - 2]);
		if ($penultimateSegment == 'edit-question')
		{
			$vars['view'] = 'myquestion';
			$vars['layout'] = 'edit';
			$vars['id'] = $segments[$count - 1];
			return $vars;
		}
	}
		
	if (is_null($menu) || $menu->query['view'] == 'sections')
	{
		// No menu or we have only a Sections menu item. The segments are section_alias/topic_path/question_alias
		
		//If there is only 1 segment (alias) then it is a section.
		//If there are more than 1 segments then it could be a topic or a question
		if ($count == 1)
		{
			$vars['view'] = 'section';
			
			// We must find the section id from the alias
			$sectionAlias = str_replace(':', '-', $segments[0]);
			$sectionId = getSectionId($sectionAlias);
			$vars['id'] = (int) $sectionId;
		}
		
		if ($count > 1)
		{
			// We must decide if it a topic or a question
			// *** This fails if a question has the same alias as topic ***
			// *** It fails only if the resulting url is the same, so it's not really a problem ***
			// Check for question first
			$lastSegment = $segments[$count - 1];
			$lastSegment = str_replace(':', '-', $lastSegment);
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('id')
				->from('#__minitek_faqbook_questions')
				->where($db->quoteName('alias').'=' . $db->quote($lastSegment));
			$db->setQuery($dbQuery);
			$isQuestion = $db->loadResult();
			
			if ($isQuestion)
			{
				$vars['view'] = 'question';
				
				// We must find the question id from the alias
				$questionId = getQuestionId($lastSegment);
				$vars['id'] = (int) $questionId;
			}
			else
			{
				// It is a topic
				$vars['view'] = 'topic';
				
				$topicId = getTopicId($lastSegment); // we also need the parent topic id
				$vars['id'] = (int) $topicId;
			}
		}
	}
	else
	{
		// We don't mind about a section page because we have a menu item for it.
		
		// We must check whether it is a topic or a question.
		// We get the last segment and check its alias.
		if ($count > 1)
		{	
			// Check for question first
			// If we have enough segments, we check for parent topic to avoid duplicate question aliases and make sure we have the correct question
			// Get parent topic id
			$topicAlias = str_replace(':', '-', $segments[$count - 2]);
			$topicId = getTopicId($topicAlias);
			
			$lastSegment = $segments[$count - 1];
			$lastSegment = str_replace(':', '-', $lastSegment);
			$db = JFactory::getDbo();
			$dbQuery = $db->getQuery(true)
				->select('id')
				->from('#__minitek_faqbook_questions')
				->where($db->quoteName('alias').'=' . $db->quote($lastSegment))
				->where($db->quoteName('topicid').'=' . $db->quote($topicId));
			$db->setQuery($dbQuery);
			$isQuestion = $db->loadResult();
			
			if ($isQuestion)
			{
				$vars['view'] = 'question';
				$vars['id'] = (int) $isQuestion;
			}
			else
			{
				// It is a topic
				$vars['view'] = 'topic';
			
				// We check for parent topic to avoid duplicate topic aliases and make sure we have the correct topic
				// Get parent topic id
				$parentTopicPath = str_replace(':', '-', $segments[0]);
				foreach ($segments as $seg_key => $segment)
				{
					if ($seg_key == '0' || $seg_key == count($segments)- 1)
					{
						continue;	
					}
					$parentTopicPath .= '/'.str_replace(':', '-', $segment);
				}
				$parentTopicId = getTopicIdfromPath($parentTopicPath);
				$topicAlias = str_replace(':', '-', $segments[$count - 1]);
				
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('id')
					->from('#__minitek_faqbook_topics')
					->where('alias=' . $db->quote($topicAlias))
					->where('parent_id=' . $db->quote($parentTopicId));
				$db->setQuery($dbQuery);
				$topicId = $db->loadResult();
				
				$vars['id'] = (int) $topicId;
			}
		}
		else
		{
			// We don't have enough segments to check for parent topics, so we get the parent topic from the active menu.
			// If the active menu is a section, we don't mind about the parent topic because it is a first level topic.
			
			// If active menu is a section, then alias is a topic
			if ($menu->query['view'] == 'section')
			{
				$vars['view'] = 'topic';
				
				$topicAlias = str_replace(':', '-', $segments[$count - 1]);
				$topicId = getTopicId($topicAlias);
				$vars['id'] = (int) $topicId;
			}
			
			// If active menu is a topic, alias can be a topic or a question
			if ($menu->query['view'] == 'topic')
			{
				// Check for question first
				// We check for parent topic first to avoid duplicate question aliases and make sure we have the correct question
				$topicId = $menu->query['id'];
				
				$lastSegment = $segments[$count - 1];
				$lastSegment = str_replace(':', '-', $lastSegment);
				$db = JFactory::getDbo();
				$dbQuery = $db->getQuery(true)
					->select('id')
					->from('#__minitek_faqbook_questions')
					->where($db->quoteName('alias').'=' . $db->quote($lastSegment))
					->where($db->quoteName('topicid').'=' . $db->quote($topicId));
				$db->setQuery($dbQuery);
				$isQuestion = $db->loadResult();
				
				if ($isQuestion)
				{
					$vars['view'] = 'question';
					$vars['id'] = (int) $isQuestion;
				}
				else
				{
					// It is a topic
					$vars['view'] = 'topic';
					
					// We check for parent topic to avoid duplicate topic aliases and make sure we have the correct topic
					// Get parent topic id
					$parentTopicId = $menu->query['id'];
					$topicAlias = str_replace(':', '-', $segments[$count - 1]);
					
					$db = JFactory::getDbo();
					$dbQuery = $db->getQuery(true)
						->select('id')
						->from('#__minitek_faqbook_topics')
						->where('alias=' . $db->quote($topicAlias))
						->where('parent_id=' . $db->quote($parentTopicId));
					$db->setQuery($dbQuery);
					$topicId = $db->loadResult();
					
					$vars['id'] = (int) $topicId;
				}
			}
		}
	}
	
	return $vars;
}

function getSectionId($alias)
{
	$db = JFactory::getDbo();
	$dbQuery = $db->getQuery(true)
		->select('id')
		->from('#__minitek_faqbook_sections')
		->where('alias=' . $db->quote($alias));
	$db->setQuery($dbQuery);
	$id = $db->loadResult();
	
	return $id;
}

function getTopicId($alias)
{
	$db = JFactory::getDbo();
	$dbQuery = $db->getQuery(true)
		->select('id')
		->from('#__minitek_faqbook_topics')
		->where('alias=' . $db->quote($alias));
	$db->setQuery($dbQuery);
	$id = $db->loadResult();
	
	return $id;
}

function getTopicIdfromPath($path)
{
	$db = JFactory::getDbo();
	$dbQuery = $db->getQuery(true)
		->select('id')
		->from('#__minitek_faqbook_topics')
		->where('path=' . $db->quote($path));
	$db->setQuery($dbQuery);
	$id = $db->loadResult();
	
	return $id;
}

function getQuestionId($alias)
{
	$db = JFactory::getDbo();
	$dbQuery = $db->getQuery(true)
		->select('id')
		->from('#__minitek_faqbook_questions')
		->where('alias=' . $db->quote($alias));
	$db->setQuery($dbQuery);
	$id = $db->loadResult();
	
	return $id;
}