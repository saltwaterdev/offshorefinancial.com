<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class FaqBookProModelQuestion extends JModelItem
{
	protected $_context = 'com_faqbookpro.question';
	
	var $utilities = null;
	var $navigation = null;
	 
	function __construct() 
	{
		$this->utilities = $this->getUtilitiesLib();
		$this->navigation = $this->getNavigationLib();
		
		parent::__construct();	
	}
	
	public function getUtilitiesLib()
	{
		require_once( JPATH_SITE.DS.'components'.DS.'com_faqbookpro'.DS.'libraries'.DS.'utilities'.DS.'utilities.php' );
		$utilities = new FAQBookProLibUtilities;
		
		return $utilities;
	}
	
	public function getNavigationLib()
	{
		require_once( JPATH_SITE.DS.'components'.DS.'com_faqbookpro'.DS.'libraries'.DS.'utilities'.DS.'navigation.php' );
		$navigation = new FAQBookProLibUtilitiesNavigation;
		
		return $navigation;
	}
	
	public static function authorizeQuestion($id)
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();

		$query = $db->getQuery(true);
		
		$query->select('*')
			->from('#__minitek_faqbook_questions');
		$query->where('id = '.$id);
		$query->where('access IN (' . implode(',', $user->getAuthorisedViewLevels()) . ')');
		
		$db->setQuery($query);
		
		$row = $db->loadObject();
		if ($row)
		{
			return true;	
		}
		else
		{
			return false;
		}
	}
	
	protected function populateState()
	{
		$app = JFactory::getApplication('site');

		// Load state from the request.
		$pk = $app->input->getInt('id');
		$this->setState('question.id', $pk);

		$offset = $app->input->getUInt('limitstart');
		$this->setState('list.offset', $offset);

		// Load the parameters.
		$params = $app->getParams();
		$this->setState('params', $params);

		// TODO: Tune these values based on other permissions.
		$user = JFactory::getUser();

		if ((!$user->authorise('core.edit.state', 'com_faqbookpro')) && (!$user->authorise('core.edit', 'com_faqbookpro')))
		{
			$this->setState('filter.published', 1);
			$this->setState('filter.archived', 2);
		}

		$this->setState('filter.language', JLanguageMultilang::isEnabled());
	}
	
	public function getItem($pk = null)
	{
		$user = JFactory::getUser();

		$pk = (!empty($pk)) ? $pk : (int) $this->getState('question.id');

		if ($this->_item === null)
		{
			$this->_item = array();
		}

		if (!isset($this->_item[$pk]))
		{
			try
			{
				$db = $this->getDbo();
				$query = $db->getQuery(true)
					->select(
						$this->getState(
							'item.select', 'a.id, a.asset_id, a.title, a.alias, a.introtext, a.fulltext, ' .
							// If badcats is not null, this means that the article is inside an unpublished topic
							// In this case, the state is set to 0 to indicate Unpublished (even if the article state is Published)
							'CASE WHEN badcats.id is null THEN a.state ELSE 0 END AS state, ' .
							'a.topicid, a.created, a.created_by, a.created_by_alias, ' .
							// Use created if modified is 0
							'CASE WHEN a.modified = ' . $db->quote($db->getNullDate()) . ' THEN a.created ELSE a.modified END as modified, ' .
							'a.modified_by, a.checked_out, a.checked_out_time, a.publish_up, a.publish_down, ' .
							'a.images, a.urls, a.attribs, a.version, a.ordering, ' .
							'a.metakey, a.metadesc, a.access, a.hits, a.metadata, a.featured, a.language, a.xreference'
						)
					);
				$query->from('#__minitek_faqbook_questions AS a');

				// Join on topic table.
				$query->select('c.title AS topic_title, c.alias AS topic_alias, c.access AS topic_access')
					->join('LEFT', '#__minitek_faqbook_topics AS c on c.id = a.topicid');

				// Join on user table.
				$query->select('u.name AS author')
					->join('LEFT', '#__users AS u on u.id = a.created_by');

				// Filter by language
				if ($this->getState('filter.language'))
				{
					$query->where('a.language in (' . $db->quote(JFactory::getLanguage()->getTag()) . ',' . $db->quote('*') . ')');
				}

				// Join over the topics to get parent topic titles
				$query->select('parent.title as parent_title, parent.id as parent_id, parent.path as parent_route, parent.alias as parent_alias')
					->join('LEFT', '#__minitek_faqbook_topics as parent ON parent.id = c.parent_id');

				// Join on voting table
				/*$query->select('ROUND(v.rating_sum / v.rating_count, 0) AS rating, v.rating_count as rating_count')
					->join('LEFT', '#__content_rating AS v ON a.id = v.content_id')

					->where('a.id = ' . (int) $pk);*/

				if ((!$user->authorise('core.edit.state', 'com_faqbookpro')) && (!$user->authorise('core.edit', 'com_faqbookpro')))
				{
					// Filter by start and end dates.
					$nullDate = $db->quote($db->getNullDate());
					$date = JFactory::getDate();

					$nowDate = $db->quote($date->toSql());

					$query->where('(a.publish_up = ' . $nullDate . ' OR a.publish_up <= ' . $nowDate . ')')
						->where('(a.publish_down = ' . $nullDate . ' OR a.publish_down >= ' . $nowDate . ')');
				}

				// Join to check for topic published state in parent topics up the tree
				// If all topics are published, badcats.id will be null, and we just use the article state
				$subquery = ' (SELECT cat.id as id FROM #__minitek_faqbook_topics AS cat JOIN #__minitek_faqbook_topics AS parent ';
				$subquery .= 'ON cat.lft BETWEEN parent.lft AND parent.rgt ';
				$subquery .= ' WHERE parent.published <= 0 GROUP BY cat.id)';
				$query->join('LEFT OUTER', $subquery . ' AS badcats ON badcats.id = c.id');

				// Filter by published state.
				$published = $this->getState('filter.published');
				$archived = $this->getState('filter.archived');

				if (is_numeric($published))
				{
					$query->where('(a.state = ' . (int) $published . ' OR a.state =' . (int) $archived . ')');
				}

				$db->setQuery($query);

				$data = $db->loadObject();

				if (empty($data))
				{
					return JError::raiseError(404, JText::_('COM_FAQBOOKPRO_ERROR_QUESTION_NOT_FOUND'));
				}

				// Check for published state if filter set.
				if (((is_numeric($published)) || (is_numeric($archived))) && (($data->state != $published) && ($data->state != $archived)))
				{
					return JError::raiseError(404, JText::_('COM_FAQBOOKPRO_ERROR_QUESTION_NOT_FOUND'));
				}

				// Convert parameter fields to objects.
				$registry = new Registry;
				$registry->loadString($data->attribs);

				$data->params = clone $this->getState('params');
				$data->params->merge($registry);

				$registry = new Registry;
				$registry->loadString($data->metadata);
				$data->metadata = $registry;

				// Technically guest could edit an article, but lets not check that to improve performance a little.
				if (!$user->get('guest'))
				{
					$userId = $user->get('id');
					$asset = 'com_faqbookpro.question.' . $data->id;

					// Check general edit permission first.
					if ($user->authorise('core.edit', $asset))
					{
						$data->params->set('access-edit', true);
					}

					// Now check if edit.own is available.
					elseif (!empty($userId) && $user->authorise('core.edit.own', $asset))
					{
						// Check for a valid user and that they are the owner.
						if ($userId == $data->created_by)
						{
							$data->params->set('access-edit', true);
						}
					}
				}

				// Compute view access permissions.
				if ($access = $this->getState('filter.access'))
				{
					// If the access filter has been set, we already know this user can view.
					$data->params->set('access-view', true);
				}
				else
				{
					// If no access filter is set, the layout takes some responsibility for display of limited information.
					$user = JFactory::getUser();
					$groups = $user->getAuthorisedViewLevels();

					if ($data->topicid == 0 || $data->topic_access === null)
					{
						$data->params->set('access-view', in_array($data->access, $groups));
					}
					else
					{
						$data->params->set('access-view', in_array($data->access, $groups) && in_array($data->topic_access, $groups));
					}
				}

				$this->_item[$pk] = $data;
			}
			catch (Exception $e)
			{
				if ($e->getCode() == 404)
				{
					// Need to go thru the error handler to allow Redirect to work.
					JError::raiseError(404, $e->getMessage());
				}
				else
				{
					$this->setError($e);
					$this->_item[$pk] = false;
				}
			}
		}

		return $this->_item[$pk];
	}
	
	public static function getQuestion($id)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM '. $db->quoteName( '#__minitek_faqbook_questions' );
		$query .= ' WHERE ' . $db->quoteName( 'id' ) . ' = '. $db->quote($id).' ';
		$query .= ' AND ' . $db->quoteName( 'state' ) . ' = '. $db->quote(1).' ';
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($row)
		{
			return $row;	
		}
		else
		{
			return false;
		}
	}
	
	public function getQuestionSection($topicId)
	{
		$db = JFactory::getDbo();
		$user = JFactory::getUser();
		
		$query = $db->getQuery(true);
		
		$query->select('a.section_id');
		$query->from('#__minitek_faqbook_topics AS a');
					
		// Join over the sections
		$query->select('section.id as section_id')
			->join('LEFT', '#__minitek_faqbook_sections as section ON section.id = a.section_id');
						
		$query->where('a.id = ' . $db->quote($topicId));
		
		// Get the results
		$db->setQuery($query);
		$sectionId = $db->loadObject()->section_id;
		
		return $sectionId;
	}
			
	public static function FaqVoting($id, $type) 
	{
		$db = JFactory::getDBO();
		$reason = JRequest::getVar('reason',0,'','INT');
		$user = JFactory::getUser();
    	$user_id = $user->id;
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$jnow = JFactory::getDate();
		$date = $jnow->toSql();
		
		$output = '';
		
		// Add controls for existing votes
		if ($user_id) 
		{
  			$query	= ' SELECT a.* FROM ' . $db->quoteName('#__minitek_faqbook_votes') . ' AS a '
  				. ' WHERE a.'.$db->quoteName('question_id').'=' . $db->Quote($id)
				. ' AND a.'.$db->quoteName('user_id').'=' . $db->Quote($user_id);
  			$db->setQuery( $query );
		  	$user_vote_exists = $db->loadObject();
			
			if (!$user_vote_exists) 
			{
    			if ($type == 1) 
				{
    		  		$query = " INSERT INTO "
          				.$db->quoteName("#__minitek_faqbook_votes")
          				." (question_id, user_id, user_ip, vote_up, creation_date) "
                  		." VALUES ('".$id."','".$user_id."','".$user_ip."','1','".$date."') ";
    				$db->setQuery($query);
    				$db->query();	
					
					// Get sum
  			  		$query = "SELECT COUNT(*) FROM "
            			.$db->quoteName("#__minitek_faqbook_votes")
            			." WHERE " . $db->quoteName("question_id") . "=" . $db->Quote($id)
            			." AND " . $db->quoteName("vote_up") . "=" . $db->Quote('1');
  					$db->setQuery($query);
  					$vote_sum = $db->loadResult();
        		}
				
    			if ($type == 0) 
				{
    		  		$query = " INSERT INTO `#__minitek_faqbook_votes` (question_id, user_id, user_ip, vote_down, reason, creation_date) "
                  		." VALUES ('".$id."','".$user_id."','".$user_ip."','1','".$reason."','".$date."') ";
    				$db->setQuery($query);
    				$db->query();		
					
					// Get sum
  			  		$query = "SELECT COUNT(*) FROM "
            			.$db->quoteName("#__minitek_faqbook_votes")
            			." WHERE " . $db->quoteName("question_id") . "=" . $db->Quote($id)
            			." AND " . $db->quoteName("vote_down") . "=" . $db->Quote('1');
  					$db->setQuery($query);
  					$vote_sum = $db->loadResult();
        		}
				
				$output = $vote_sum;
			}
		} 
		else 
		{
		  	$query = ' SELECT a.* FROM ' . $db->quoteName('#__minitek_faqbook_votes') . ' AS a '
  				. ' WHERE a.'.$db->quoteName('question_id').'=' . $db->Quote($id)
				. ' AND a.'.$db->quoteName('user_ip').'=' . $db->Quote($user_ip);
  			$db->setQuery( $query );
		  	$ip_vote_exists = $db->loadObject();
			
			if (!$ip_vote_exists) 
			{
    			if ($type == 1) {
    		  	$query = " INSERT INTO `#__minitek_faqbook_votes` (question_id, user_id, user_ip, vote_up, creation_date) "
                	." VALUES ('".$id."','".$user_id."','".$user_ip."','1','".$date."') ";
    			$db->setQuery($query);
    			$db->query();	
					
				// Get sum
  			  	$query = "SELECT COUNT(*) FROM "
            		.$db->quoteName("#__minitek_faqbook_votes")
            		." WHERE " . $db->quoteName("question_id") . "=" . $db->Quote($id)
            		." AND " . $db->quoteName("vote_up") . "=" . $db->Quote('1');
  				$db->setQuery($query);
  				$vote_sum = $db->loadResult();
        	}
			
    		if ($type == 0) 
			{
    		  	$query = " INSERT INTO `#__minitek_faqbook_votes` (question_id, user_id, user_ip, vote_down, reason, creation_date) "
                	." VALUES ('".$id."','".$user_id."','".$user_ip."','1','".$reason."','".$date."') ";
    			$db->setQuery($query);
    			$db->query();		
					
				// Get sum
  			 	$query = "SELECT COUNT(*) FROM "
            		.$db->quoteName("#__minitek_faqbook_votes")
            		." WHERE " . $db->quoteName("question_id") . "=" . $db->Quote($id)
            		." AND " . $db->quoteName("vote_down") . "=" . $db->Quote('1');
  				$db->setQuery($query);
  				$vote_sum = $db->loadResult();
        	}
				
				$output = $vote_sum;
			}
		
		}
		
		echo $output;
	}
	
	public static function FaqVotingReason($rid, $fid) 
	{
		$db = JFactory::getDBO();
		$user = JFactory::getUser();
    	$user_id = $user->id;
		$user_ip = $_SERVER['REMOTE_ADDR'];
		$jnow = JFactory::getDate();
		$date = $jnow->toSql();
		
		if ($user_id) 
		{ 		
    		$query = " UPDATE `#__minitek_faqbook_votes` "
				." SET reason = ".$db->Quote($rid)." "
				." WHERE question_id = ".$db->Quote($fid)." "
				." AND user_id = ".$db->Quote($user_id)." ";
                  
    		$db->setQuery($query);
    		$db->query();	
			
			$output = $rid;
			
			// db error handling	
		} 
		else 
		{
    		$query = " UPDATE `#__minitek_faqbook_votes` "
				." SET reason = ".$db->Quote($rid)." "
				." WHERE question_id = ".$db->Quote($fid)." "
				." AND user_ip = ".$db->Quote($user_ip)." ";
                  
    		$db->setQuery($query);
    		$db->query();		
							
			$output = $rid;
			
			// db error handling			
		}
		
		return $output;
	}
	
	static function getFaqVotes($faq_id, $type) 
	{	
	  	$db = JFactory::getDBO();
	  	$query = "SELECT COUNT(*) FROM "
        	.$db->quoteName("#__minitek_faqbook_votes")
            ." WHERE " . $db->quoteName("question_id") . "=" . $db->Quote($faq_id)
            ." AND " . $db->quoteName($type) . "=" . $db->Quote('1');
  		$db->setQuery($query);
  		$vote_sum = $db->loadResult();
		
		return $vote_sum;
	}
	
	public static function addHit($id)
	{	
	    $db = JFactory::getDBO();
		$query = " UPDATE `#__minitek_faqbook_questions` "
			." SET hits = hits + 1 "
			." WHERE id = ".$db->Quote($id)." ";
    	$db->setQuery($query);
    	$db->query();			
	}
}