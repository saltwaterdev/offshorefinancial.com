<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2018 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	https://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

if(!defined('DS'))
{
	define('DS',DIRECTORY_SEPARATOR);
}

use Joomla\Registry\Registry;

class FAQBookProModelMyQuestion extends JModelAdmin
{
	var $utilities = null;
	var $navigation = null;
	
	protected $text_prefix = 'COM_FAQBOOKPRO';
	
	public function __construct($config = array())
	{
		$this->utilities = $this->getUtilitiesLib();
		$this->navigation = $this->getNavigationLib();
		
		parent::__construct($config);
	}
	
	public function getUtilitiesLib()
	{
		$utilities = new FAQBookProLibUtilities;
		
		return $utilities;
	}
	
	public function getNavigationLib()
	{
		$navigation = new FAQBookProLibUtilitiesNavigation;
		
		return $navigation;
	}
	
	public static function getQuestion($id)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM '. $db->quoteName( '#__minitek_faqbook_questions' );
		$query .= ' WHERE ' . $db->quoteName( 'id' ) . ' = '. $db->quote($id).' ';
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($row)
		{
			return $row;	
		}
		else
		{
			return false;
		}
	}
		
	protected function generateNewTitle($topic_id, $alias, $title)
	{
		// Alter the title & alias
		$table = $this->getTable();
		while ($table->load(array('alias' => $alias, 'topicid' => $topic_id)))
		{
			$title = JString::increment($title);
			$alias = JString::increment($alias, 'dash');
		}
		
		return array($title, $alias);
	}

	protected function canDelete($record)
	{
		$user = JFactory::getUser();

		if (!empty($record->id))
		{
			/*if ($record->state != -2)
			{
				return false;
			}*/
				
			return $user->authorise('core.delete', 'com_faqbookpro.question.' . (int) $record->id);
		}

		return false;
	}

	protected function canEditState($record)
	{
		$user = JFactory::getUser();

		// Check for existing question.
		if (!empty($record->id))
		{
			return $user->authorise('core.edit.state', 'com_faqbookpro.question.' . (int) $record->id);
		}
		// New question, so check against the topic.
		elseif (!empty($record->topicid))
		{
			return $user->authorise('core.edit.state', 'com_faqbookpro.topic.' . (int) $record->topicid);
		}
		// Default to component settings if neither question nor topic known.
		else
		{
			return parent::canEditState('com_faqbookpro');
		}
	}

	protected function prepareTable($table)
	{
		// Set the publish date to now
		$db = $this->getDbo();
		
		if ($table->state == 1 && (int) $table->publish_up == 0)
		{
			$table->publish_up = JFactory::getDate()->toSql();
		}

		if ($table->state == 1 && intval($table->publish_down) == 0)
		{
			$table->publish_down = $db->getNullDate();
		}
	}
	
	public function getTable($type = 'Question', $prefix = 'FAQBookProTable', $config = array())
	{
		return JTable::getInstance($type, $prefix, $config);
	}

	public function getItem($pk = null)
	{
		if ($item = parent::getItem($pk))
		{		
			// Convert the params field to an array.
			$registry = new Registry;
			$registry->loadString($item->attribs);
			$item->attribs = $registry->toArray();
		
			// Convert the metadata field to an array.
			$registry = new JRegistry;
			$registry->loadString($item->metadata);
			$item->metadata = $registry->toArray();
			
			// Convert the images field to an array.
			$registry = new Registry;
			$registry->loadString($item->images);
			$item->images = $registry->toArray();
			
			$item->articletext = trim($item->fulltext) != '' ? $item->introtext . "<hr id=\"system-readmore\" />" . $item->fulltext : $item->introtext;
		}

		return $item;
	}

	public function getForm($data = array(), $loadData = true)
	{
		$params = $this->utilities->getParams('com_faqbookpro');
		$user = JFactory::getUser();
		
		if ($params->get('question_captcha') == 1 || ($params->get('question_captcha') == 2 && !$user->id))		
		{
			// Get the form with captcha
			$form = $this->loadForm('com_faqbookpro.question', 'question', array('control' => 'jform', 'load_data' => $loadData));
		}
		else
		{
			// Get the form without captcha
			$form = $this->loadForm('com_faqbookpro.question_noc', 'question_noc', array('control' => 'jform', 'load_data' => $loadData));
		}
		
		if (empty($form))
		{
			return false;
		}
		
		$jinput = JFactory::getApplication()->input;

		// The front end calls this model and uses a_id to avoid id clashes so we need to check for that first.
		if ($jinput->get('a_id'))
		{
			$id = $jinput->get('a_id', 0);
		}
		// The back end uses id so we use that the rest of the time and set it to 0 by default.
		else
		{
			$id = $jinput->get('id', 0);
		}

		// Determine correct permissions to check.
		if ($this->getState('question.id'))
		{
			$id = $this->getState('question.id');

			// Existing record. Can only edit in selected topics.
			$form->setFieldAttribute('topicid', 'action', 'core.edit');

			// Existing record. Can only edit own questions in selected topics.
			$form->setFieldAttribute('topicid', 'action', 'core.edit.own');
		}
		else
		{
			// New record. Can only create in selected topics.
			$form->setFieldAttribute('topicid', 'action', 'core.create');
		}

		// Check for existing question.
		// Modify the form based on Edit State access controls.
		if ($id != 0 && (!$user->authorise('core.edit.state', 'com_faqbookpro.question.' . (int) $id))
			|| ($id == 0 && !$user->authorise('core.edit.state', 'com_faqbookpro')))
		{
			// Disable fields for display.
			$form->setFieldAttribute('featured', 'disabled', 'true');
			$form->setFieldAttribute('ordering', 'disabled', 'true');
			$form->setFieldAttribute('publish_up', 'disabled', 'true');
			$form->setFieldAttribute('publish_down', 'disabled', 'true');
			if (!$params->get('auto_publish'))
			{
				$form->setFieldAttribute('state', 'disabled', 'true');
			}

			// Disable fields while saving.
			// The controller has already verified this is a question you can edit.
			$form->setFieldAttribute('featured', 'filter', 'unset');
			$form->setFieldAttribute('ordering', 'filter', 'unset');
			$form->setFieldAttribute('publish_up', 'filter', 'unset');
			$form->setFieldAttribute('publish_down', 'filter', 'unset');
			if (!$params->get('auto_publish'))
			{
				$form->setFieldAttribute('state', 'filter', 'unset');
			}
		}

		return $form;
	}

	protected function loadFormData()
	{
		// Check the session for previously entered form data.
		$app = JFactory::getApplication();
		$data = $app->getUserState('com_faqbookpro.edit.question.data', array());

		if (empty($data))
		{
			$data = $this->getItem();

			// Pre-select some filters (Status, Topic, Language, Access) in edit form if those have been selected in Global Configuration
			/*if ($this->getState('question.id') == 0)
			{
				$filters = (array) $app->getUserState('com_faqbookpro.questions.filter');
				$data->set(
					'state',
					$app->input->getInt(
						'state',
						((isset($filters['published']) && $filters['published'] !== '') ? $filters['published'] : null)
					)
				);
				$data->set('topicid', $app->input->getInt('topicid', (!empty($filters['topic_id']) ? $filters['topic_id'] : null)));
				$data->set('language', $app->input->getString('language', (!empty($filters['language']) ? $filters['language'] : null)));
				$data->set('access', $app->input->getInt('access', (!empty($filters['access']) ? $filters['access'] : JFactory::getConfig()->get('access'))));
			}*/
		}

		// If there are params fieldsets in the form it will fail with a registry object
		if (isset($data->params) && $data->params instanceof Registry)
		{
			$data->params = $data->params->toArray();
		}

		$this->preprocessData('com_faqbookpro.question', $data);

		return $data;
	}

	public function save($data)
	{		
		$input = JFactory::getApplication()->input;
		$filter  = JFilterInput::getInstance();

		if (isset($data['metadata']) && isset($data['metadata']['author']))
		{
			$data['metadata']['author'] = $filter->clean($data['metadata']['author'], 'TRIM');
		}

		if (isset($data['created_by_alias']))
		{
			$data['created_by_alias'] = $filter->clean($data['created_by_alias'], 'TRIM');
		}

		if (isset($data['images']) && is_array($data['images']))
		{
			$registry = new Registry;
			$registry->loadArray($data['images']);
			$data['images'] = (string) $registry;
		}

		// Automatic handling of alias for empty fields
		if (in_array($input->get('task'), array('apply', 'save', 'save2new')) && (!isset($data['id']) || (int) $data['id'] == 0))
		{
			if (!isset($data['alias']) || $data['alias'] == null)
			{
				if (JFactory::getConfig()->get('unicodeslugs') == 1)
				{
					$data['alias'] = JFilterOutput::stringURLUnicodeSlug($data['title']);
				}
				else
				{
					$data['alias'] = JFilterOutput::stringURLSafe($data['title']);
				}

				$table = JTable::getInstance('Question', 'FAQBookProTable');

				if ($table->load(array('alias' => $data['alias'], 'topicid' => $data['topicid'])))
				{
					$msg = JText::_('COM_FAQBOOKPRO_SAVE_WARNING');
				}

				list($title, $alias) = $this->generateNewTitle($data['topicid'], $data['alias'], $data['title']);
				$data['alias'] = $alias;

				if (isset($msg))
				{
					JFactory::getApplication()->enqueueMessage($msg, 'warning');
				}
			}
		}
		
		// Code from JModelAdmin
		$table      = $this->getTable();
		$context    = $this->option . '.' . $this->name;
		$key = $table->getKeyName();
		$pk = (!empty($data[$key])) ? $data[$key] : (int) $this->getState($this->getName() . '.id');
		$isNew = true;

		try
		{
			// Load the row if saving an existing record.
			if ($pk > 0)
			{
				$table->load($pk);
				$isNew = false;
			}
			
			// Bind the data.
			if (!$table->bind($data))
			{
				$this->setError($table->getError());
				return false;
			}
			
			// Prepare the row for saving
			$this->prepareTable($table);
			
			// Check the data.
			if (!$table->check())
			{
				$this->setError($table->getError());
				return false;
			}
			
			// Store the data.
			if (!$table->store())
			{
				$this->setError($table->getError());
				return false;
			}
			
			// Clean the cache.
			$this->cleanCache();
		}
		catch (Exception $e)
		{
			$this->setError($e->getMessage());
			return false;
		}
		
		if (isset($table->$key))
		{
			$this->setState($this->getName() . '.id', $table->$key);
		}
		
		$this->setState($this->getName() . '.new', $isNew);
		
		$params = JComponentHelper::getParams('com_faqbookpro');
		$new_question_notification = $params->get('new_question_notification', 1);
				
		if ($new_question_notification && $isNew)
		{
			// Send email for new question to moderators
			$this->sendEmailtoAdmins($table);
		}
		
		return true;
	}

	protected function preprocessForm(JForm $form, $data, $group = 'faqbookpro')
	{
		parent::preprocessForm($form, $data, $group);
	}

	protected function cleanCache($group = null, $client_id = 0)
	{
		parent::cleanCache('com_faqbookpro');
	}
		
	public static function getEmailTemplate($template_key, $recipient_lang) 
	{
		$db = JFactory::getDbo();
		$query = $db->getQuery(true);
		$query->select('*')
			->from('#__minitek_faqbook_email_templates AS et');
		$query->where(
			'et.state = 1 
			AND et.template_key = '.$db->quote($template_key).' 
			AND et.language = '.$db->quote($recipient_lang).''
		);
		$query->order('et.id DESC');
		$db->setQuery($query);
		$email_template = $db->loadObject();
		
		if (!$email_template)
		{
			$query = $db->getQuery(true);
			$query->select('*')
				->from('#__minitek_faqbook_email_templates AS et');
			$query->where(
				'et.state = 1 
				AND et.template_key = '.$db->quote($template_key).' 
				AND et.language = '.$db->quote('*').''
			);
			$query->order('et.id DESC');
			$db->setQuery($query);
			$email_template = $db->loadObject();
		}
		
		return $email_template;
	}
	
	public static function getTopic($id)
	{
		$db = JFactory::getDBO();
		$query = 'SELECT * FROM '. $db->quoteName( '#__minitek_faqbook_topics' );
		$query .= ' WHERE ' . $db->quoteName( 'id' ) . ' = '. $db->quote($id).' ';
		$db->setQuery($query);
		$row = $db->loadObject();
		if ($row)
		{
			return $row;	
		}
		else
		{
			return false;
		}
	}
	
	/**
	 * Sends emails for new questions to administrators
	 */
	public function sendEmailtoAdmins($item)
	{
		require_once (JPATH_SITE.DS.'components'.DS.'com_faqbookpro'.DS.'helpers'.DS.'route.php');
		
		$app = JApplication::getInstance('site');
		$router = $app->getRouter();
		$parse = parse_url(JURI::root());
		$domain = $parse['scheme'];
		$domain .= '://'.$parse['host'];
		
		$questionId = $item->id;
		$questionTitle = $item->title;
		$topicId = $item->topicid;
		$created = $item->created;
		$author = JFactory::getUser($item->created_by);
		
		// Get question url
		$uri = $router->build(FaqBookProHelperRoute::getQuestionRoute($questionId, $topicId));
		$questionUrl = $uri->toString();
		$questionUrl = str_replace('administrator/', '', $questionUrl);
		$questionUrl = $domain.''.$questionUrl;
		
		// Get topic title
		$topicTitle = $this->getTopic($topicId)->title;
		
		// Get topic url
		$uri = $router->build(FaqBookProHelperRoute::getTopicRoute($topicId));
		$topicUrl = $uri->toString();
		$topicUrl = str_replace('administrator/', '', $topicUrl);	
		$topicUrl = $domain.''.$topicUrl;
		
		// Get email template key
		$template_key = 'new-question';
		
		// Get topic params
		$topicParams = json_decode($this->getTopic($topicId)->params, false);
		
		// Check for topic-specific managers first
		$topic_managers = isset($topicParams->notify_managers) ? $topicParams->notify_managers : false;
	
		if ($topic_managers && $topic_managers[0] != 'global') 
		{
			if ($topic_managers[0] == 'all')
			{
				require_once JPATH_ROOT.'/administrator/components/com_faqbookpro/helpers/utilities.php';
				$managers = FAQBookProHelperUtilities::getManagers();
			}
			else
			{
				// Now managers is an array of integers
				$managers = $topic_managers;
			}
		} 
		else 
		{
			// Fall-back to global config managers
			$params = JComponentHelper::getParams('com_faqbookpro');
			$notify_managers = $params->get('notify_managers', 'all');
			
			if ((count($notify_managers) == 1 && $notify_managers[0] == 'all') || $notify_managers == 'all')
			{
				require_once JPATH_ROOT.'/administrator/components/com_faqbookpro/helpers/utilities.php';
				$managers = FAQBookProHelperUtilities::getManagers();
			}
			else
			{
				// Now managers is an array of integers
				$managers = $notify_managers;
			}
		}
		
		foreach ($managers as $manager)
		{
			// Get site language
			$lang = JFactory::getLanguage();
			$defaultLang = $lang->getTag();
			
			// Get recipient language
			$manager_id = is_object($manager) ? $manager->id : $manager;
			$recipient = JFactory::getUser($manager_id);
			$recipient_lang = $recipient->getParam('language', $defaultLang);
			
			// Select email template
			$email_template = $this->getEmailTemplate($template_key, $recipient_lang);
			
			// Create email queue subject/content
			$email_subject = $email_template->subject;
			$email_content = $email_template->content;
			
			// Replace placeholders
			$email_subject = str_ireplace(
				array (
					'[NOTIFICATION_DATE]',
					'[RECIPIENT_NAME]',
					'[QUESTION_ID]',
					'[QUESTION_TITLE]',
					'[QUESTION_URL]',
					'[AUTHOR_NAME]',
					'[TOPIC_TITLE]',
					'[TOPIC_URL]',
					'[SITENAME]'
				),
				array (
					$item->created,
					$recipient->name,
					$questionId,
					$questionTitle,
					$questionUrl,
					$author->name,
					$topicTitle,
					$topicUrl,
					JURI::root()
				),
				$email_subject
			); 
			
			$email_content = str_ireplace(
				array (
					'[NOTIFICATION_DATE]',
					'[RECIPIENT_NAME]',
					'[QUESTION_ID]',
					'[QUESTION_TITLE]',
					'[QUESTION_URL]',
					'[AUTHOR_NAME]',
					'[TOPIC_TITLE]',
					'[TOPIC_URL]',
					'[SITENAME]'
				),
				array (
					$item->created,
					$recipient->name,
					$questionId,
					$questionTitle,
					$questionUrl,
					$author->name,
					$topicTitle,
					$topicUrl,
					JURI::root()
				),
				$email_content
			); 
			
			// Send email
			$mailer = JFactory::getMailer();
			$config = JFactory::getConfig();
			$sender = array( 
				$config->get( 'mailfrom' ),
				$config->get( 'fromname' ) );
				
			$mailer->setSender($sender);
			$mailer->addRecipient($recipient->email);				
			$mailer->setSubject($email_subject);
			$mailer->isHTML(true);
			$mailer->Encoding = 'base64';
			$mailer->setBody($email_content);
			
			if ($author->email != $recipient->email)
			{
				$send = $mailer->Send();
			
				if ($send)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}
		
		return;
	}
}