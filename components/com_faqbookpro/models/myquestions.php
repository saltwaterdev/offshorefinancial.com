<?php
/**
* @title			Minitek FAQ Book
* @copyright   		Copyright (C) 2011-2015 Minitek, All rights reserved.
* @license   		GNU General Public License version 3 or later.
* @author url   	http://www.minitek.gr/
* @developers   	Minitek.gr
*/

// no direct access
defined('_JEXEC') or die('Restricted access');

class FAQBookProModelMyQuestions extends JModelList
{
	var $utilities = null;
	var $navigation = null;
	
	public function __construct($config = array())
	{
		$this->utilities = $this->getUtilitiesLib();
		$this->navigation = $this->getNavigationLib();
		
		if (empty($config['filter_fields']))
		{
			$config['filter_fields'] = array(
				'id', 'a.id',
				'title', 'a.title',
				'alias', 'a.alias',
				'checked_out', 'a.checked_out',
				'checked_out_time', 'a.checked_out_time',
				'topic', 'a.topic', 'topic_title',
				'state', 'a.state',
				'access', 'a.access', 'access_level',
				'created', 'a.created',
				'created_by', 'a.created_by',
				'created_by_alias', 'a.created_by_alias',
				'ordering', 'a.ordering',
				'featured', 'a.featured',
				'language', 'a.language',
				'hits', 'a.hits',
				'publish_up', 'a.publish_up',
				'publish_down', 'a.publish_down',
				'published', 'a.published',
				'author_id',
				'topic_id',
				'level',
				'votes_up',
				'votes_down'
			);

			$app = JFactory::getApplication();
		}

		parent::__construct($config);
	}
	
	public function getUtilitiesLib()
	{
		$utilities = new FAQBookProLibUtilities;
		
		return $utilities;
	}
	
	public function getNavigationLib()
	{
		$navigation = new FAQBookProLibUtilitiesNavigation;
		
		return $navigation;
	}

	protected function populateState($ordering = null, $direction = null)
	{
		$app = JFactory::getApplication();

		// Adjust the context to support modal layouts.
		if ($layout = $app->input->get('layout'))
		{
			$this->context .= '.' . $layout;
		}
		
		$search = $this->getUserStateFromRequest($this->context . '.filter.search', 'filter_search');
		$this->setState('filter.search', $search);
		
		$published = $this->getUserStateFromRequest($this->context . '.filter.published', 'filter_published', '');
		$this->setState('filter.published', $published);
		
		$topicId = $this->getUserStateFromRequest($this->context . '.filter.topic_id', 'filter_topic_id');
		$this->setState('filter.topic_id', $topicId);

		$level = $this->getUserStateFromRequest($this->context . '.filter.level', 'filter_level');
		$this->setState('filter.level', $level);


		// List state information.
		parent::populateState('a.id', 'desc');
		
		// force a language
		$forcedLanguage = $app->input->get('forcedLanguage');
		if (!empty($forcedLanguage))
		{
			$this->setState('filter.language', $forcedLanguage);
			$this->setState('filter.forcedLanguage', $forcedLanguage);
		}

	}

	protected function getStoreId($id = '')
	{
		// Compile the store id.
		$id .= ':' . $this->getState('filter.search');
		$id .= ':' . $this->getState('filter.published');
		$id .= ':' . $this->getState('filter.topic_id');

		return parent::getStoreId($id);
	}

	protected function getListQuery()
	{
		// Create a new query object.
		$db = $this->getDbo();
		$query = $db->getQuery(true);
		$user = JFactory::getUser();
		$app = JFactory::getApplication();

		// Select the required fields from the table.
		$query->select(
			$this->getState(
				'list.select',
				'a.id, a.title, a.alias, a.checked_out, a.checked_out_time, a.topicid' .
					', a.state, a.access, a.created, a.created_by, a.created_by_alias, a.ordering, a.featured, a.language, a.hits' .
					', a.publish_up, a.publish_down'
			)
		);
		$query->from('#__minitek_faqbook_questions AS a');

		// Join over the language
		$query->select('l.title AS language_title')
			->join('LEFT', $db->quoteName('#__languages') . ' AS l ON l.lang_code = a.language');

		// Join over the users for the checked out user.
		$query->select('uc.name AS editor')
			->join('LEFT', '#__users AS uc ON uc.id=a.checked_out');

		// Join over the asset groups.
		$query->select('ag.title AS access_level')
			->join('LEFT', '#__viewlevels AS ag ON ag.id = a.access');
			
		// Join over the topics.
		$query->select('c.title AS topic_title')
			->join('LEFT', '#__minitek_faqbook_topics AS c ON c.id = a.topicid');
			
		// Join over the votes for the question.
		$query->select('COUNT(DISTINCT vu.id) as votes_up')
			->join('LEFT', '#__minitek_faqbook_votes AS vu ON vu.question_id = a.id AND vu.vote_up=1')
			->group('a.id');
			
		$query->select('COUNT(DISTINCT vd.id) as votes_down')
			->join('LEFT', '#__minitek_faqbook_votes AS vd ON vd.question_id = a.id AND vd.vote_down=1')
			->group('a.id');
				
		// Filter by published state
		$published = $this->getState('filter.published');
		if (is_numeric($published))
		{
			$query->where('a.state = ' . (int) $published);
		}
		elseif ($published === '')
		{
			$query->where('(a.state = 0 OR a.state = 1)');
		}
		
		// Filter by a single or group of topics.
		$baselevel = 1;
		$topicId = $this->getState('filter.topic_id');

		if (is_numeric($topicId))
		{
			$topic_tbl = JTable::getInstance('Topic', 'FAQBookProTable');
			$topic_tbl->load($topicId);
			$rgt = $topic_tbl->rgt;
			$lft = $topic_tbl->lft;
			$baselevel = (int) $topic_tbl->level;
			$query->where('c.lft >= ' . (int) $lft)
				->where('c.rgt <= ' . (int) $rgt);
		}
		elseif (is_array($topicId))
		{
			JArrayHelper::toInteger($topicId);
			$topicId = implode(',', $topicId);
			$query->where('a.topicid IN (' . $topicId . ')');
		}

		// Filter on the level.
		if ($level = $this->getState('filter.level'))
		{
			$query->where('c.level <= ' . ((int) $level + (int) $baselevel - 1));
		}
		
		// Filter by user
		$query->where('a.created_by = ' . (int) $user->id);
		
		// Filter by search in list title
		$search = $this->getState('filter.search');
		if (!empty($search))
		{
			$search = $db->quote('%' . str_replace(' ', '%', $db->escape(trim($search), true) . '%'));
			$query->where('(a.title LIKE ' . $search . ' OR a.alias LIKE ' . $search . ')');
		}

		// Add the list ordering clause.
		$orderCol = $this->state->get('list.ordering', 'a.id');
		$orderDirn = $this->state->get('list.direction', 'desc');
		
		//sqlsrv change
		if ($orderCol == 'language')
		{
			$orderCol = 'l.title';
		}
		if ($orderCol == 'access_level')
		{
			$orderCol = 'ag.title';
		}
		$query->order($db->escape($orderCol . ' ' . $orderDirn));

		// echo nl2br(str_replace('#__','jos_',$query));
		return $query;
	}

	public function getItems()
	{
		$items = parent::getItems();
		$app = JFactory::getApplication();
		return $items;
	}
	
}
